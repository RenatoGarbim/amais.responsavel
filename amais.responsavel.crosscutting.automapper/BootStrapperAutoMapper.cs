﻿using System;
using AMais.Responsavel.CrossCutting.AutoMapper.Profile;
using AutoMapper;

namespace AMais.Responsavel.CrossCutting.AutoMapper
{
    public static class BootStrapperAutoMapper
    {
        public static Action<IMapperConfigurationExpression> ConfigAction = new Action<IMapperConfigurationExpression>(
            cfg =>
            {
                cfg.AddProfile<TabelasProfile>();

            });

    }
}