﻿using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Entities.ValueObjects;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.ValueObjects;

namespace AMais.Responsavel.CrossCutting.AutoMapper.Profile
{
    public class TabelasProfile : global::AutoMapper.Profile
    {
        public TabelasProfile()
        {

            CreateMap<Usuario, UsuarioDTO>()
                .ForMember(x => x.Email, config => config.Ignore())
                .AfterMap((scr, dest) =>
                {
                    dest.Nome = string.IsNullOrEmpty(scr.Nome) ? "" : scr.Nome;
                    dest.Sobrenome = string.IsNullOrEmpty(scr.Sobrenome) ? "" : scr.Sobrenome;
                });

            CreateMap<Endereco, EnderecoDTO>();
            
            CreateMap<Token, TokenDTO>()
                .ForMember(dest => dest.UsuarioId, source => source.MapFrom(x => x.UsuarioId));

            CreateMap<Domain.Entities.PerfilResponsavel, PerfilResponsavelDTO>();


        }
    }
}