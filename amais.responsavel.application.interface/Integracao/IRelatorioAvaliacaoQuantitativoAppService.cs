﻿// 16:23 17 03 2018  AvaliacaoDiagnostica.Application.Interface  AvaliacaoDiagnostica.Solution

using System.Collections.Generic;
using AMais.Responsavel.DTO.Integracao;

namespace AMais.Responsavel.Application.Interface.Integracao
{
    public interface IRelatorioAvaliacaoQuantitativoAppService
    {
        IEnumerable<AvaliacaoQuantitativaDTO> ObterPorAvaliacao(AvaliacaoRelatorioPesquisaDTO pesquisaDTO);
        IEnumerable<AvaliacaoQuantitativaAreaDTO> ObterPorArea(AvaliacaoRelatorioPesquisaDTO pesquisaDTO);
        IEnumerable<AvaliacaoQuantitativaDisciplinaDTO> ObterPorDisciplina(AvaliacaoRelatorioPesquisaDTO pesquisaDTO);
        IEnumerable<AvaliacaoQuantitativaCorrecaoAvaliacaoDTO> ObterPorEstudanteComCorrecao(AvaliacaoRelatorioPesquisaDTO pesquisaDTO);
        IEnumerable<AvaliacaoQuantitativaDistribuicaoDTO> ObterPorDistribuicaoResposta(AvaliacaoRelatorioPesquisaDTO pesquisaDTO);
    }
}