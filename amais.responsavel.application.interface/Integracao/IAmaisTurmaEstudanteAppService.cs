﻿// AgendaDoEstudante - AMais.AgendaDoEstudante.Application.Interface - IIntegracaoAMaisTurma.cs
// 2018 / 09 / 24 : 18:17
// 2018 / 09 / 24 : 18:17

using AMais.Responsavel.DTO.Integracao;

namespace AMais.Responsavel.Application.Interface.Integracao
{
    public interface IAmaisTurmaEstudanteAppService
    {
        AMaisTurmaEstudanteDTO ObterTurmaEstudante(string token);
    }
}