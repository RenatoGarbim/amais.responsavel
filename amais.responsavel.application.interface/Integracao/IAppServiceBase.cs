﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Linte.Core.CrossCutting.Class;

namespace AMais.Responsavel.Application.Interface.Integracao
{
    public interface IAppServiceBase<TEntity, TEntityViewModel, TEntityPesquisaViewModel> where TEntity : class
        where TEntityViewModel : class
        where TEntityPesquisaViewModel : class

    {
        void Dispose();

        TEntity MapperViewModelParaEntity(TEntityViewModel obj);

        DataResult AdicionarOuAtualizar(TEntityViewModel obj);
        DataResult Remover(int registroId);
        DataResult Remover(Guid registroId);

        IQueryable<TEntity> DefineOrdernarPor(IQueryable<TEntity> registro, string ordenarPor, string direcaoOrdem);
        Expression<Func<TEntity, bool>> CreateFilter(TEntityPesquisaViewModel modelToFilter);
        IQueryable<TEntity> ApplyFilter(TEntityPesquisaViewModel modelToFilter, IQueryable<TEntity> registros);


        TEntityViewModel ObterPorId(int registroId);
        TEntityViewModel ObterPorId(Guid registroId);
        IEnumerable<TEntityViewModel> ObterTodos(string ordenarPor = null, string direcaoOrdem = null);
        IEnumerable<TEntityViewModel> ObterTodos(int pagina, string ordenarPor = null, string direcaoOrdem = null, TEntityPesquisaViewModel viewModelFiltro = null);

        int TotalDeRegistros();

    }
}
