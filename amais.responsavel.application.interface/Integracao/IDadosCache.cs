﻿using System.Collections.Generic;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.ValueObjects;

namespace AMais.Responsavel.Application.Interface.Integracao
{
    public interface IDadosCache
    {
        Dictionary<string, string> ObterTipoEndereco(); 
        Dictionary<string, string> ObterEstado();
        EnderecoDTO ObterPorCep(string cep);

        Dictionary<string, string> ObterMunicipio(string uf, string municipio);

        IEnumerable<EscolaDTO> ObterEscola();
        EscolaDTO ObterEscola(string pesquisaNome);
        IEnumerable<EscolaDTO> ObterEscola(string uf, string cidade, string nome);

    }
}