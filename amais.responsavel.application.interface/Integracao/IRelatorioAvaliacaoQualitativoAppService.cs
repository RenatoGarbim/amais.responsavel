﻿// 16:23 17 03 2018  AvaliacaoDiagnostica.Application.Interface  AvaliacaoDiagnostica.Solution

using System.Collections.Generic;
using AMais.Responsavel.DTO.Integracao;

namespace AMais.Responsavel.Application.Interface.Integracao
{
    public interface IRelatorioAvaliacaoQualitativoAppService
    {
        IEnumerable<AvaliacaoQualitativaDescritorDTO> ObterPorDescritor(AvaliacaoRelatorioPesquisaDTO pesquisaDTO);
    }
}