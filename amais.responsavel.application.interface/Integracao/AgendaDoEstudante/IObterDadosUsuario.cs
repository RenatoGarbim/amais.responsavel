﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Integracao.AgendaDoEstudante;

namespace AMais.Responsavel.Application.Interface.Integracao.AgendaDoEstudante
{
    public interface IObterDadosUsuario
    {
        IEnumerable<EstudanteDadosDTO> ObterDados();

    }
}
