﻿using System;
using System.Collections.Generic;
using AMais.AgendaDoEstudante.DTO;
using AMais.AgendaDoEstudante.DTO.ValueObjects;

namespace AMais.AgendaDoEstudante.Application.Interface
{
    public interface IDadosCache
    {
        Dictionary<string, string> ObterTipoTarefa();
        Dictionary<string, string> ObterSegmento();
        Dictionary<string, string> ObterAnoEscolar();
        Dictionary<string, string> ObterAnoEscolar(Guid segmentoId);

        Dictionary<string, string> ObterArea(string usuarioId);
        Dictionary<string, string> ObterDisciplina(string usuarioId);

        Dictionary<string, string> ObterTipoEndereco(); 
        Dictionary<string, string> ObterEstado();
        EnderecoDTO ObterPorCep(string cep);

        Dictionary<string, string> ObterMunicipio(string uf, string municipio);

        IEnumerable<EscolaDTO> ObterEscola();
        EscolaDTO ObterEscola(string pesquisaNome);
        IEnumerable<EscolaDTO> ObterEscola(string uf, string cidade, string nome);

    }
}