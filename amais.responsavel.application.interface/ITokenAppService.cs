﻿using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Pesquisa;

namespace AMais.Responsavel.Application.Interface
{
    public interface ITokenAppService : IAppServiceBase<Token, TokenDTO, TokenPesquisaDTO>
    {

    }
}
