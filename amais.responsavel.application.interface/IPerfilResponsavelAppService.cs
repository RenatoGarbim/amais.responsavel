﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Pesquisa;

namespace AMais.Responsavel.Application.Interface
{
    public interface IPerfilResponsavelAppService : IAppServiceBase<PerfilResponsavel, PerfilResponsavelDTO, ResponsavelPesquisaDTO>
    {
        bool CriarResponsavel(Guid userId);
        PerfilResponsavelDTO ObterDadosDoUsuario(string usuarioId);
    }

}
