﻿using System;
using System.Collections.Generic;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Pesquisa;
using Linte.Core.CrossCutting.Class;

namespace AMais.Responsavel.Application.Interface
{
    public interface IUsuarioAppService : IAppServiceBase<Usuario, UsuarioDTO, UsuarioPesquisaDTO>
    {
        IEnumerable<UsuarioDTO> ObterTodos(string ordenarPor = null, string direcaoOrdem = null);
        UsuarioDTO ObterDadosDoUsuario(string usuarioId);

        IEnumerable<UsuarioEnderecoViewModel> ObterListaEndereco(string usuarioId);
        UsuarioEnderecoViewModel ObterEndereco(string usuarioId, Guid usuarioEnderecoId);
        DataResult AdicionarOuAtualizarEndereco(string usuarioId, UsuarioEnderecoViewModel registro);
        DataResult RemoverEnderecoDoUsuario(Guid usuarioEnderecoId);

    }
}