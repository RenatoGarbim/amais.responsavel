﻿using System.Linq;
using AMais.Responsavel.Application.Interface;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Pesquisa;

namespace AMais.Responsavel.Application.Service
{
    public class TokenAppService : AppServiceBase<Token, TokenDTO, TokenPesquisaDTO>, ITokenAppService
    {
        private readonly ITokenService _service;

        public TokenAppService(ITokenService service) : base(service)
        {
            _service = service;
        }

        public override Token MapperViewModelParaEntity(TokenDTO obj)
        {
            var registro = new Token(obj.TokenId, obj.TokenGuid, obj.Descricao, obj.UsuarioId, obj.TurmaEstudanteId);
            return registro;
        }

        public override IQueryable<Token> ApplyFilter(TokenPesquisaDTO modelToFilter, IQueryable<Token> registros)
        {
            if (!string.IsNullOrEmpty(modelToFilter.Descricao))
                registros = registros.Where(x => x.Descricao.Contains(modelToFilter.Descricao));

            return base.ApplyFilter(modelToFilter, registros);
        }
    }
}