﻿using System;
using System.Collections.Generic;
using System.Linq;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.ValueObjects;
using Linte.Base.Cache.Entities;
using Linte.Base.Cache.Interfaces;

namespace AMais.Responsavel.Application.Service
{
    public class DadosCache : IDadosCache
    {
        private readonly IEstadoCache _estadoCache;
        private readonly IEscolaCache _escolaCache;
        private readonly IMunicipioCache _municipioCache;

        public DadosCache(IEstadoCache estadoCache, IEscolaCache escolaCache, IMunicipioCache municipioCache)
        {
            _estadoCache = estadoCache;
            _escolaCache = escolaCache;
            _municipioCache = municipioCache;
        }

        public Dictionary<string, string> ObterEstado()
        {
            var registros = _estadoCache.GetDataInCache();
            return registros;
        }

        public Dictionary<string, string> ObterMunicipio(string uf)
        {
            var registros = _municipioCache.GetDataInCache(uf);
            return registros;
        }

        public Dictionary<string, string> ObterMunicipio(string uf, string municipio)
        {
            var registros = _municipioCache.GetDataInCache(uf, municipio);
            return registros;
        }

        public IEnumerable<EscolaDTO> ObterEscola()
        {
            var registros = _escolaCache.GetDataInCache().ToList();
            return MapperToEscolaViewModel(registros);
        }

        public EscolaDTO ObterEscola(string pesquisaNome)
        {
            var registro = _escolaCache.GetDataInCache(pesquisaNome);

            return registro == null
                ? new EscolaDTO()
                : new EscolaDTO
                {
                    CodigoMec = registro.CodigoMec,
                    Nome = registro.Nome,
                };
        }

        public IEnumerable<EscolaDTO> ObterEscola(string uf, string cidade, string nome)
        {
            var registros = _escolaCache.GetDataInCache(uf, cidade, nome).ToList();
            return MapperToEscolaViewModel(registros);
        }

        private IEnumerable<EscolaDTO> MapperToEscolaViewModel(IEnumerable<Escola> registros)
        {
            return registros.Select(registro => new EscolaDTO
            {
                CodigoMec = registro.CodigoMec,
                Nome = registro.Nome,
                Endereco = new EnderecoDTO()
            }).ToList();
        }

        public Dictionary<string, string> ObterTipoEndereco()
        {
            //var registros = _tipoEnderecoService.GetAllRecords().ToList();
            //return registros.ToDictionary(tipoEndereco => tipoEndereco.TipoEnderecoId.ToString(), tipoEndereco => tipoEndereco.Descricao);

            throw new NotImplementedException();

        }

        public EnderecoDTO ObterPorCep(string cep)
        {
            //var registro = _enderecoService.GetAllRecords().FirstOrDefault(x => x.CEP.Equals(cep));
            //return registro != null ? new EnderecoViewModel() { Descricao = registro.Descricao, Bairro = registro.Bairro, Cidade = registro.Cidade, Estado = registro.Estado } : new EnderecoViewModel();

            throw new NotImplementedException();

        }
    }
}