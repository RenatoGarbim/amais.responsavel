﻿using System;
using System.Collections.Generic;
using System.Linq;
using AMais.Responsavel.Application.Interface;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Pesquisa;
using AutoMapper;
using Linte.Core.CrossCutting.Class;
using Linte.Core.DomainEvent.Events;

namespace AMais.Responsavel.Application.Service
{
    public class UsuarioAppService : AppServiceBase<Usuario, UsuarioDTO, UsuarioPesquisaDTO>, IUsuarioAppService
    {
        private readonly IUsuarioService _service;

        public UsuarioAppService(IUsuarioService service) : base(service)
        {
            _service = service;
        }

        public override Usuario MapperViewModelParaEntity(UsuarioDTO obj)
        {
            var registro = new Usuario(obj.UsuarioId, obj.Nome, obj.Sobrenome, obj.CaminhoImagem);
            return registro;
        }

        public override DataResult AdicionarOuAtualizar(UsuarioDTO obj)
        {
            //var registroAnterior = _service.GetRecordById(obj.UsuarioId);
            //obj.Estudante = registroAnterior.Estudante ?? false;
            //obj.Professor = registroAnterior.Professor ?? false;
            //obj.Tutor = registroAnterior.Tutor ?? false;

            var registro = MapperViewModelParaEntity(obj);

            if (registro.UsuarioId != Guid.Empty.ToString())
                return _service.UpdateRecord(registro);

            Notifications.Handle(new DomainNotification("OperacaoBloqueada",
                "Somente é permitido a criação de usuários pelo formulário de registro do login!"));
            return new DataResult() {Result = false};
        }

        public IEnumerable<UsuarioDTO> ObterTodos(string ordenarPor = null, string direcaoOrdem = null)
        {
            var registros = _service.GetAllRecords();

            TotalRegistros = registros.Count();

            if (!string.IsNullOrEmpty(ordenarPor))
                registros = DefineOrdernarPor(registros, ordenarPor, direcaoOrdem);

            return Mapper.Map<IEnumerable<UsuarioDTO>>(registros.ToList());
        }

        public UsuarioDTO ObterDadosDoUsuario(string usuarioId)
        {
            var registro = _service.GetRecordById(usuarioId);
            return Mapper.Map<UsuarioDTO>(registro);
        }

        public IEnumerable<UsuarioEnderecoViewModel> ObterListaEndereco(string usuarioId)
        {
            throw new NotImplementedException();
        }

        public UsuarioEnderecoViewModel ObterEndereco(string usuarioId, Guid usuarioEnderecoId)
        {
            throw new NotImplementedException();
        }

        public DataResult AdicionarOuAtualizarEndereco(string usuarioId, UsuarioEnderecoViewModel registro)
        {
            throw new NotImplementedException();
        }

        public DataResult RemoverEnderecoDoUsuario(Guid usuarioEnderecoId)
        {
            throw new NotImplementedException();
        }

    }
}