﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMais.Responsavel.Application.Interface;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Pesquisa;
using AutoMapper;
using Linte.Core.CrossCutting.Interfaces;

namespace AMais.Responsavel.Application.Service
{
    public class PerfilResponsavelAppService : AppServiceBase<PerfilResponsavel, PerfilResponsavelDTO, ResponsavelPesquisaDTO>, IPerfilResponsavelAppService
    {
        private readonly IPerfilResponsavelService _service;

        public PerfilResponsavelAppService(IPerfilResponsavelService service) : base(service)
        {
            _service = service;
        }

        public bool CriarResponsavel(Guid userId)
        {
            if (userId == Guid.Empty) return false;

            _service.InsertRecord(new Domain.Entities.PerfilResponsavel(Guid.NewGuid(), 0, null, null, null, null, null, null, userId));

            return Notifications.HasNotifications();
        }

        public PerfilResponsavelDTO ObterDadosDoUsuario(string usuarioId)
        {
            var registro = _service.ObterPor(usuarioId);

            return Mapper.Map<PerfilResponsavelDTO>(registro);
        }
    }
}
