﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AMais.Responsavel.Application.Interface.Integracao;
using AutoMapper;
using Linte.Core.CrossCutting.Class;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Domain.Query;
using Linte.Core.DomainEvent.Events;
using Linte.Core.DomainEvent.Interfaces;

namespace AMais.Responsavel.Application.Service
{
    public abstract class AppServiceBase<TEntity, TEntityViewModel, TEntityPesquisaViewModel> : IDisposable, IAppServiceBase<TEntity, TEntityViewModel, TEntityPesquisaViewModel> where TEntity : class where TEntityViewModel : class where TEntityPesquisaViewModel : class
    {
        //protected readonly string StrConexao = ConfigurationManager.ConnectionStrings["AmaisRelatorio"].ConnectionString;
        internal int TotalRegistros;

        //private readonly IUnitOfWork _unitOfWork;
        protected readonly IHandler<DomainNotification> Notifications;

        private readonly IServiceBase<TEntity> _serviceBase;
        protected AppServiceBase(IServiceBase<TEntity> serviceBase)
        {
            if (DomainEvent.Container != null)
                Notifications = DomainEvent.Container.GetInstance<IHandler<DomainNotification>>();
            _serviceBase = serviceBase;
        }

        public virtual DataResult AdicionarOuAtualizar(TEntityViewModel obj)
        {
            var entity = MapperViewModelParaEntity(obj);

            var propertyInfo = typeof(TEntity).GetProperty(typeof(TEntity).Name + "Id");
            if (propertyInfo != null)
            {
                var atualizaRegistro = propertyInfo.PropertyType == typeof(Guid) ? VerificarRegistroParaAtualizacaoChaveGuid(propertyInfo.GetValue(entity).ToString()) :
                    VerificarRegistroParaAtualizacaoChaveInt(propertyInfo.GetValue(entity).ToString());

                if (atualizaRegistro)
                    return _serviceBase.UpdateRecord(entity);

            }
            entity = DefinirRegistroId(entity);
            return _serviceBase.InsertRecord(entity);
        }

        public virtual IQueryable<TEntity> ApplyFilter(TEntityPesquisaViewModel modelToFilter, IQueryable<TEntity> registros)
        {
            return registros;
        }

        public virtual Expression<Func<TEntity, bool>> CreateFilter(TEntityPesquisaViewModel modelToFilter)
        {
            return null;
        }

        public virtual IQueryable<TEntity> DefineOrdernarPor(IQueryable<TEntity> registro, string ordenarPor, string direcaoOrdem)
        {
            if (!string.IsNullOrEmpty(direcaoOrdem) && direcaoOrdem == "DESC")
                return registro.OrderBy(string.Concat(ordenarPor, " Desc")).AsQueryable();

            return registro.OrderBy(string.Concat(ordenarPor, " Asc")).AsQueryable();
        }

        public void Dispose()
        {
            _serviceBase.Dispose();
            GC.SuppressFinalize(this);
        }

        public virtual TEntity MapperViewModelParaEntity(TEntityViewModel obj)
        {
            throw new NotImplementedException();
        }

        public TEntityViewModel ObterPorId(Guid registroId)
        {
            var registro = _serviceBase.GetRecordById(registroId);
            TotalRegistros = 1;
            return Mapper.Map<TEntityViewModel>(registro);
        }

        public TEntityViewModel ObterPorId(int registroId)
        {
            var registro = _serviceBase.GetRecordById(registroId);
            TotalRegistros = 1;
            return Mapper.Map<TEntityViewModel>(registro);
        }

        public IEnumerable<TEntityViewModel> ObterTodos(string ordenarPor = null, string direcaoOrdem = null)
        {
            var registros = _serviceBase.GetAllRecords();

            TotalRegistros = registros.Count();

            if (!string.IsNullOrEmpty(ordenarPor))
                registros = DefineOrdernarPor(registros, ordenarPor, direcaoOrdem);

            return Mapper.Map<IEnumerable<TEntityViewModel>>(registros.ToList());
        }

        public IEnumerable<TEntityViewModel> ObterTodos(int pagina, string ordenarPor = null, string direcaoOrdem = null, TEntityPesquisaViewModel viewModelFiltro = default(TEntityPesquisaViewModel))
        {
            var predicates = CreateFilter(viewModelFiltro);
            var registros = predicates == null ? ApplyFilter(viewModelFiltro, _serviceBase.GetAllRecords()) : ApplyFilter(viewModelFiltro, _serviceBase.GetAllRecords().AsExpandable().Where(predicates));

            TotalRegistros = registros.Count();

            registros = !string.IsNullOrEmpty(ordenarPor) ? DefineOrdernarPor(registros, ordenarPor, direcaoOrdem) : DefineOrdernarPor(registros, typeof(TEntity).Name + "Id", "asc");

            pagina = pagina == 0 ? 0 : (pagina - 1) * 15;

            if (TotalRegistros > 0 && pagina == TotalRegistros)
                pagina = pagina - 15;
            registros = registros.Skip(pagina).Take(15);

            return Mapper.Map<IEnumerable<TEntityViewModel>>(registros.ToList()); ;
        }

        public DataResult Remover(int registroId)
        {
            var registro = _serviceBase.GetRecordById(registroId);
            if (registro == null)
                Notifications.Handle(new DomainNotification("RegistroNaoLocalizado", "Registro não localizado para exclusão, possivelmente esse registro foi excluído por outro usuário ou sessão!"));

            return Notifications.HasNotifications() ?
                new DataResult() { Result = false, MessageException = Notifications.GetValues().Aggregate("", (current, validationError) => current + validationError.Value + Environment.NewLine) } : _serviceBase.RemoveRecord(registro);
        }

        public DataResult Remover(Guid registroId)
        {
            var registro = _serviceBase.GetRecordById(registroId);
            if (registro == null)
                Notifications.Handle(new DomainNotification("RegistroNaoLocalizado", "Registro não localizado para exclusão, possivelmente esse registro foi excluído por outro usuário ou sessão!"));

            return Notifications.HasNotifications() ?
                new DataResult() { Result = false, MessageException = Notifications.GetValues().Aggregate("", (current, validationError) => current + validationError.Value + Environment.NewLine) } : _serviceBase.RemoveRecord(registro);
        }

        public int TotalDeRegistros()
        {
            return TotalRegistros;
        }

        private int TotalDeRegistros(string sql)
        {
            //sql = $"SELECT COUNT(*) {sql.Substring(sql.IndexOf("FROM", StringComparison.Ordinal), sql.Length - sql.IndexOf("FROM", StringComparison.Ordinal))}";

            //using (var dbConnection = new MySqlConnection(StrConexao))
            //{
            //    dbConnection.Open();
            //    var resultado = dbConnection.Query<int>(sql);
            //    return resultado.FirstOrDefault();
            //}
            return 0;
        }

        private static TEntity DefinirRegistroId(TEntity obj)
        {
            var propertyInfo = obj.GetType().GetProperty(typeof(TEntity).Name + "Id");

            if (propertyInfo != null && propertyInfo.PropertyType == typeof(Guid))
                propertyInfo.SetValue(obj, Convert.ChangeType(Guid.NewGuid(), propertyInfo.PropertyType), null);

            return obj;
        }

        private static bool VerificarRegistroParaAtualizacaoChaveGuid(string value)
        {
            var id = Guid.Parse(value);

            return (id != Guid.Empty);
        }

        private static bool VerificarRegistroParaAtualizacaoChaveInt(string value)
        {
            var id = int.Parse(value);
            return id > 0;
        }

    }
}
