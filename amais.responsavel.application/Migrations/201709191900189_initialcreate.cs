namespace AMais.AgendaDoEstudante.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialcreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnoEscolar",
                c => new
                    {
                        AnoEscolarId = c.Guid(nullable: false),
                        SegmentoId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 20, unicode: false),
                        RegistroInterno = c.Boolean(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.AnoEscolarId)
                .ForeignKey("dbo.Segmento", t => t.SegmentoId)
                .Index(t => t.SegmentoId);
            
            CreateTable(
                "dbo.DisciplinaAnoEscolar",
                c => new
                    {
                        DisciplinaAnoEscolarId = c.Guid(nullable: false),
                        DisciplinaId = c.Guid(nullable: false),
                        AnoEscolarId = c.Guid(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.DisciplinaAnoEscolarId)
                .ForeignKey("dbo.AnoEscolar", t => t.AnoEscolarId)
                .ForeignKey("dbo.Disciplina", t => t.DisciplinaId)
                .Index(t => t.DisciplinaId)
                .Index(t => t.AnoEscolarId);
            
            CreateTable(
                "dbo.Disciplina",
                c => new
                    {
                        DisciplinaId = c.Guid(nullable: false),
                        UsuarioId = c.String(maxLength: 128, unicode: false),
                        AreaId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50, unicode: false),
                        DetalhamentoDisciplina = c.String(unicode: false),
                        RegistroInterno = c.Boolean(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.DisciplinaId)
                .ForeignKey("dbo.Area", t => t.AreaId)
                .ForeignKey("dbo.aspnetusers", t => t.UsuarioId)
                .Index(t => t.UsuarioId)
                .Index(t => t.AreaId);
            
            CreateTable(
                "dbo.Area",
                c => new
                    {
                        AreaId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50, unicode: false),
                        UsuarioId = c.String(maxLength: 128, unicode: false),
                        RegistroInterno = c.Boolean(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.AreaId)
                .ForeignKey("dbo.aspnetusers", t => t.UsuarioId)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.aspnetusers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, unicode: false),
                        Nome = c.String(maxLength: 200, unicode: false),
                        Sobrenome = c.String(maxLength: 200, unicode: false),
                        Telefone = c.String(maxLength: 20, unicode: false),
                        Celular = c.String(maxLength: 20, unicode: false),
                        CaminhoImagem = c.String(unicode: false),
                        DataNascimento = c.DateTime(precision: 0),
                        Estudante = c.Boolean(),
                        Professor = c.Boolean(),
                        Tutor = c.Boolean(),
                        ConvidadoPor = c.String(unicode: false),
                        Email = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tarefa",
                c => new
                    {
                        TarefaId = c.Guid(nullable: false),
                        UsuarioId = c.String(maxLength: 128, unicode: false),
                        TipoTarefaId = c.Guid(nullable: false),
                        DataInicio = c.DateTime(precision: 0),
                        DataTermino = c.DateTime(precision: 0),
                        DiaInteiro = c.Boolean(nullable: false),
                        HoraInicio = c.String(unicode: false),
                        HoraTermino = c.String(unicode: false),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                        NotaTarefa = c.String(maxLength: 300, unicode: false),
                        PercentualCompleto = c.Int(nullable: false),
                        Status = c.Boolean(nullable: false),
                        OrigemTarefa = c.String(maxLength: 25, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TarefaId)
                .ForeignKey("dbo.TipoTarefa", t => t.TipoTarefaId)
                .ForeignKey("dbo.aspnetusers", t => t.UsuarioId)
                .Index(t => t.UsuarioId)
                .Index(t => t.TipoTarefaId);
            
            CreateTable(
                "dbo.TarefaSub",
                c => new
                    {
                        TarefaSubId = c.Guid(nullable: false),
                        TarefaId = c.Guid(nullable: false),
                        TipoTarefaId = c.Guid(nullable: false),
                        DataInicio = c.DateTime(precision: 0),
                        DataTermino = c.DateTime(precision: 0),
                        DiaInteiro = c.Boolean(nullable: false),
                        HoraInicio = c.String(unicode: false),
                        HoraTermino = c.String(unicode: false),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                        NotaTarefa = c.String(maxLength: 300, unicode: false),
                        Status = c.Boolean(nullable: false),
                        PercentualCompleto = c.Int(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TarefaSubId)
                .ForeignKey("dbo.Tarefa", t => t.TarefaId)
                .ForeignKey("dbo.TipoTarefa", t => t.TipoTarefaId)
                .Index(t => t.TarefaId)
                .Index(t => t.TipoTarefaId);
            
            CreateTable(
                "dbo.TipoTarefa",
                c => new
                    {
                        TipoTarefaId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TipoTarefaId);
            
            CreateTable(
                "dbo.TurmaDisciplinaAulaTarefa",
                c => new
                    {
                        TurmaDisciplinaAulaTarefaId = c.Guid(nullable: false),
                        TurmaDisciplinaAulaId = c.Guid(nullable: false),
                        TarefaId = c.Guid(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaAulaTarefaId)
                .ForeignKey("dbo.Tarefa", t => t.TarefaId)
                .ForeignKey("dbo.TurmaDisciplinaAula", t => t.TurmaDisciplinaAulaId)
                .Index(t => t.TurmaDisciplinaAulaId)
                .Index(t => t.TarefaId);
            
            CreateTable(
                "dbo.TurmaDisciplinaAula",
                c => new
                    {
                        TurmaDisciplinaAulaId = c.Guid(nullable: false),
                        TurmaDisciplinaId = c.Guid(nullable: false),
                        Sala = c.String(maxLength: 50, unicode: false),
                        Professor = c.String(maxLength: 100, unicode: false),
                        Data = c.DateTime(precision: 0),
                        HoraInicio = c.String(maxLength: 8, unicode: false),
                        HoraTermino = c.String(maxLength: 8, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaAulaId)
                .ForeignKey("dbo.TurmaDisciplina", t => t.TurmaDisciplinaId)
                .Index(t => t.TurmaDisciplinaId);
            
            CreateTable(
                "dbo.TurmaDisciplina",
                c => new
                    {
                        TurmaDisciplinaId = c.Guid(nullable: false),
                        TurmaId = c.Guid(nullable: false),
                        DisciplinaId = c.Guid(),
                        Sala = c.String(maxLength: 50, unicode: false),
                        Professor = c.String(maxLength: 100, unicode: false),
                        DataInicio = c.DateTime(precision: 0),
                        DataTermino = c.DateTime(precision: 0),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaId)
                .ForeignKey("dbo.Disciplina", t => t.DisciplinaId)
                .ForeignKey("dbo.Turma", t => t.TurmaId)
                .Index(t => t.TurmaId)
                .Index(t => t.DisciplinaId);
            
            CreateTable(
                "dbo.Turma",
                c => new
                    {
                        TurmaId = c.Guid(nullable: false),
                        UsuarioId = c.String(nullable: false, maxLength: 128, unicode: false),
                        NomeTurma = c.String(maxLength: 50, unicode: false),
                        EscolaId = c.Guid(),
                        AnoEscolarId = c.Guid(),
                        WhatsUpGrupo = c.String(maxLength: 20, unicode: false),
                        EmailGrupo = c.String(maxLength: 254, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaId)
                .ForeignKey("dbo.AnoEscolar", t => t.AnoEscolarId)
                .ForeignKey("dbo.Escola", t => t.EscolaId)
                .ForeignKey("dbo.aspnetusers", t => t.UsuarioId)
                .Index(t => t.UsuarioId)
                .Index(t => t.EscolaId)
                .Index(t => t.AnoEscolarId);
            
            CreateTable(
                "dbo.Escola",
                c => new
                    {
                        EscolaId = c.Guid(nullable: false),
                        Nome = c.String(maxLength: 200, unicode: false),
                        CodigoMec = c.String(maxLength: 12, unicode: false),
                        Rua = c.String(maxLength: 200, unicode: false),
                        Numero = c.String(maxLength: 30, unicode: false),
                        Complemento = c.String(maxLength: 100, unicode: false),
                        Bairro = c.String(maxLength: 200, unicode: false),
                        Cidade = c.String(maxLength: 150, unicode: false),
                        Estado = c.String(maxLength: 2, unicode: false),
                        CEP = c.String(maxLength: 9, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.EscolaId);
            
            CreateTable(
                "dbo.UsuarioEscola",
                c => new
                    {
                        UsuarioEscolaId = c.Guid(nullable: false),
                        EscolaId = c.Guid(nullable: false),
                        UsuarioId = c.String(nullable: false, maxLength: 128, unicode: false),
                        Ativo = c.Boolean(nullable: false),
                        Telefone = c.String(maxLength: 20, unicode: false),
                        Celular = c.String(maxLength: 20, unicode: false),
                        Email = c.String(maxLength: 254, unicode: false),
                        DataMatricula = c.DateTime(precision: 0),
                        DataEncerramento = c.DateTime(precision: 0),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.UsuarioEscolaId)
                .ForeignKey("dbo.Escola", t => t.EscolaId)
                .ForeignKey("dbo.aspnetusers", t => t.UsuarioId)
                .Index(t => t.EscolaId)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.TurmaUsuario",
                c => new
                    {
                        TurmaUsuarioId = c.Guid(nullable: false),
                        TurmaId = c.Guid(nullable: false),
                        UsuarioId = c.String(nullable: false, maxLength: 128, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaUsuarioId)
                .ForeignKey("dbo.Turma", t => t.TurmaId)
                .ForeignKey("dbo.aspnetusers", t => t.UsuarioId)
                .Index(t => t.TurmaId)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.TurmaDisciplinaExame",
                c => new
                    {
                        TurmaDisciplinaExameId = c.Guid(nullable: false),
                        TurmaDisciplinaId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                        Data = c.DateTime(nullable: false, precision: 0),
                        HoraInicio = c.String(maxLength: 8, unicode: false),
                        HoraTermino = c.String(maxLength: 8, unicode: false),
                        Sala = c.String(maxLength: 50, unicode: false),
                        Nota = c.String(maxLength: 2, unicode: false),
                        Anotacao = c.String(maxLength: 300, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaExameId)
                .ForeignKey("dbo.TurmaDisciplina", t => t.TurmaDisciplinaId)
                .Index(t => t.TurmaDisciplinaId);
            
            CreateTable(
                "dbo.TurmaDisciplinaExameConteudo",
                c => new
                    {
                        TurmaDisciplinaExameConteudoId = c.Guid(nullable: false),
                        TurmaDisciplinaExameId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaExameConteudoId)
                .ForeignKey("dbo.TurmaDisciplinaExame", t => t.TurmaDisciplinaExameId)
                .Index(t => t.TurmaDisciplinaExameId);
            
            CreateTable(
                "dbo.TurmaDisciplinaExameConteudoTarefa",
                c => new
                    {
                        TurmaDisciplinaExameConteudoTarefaId = c.Guid(nullable: false),
                        TurmaDisciplinaExameConteudoId = c.Guid(nullable: false),
                        TarefaId = c.Guid(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaExameConteudoTarefaId)
                .ForeignKey("dbo.Tarefa", t => t.TarefaId)
                .ForeignKey("dbo.TurmaDisciplinaExameConteudo", t => t.TurmaDisciplinaExameConteudoId)
                .Index(t => t.TurmaDisciplinaExameConteudoId)
                .Index(t => t.TarefaId);
            
            CreateTable(
                "dbo.TurmaDisciplinaTrabalho",
                c => new
                    {
                        TurmaDisciplinaTrabalhoId = c.Guid(nullable: false),
                        TurmaDisciplinaId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                        DataEntrega = c.DateTime(nullable: false, precision: 0),
                        Nota = c.String(maxLength: 2, unicode: false),
                        BriefTrabalho = c.String(unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaTrabalhoId)
                .ForeignKey("dbo.TurmaDisciplina", t => t.TurmaDisciplinaId)
                .Index(t => t.TurmaDisciplinaId);
            
            CreateTable(
                "dbo.TurmaDisciplinaTrabalhoTarefa",
                c => new
                    {
                        TurmaDisciplinaTrabalhoTarefaId = c.Guid(nullable: false),
                        TurmaDisciplinaTrabalhoId = c.Guid(nullable: false),
                        TarefaId = c.Guid(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaTrabalhoTarefaId)
                .ForeignKey("dbo.Tarefa", t => t.TarefaId)
                .ForeignKey("dbo.TurmaDisciplinaTrabalho", t => t.TurmaDisciplinaTrabalhoId)
                .Index(t => t.TurmaDisciplinaTrabalhoId)
                .Index(t => t.TarefaId);
            
            CreateTable(
                "dbo.TurmaDisciplinaAulaAnotacao",
                c => new
                    {
                        TurmaDisciplinaAulaAnotacaoId = c.Guid(nullable: false),
                        TurmaDisciplinaAulaId = c.Guid(nullable: false),
                        Anotacao = c.String(maxLength: 300, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TurmaDisciplinaAulaAnotacaoId)
                .ForeignKey("dbo.TurmaDisciplinaAula", t => t.TurmaDisciplinaAulaId)
                .Index(t => t.TurmaDisciplinaAulaId);
            
            CreateTable(
                "dbo.UsuarioEndereco",
                c => new
                    {
                        UsuarioEnderecoId = c.Guid(nullable: false),
                        UsuarioId = c.String(maxLength: 128, unicode: false),
                        EnderecoId = c.Guid(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.UsuarioEnderecoId)
                .ForeignKey("dbo.Endereco", t => t.EnderecoId)
                .ForeignKey("dbo.aspnetusers", t => t.UsuarioId)
                .Index(t => t.UsuarioId)
                .Index(t => t.EnderecoId);
            
            CreateTable(
                "dbo.Endereco",
                c => new
                    {
                        EnderecoId = c.Guid(nullable: false),
                        TipoEnderecoId = c.Guid(nullable: false),
                        Logradouro = c.String(maxLength: 25, unicode: false),
                        Descricao = c.String(nullable: false, maxLength: 200, unicode: false),
                        Numero = c.String(maxLength: 30, unicode: false),
                        Complemento = c.String(maxLength: 100, unicode: false),
                        Bairro = c.String(maxLength: 200, unicode: false),
                        CodigoCidade = c.String(maxLength: 12, unicode: false),
                        Cidade = c.String(maxLength: 150, unicode: false),
                        Estado = c.String(maxLength: 2, unicode: false),
                        CEP = c.String(maxLength: 9, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.EnderecoId)
                .ForeignKey("dbo.TipoEndereco", t => t.TipoEnderecoId)
                .Index(t => t.TipoEnderecoId);
            
            CreateTable(
                "dbo.TipoEndereco",
                c => new
                    {
                        TipoEnderecoId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TipoEnderecoId)
                .Index(t => t.Descricao);
            
            CreateTable(
                "dbo.Segmento",
                c => new
                    {
                        SegmentoId = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 20, unicode: false),
                        OrdemExibicao = c.Int(nullable: false),
                        RegistroInterno = c.Boolean(nullable: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.SegmentoId);
            
            CreateTable(
                "dbo.Professor",
                c => new
                    {
                        ProfessorId = c.Guid(nullable: false),
                        UsuarioId = c.String(nullable: false, maxLength: 128, unicode: false),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        SobreNome = c.String(maxLength: 100, unicode: false),
                        Email = c.String(maxLength: 254, unicode: false),
                        RegistroDataCadastro = c.DateTime(nullable: false, precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.ProfessorId);

            AddColumn("aspnetusers", "Nome", c => c.String(maxLength: 200));
            AddColumn("aspnetusers", "Sobrenome", c => c.String(maxLength: 200));
            AddColumn("aspnetusers", "Telefone", c => c.String(maxLength: 16));
            AddColumn("aspnetusers", "Celular", c => c.String(maxLength: 16));
            AddColumn("aspnetusers", "DataNascimento", c => c.DateTime(nullable: null));
            AddColumn("aspnetusers", "Estudante", c => c.Boolean(defaultValue: false));
            AddColumn("aspnetusers", "Professor", c => c.Boolean(defaultValue: false));
            AddColumn("aspnetusers", "Tutor", c => c.Boolean(defaultValue: false));
            AddColumn("aspnetusers", "CaminhoImagem", c => c.String(maxLength:256));
            AddColumn("aspnetusers", "ConvidadoPor", c => c.String(maxLength: 256));
        }

        public override void Down()
        {
            DropForeignKey("dbo.AnoEscolar", "SegmentoId", "dbo.Segmento");
            DropForeignKey("dbo.DisciplinaAnoEscolar", "DisciplinaId", "dbo.Disciplina");
            DropForeignKey("dbo.UsuarioEndereco", "UsuarioId", "dbo.aspnetusers");
            DropForeignKey("dbo.UsuarioEndereco", "EnderecoId", "dbo.Endereco");
            DropForeignKey("dbo.Endereco", "TipoEnderecoId", "dbo.TipoEndereco");
            DropForeignKey("dbo.Tarefa", "UsuarioId", "dbo.aspnetusers");
            DropForeignKey("dbo.TurmaDisciplinaAulaTarefa", "TurmaDisciplinaAulaId", "dbo.TurmaDisciplinaAula");
            DropForeignKey("dbo.TurmaDisciplinaAulaAnotacao", "TurmaDisciplinaAulaId", "dbo.TurmaDisciplinaAula");
            DropForeignKey("dbo.TurmaDisciplinaTrabalhoTarefa", "TurmaDisciplinaTrabalhoId", "dbo.TurmaDisciplinaTrabalho");
            DropForeignKey("dbo.TurmaDisciplinaTrabalhoTarefa", "TarefaId", "dbo.Tarefa");
            DropForeignKey("dbo.TurmaDisciplinaTrabalho", "TurmaDisciplinaId", "dbo.TurmaDisciplina");
            DropForeignKey("dbo.TurmaDisciplinaExameConteudoTarefa", "TurmaDisciplinaExameConteudoId", "dbo.TurmaDisciplinaExameConteudo");
            DropForeignKey("dbo.TurmaDisciplinaExameConteudoTarefa", "TarefaId", "dbo.Tarefa");
            DropForeignKey("dbo.TurmaDisciplinaExameConteudo", "TurmaDisciplinaExameId", "dbo.TurmaDisciplinaExame");
            DropForeignKey("dbo.TurmaDisciplinaExame", "TurmaDisciplinaId", "dbo.TurmaDisciplina");
            DropForeignKey("dbo.TurmaDisciplinaAula", "TurmaDisciplinaId", "dbo.TurmaDisciplina");
            DropForeignKey("dbo.Turma", "UsuarioId", "dbo.aspnetusers");
            DropForeignKey("dbo.TurmaUsuario", "UsuarioId", "dbo.aspnetusers");
            DropForeignKey("dbo.TurmaUsuario", "TurmaId", "dbo.Turma");
            DropForeignKey("dbo.TurmaDisciplina", "TurmaId", "dbo.Turma");
            DropForeignKey("dbo.Turma", "EscolaId", "dbo.Escola");
            DropForeignKey("dbo.UsuarioEscola", "UsuarioId", "dbo.aspnetusers");
            DropForeignKey("dbo.UsuarioEscola", "EscolaId", "dbo.Escola");
            DropForeignKey("dbo.Turma", "AnoEscolarId", "dbo.AnoEscolar");
            DropForeignKey("dbo.TurmaDisciplina", "DisciplinaId", "dbo.Disciplina");
            DropForeignKey("dbo.TurmaDisciplinaAulaTarefa", "TarefaId", "dbo.Tarefa");
            DropForeignKey("dbo.TarefaSub", "TipoTarefaId", "dbo.TipoTarefa");
            DropForeignKey("dbo.Tarefa", "TipoTarefaId", "dbo.TipoTarefa");
            DropForeignKey("dbo.TarefaSub", "TarefaId", "dbo.Tarefa");
            DropForeignKey("dbo.Disciplina", "UsuarioId", "dbo.aspnetusers");
            DropForeignKey("dbo.Area", "UsuarioId", "dbo.aspnetusers");
            DropForeignKey("dbo.Disciplina", "AreaId", "dbo.Area");
            DropForeignKey("dbo.DisciplinaAnoEscolar", "AnoEscolarId", "dbo.AnoEscolar");
            DropIndex("dbo.TipoEndereco", new[] { "Descricao" });
            DropIndex("dbo.Endereco", new[] { "TipoEnderecoId" });
            DropIndex("dbo.UsuarioEndereco", new[] { "EnderecoId" });
            DropIndex("dbo.UsuarioEndereco", new[] { "UsuarioId" });
            DropIndex("dbo.TurmaDisciplinaAulaAnotacao", new[] { "TurmaDisciplinaAulaId" });
            DropIndex("dbo.TurmaDisciplinaTrabalhoTarefa", new[] { "TarefaId" });
            DropIndex("dbo.TurmaDisciplinaTrabalhoTarefa", new[] { "TurmaDisciplinaTrabalhoId" });
            DropIndex("dbo.TurmaDisciplinaTrabalho", new[] { "TurmaDisciplinaId" });
            DropIndex("dbo.TurmaDisciplinaExameConteudoTarefa", new[] { "TarefaId" });
            DropIndex("dbo.TurmaDisciplinaExameConteudoTarefa", new[] { "TurmaDisciplinaExameConteudoId" });
            DropIndex("dbo.TurmaDisciplinaExameConteudo", new[] { "TurmaDisciplinaExameId" });
            DropIndex("dbo.TurmaDisciplinaExame", new[] { "TurmaDisciplinaId" });
            DropIndex("dbo.TurmaUsuario", new[] { "UsuarioId" });
            DropIndex("dbo.TurmaUsuario", new[] { "TurmaId" });
            DropIndex("dbo.UsuarioEscola", new[] { "UsuarioId" });
            DropIndex("dbo.UsuarioEscola", new[] { "EscolaId" });
            DropIndex("dbo.Turma", new[] { "AnoEscolarId" });
            DropIndex("dbo.Turma", new[] { "EscolaId" });
            DropIndex("dbo.Turma", new[] { "UsuarioId" });
            DropIndex("dbo.TurmaDisciplina", new[] { "DisciplinaId" });
            DropIndex("dbo.TurmaDisciplina", new[] { "TurmaId" });
            DropIndex("dbo.TurmaDisciplinaAula", new[] { "TurmaDisciplinaId" });
            DropIndex("dbo.TurmaDisciplinaAulaTarefa", new[] { "TarefaId" });
            DropIndex("dbo.TurmaDisciplinaAulaTarefa", new[] { "TurmaDisciplinaAulaId" });
            DropIndex("dbo.TarefaSub", new[] { "TipoTarefaId" });
            DropIndex("dbo.TarefaSub", new[] { "TarefaId" });
            DropIndex("dbo.Tarefa", new[] { "TipoTarefaId" });
            DropIndex("dbo.Tarefa", new[] { "UsuarioId" });
            DropIndex("dbo.Area", new[] { "UsuarioId" });
            DropIndex("dbo.Disciplina", new[] { "AreaId" });
            DropIndex("dbo.Disciplina", new[] { "UsuarioId" });
            DropIndex("dbo.DisciplinaAnoEscolar", new[] { "AnoEscolarId" });
            DropIndex("dbo.DisciplinaAnoEscolar", new[] { "DisciplinaId" });
            DropIndex("dbo.AnoEscolar", new[] { "SegmentoId" });
            DropTable("dbo.Professor");
            DropTable("dbo.Segmento");
            DropTable("dbo.TipoEndereco");
            DropTable("dbo.Endereco");
            DropTable("dbo.UsuarioEndereco");
            DropTable("dbo.TurmaDisciplinaAulaAnotacao");
            DropTable("dbo.TurmaDisciplinaTrabalhoTarefa");
            DropTable("dbo.TurmaDisciplinaTrabalho");
            DropTable("dbo.TurmaDisciplinaExameConteudoTarefa");
            DropTable("dbo.TurmaDisciplinaExameConteudo");
            DropTable("dbo.TurmaDisciplinaExame");
            DropTable("dbo.TurmaUsuario");
            DropTable("dbo.UsuarioEscola");
            DropTable("dbo.Escola");
            DropTable("dbo.Turma");
            DropTable("dbo.TurmaDisciplina");
            DropTable("dbo.TurmaDisciplinaAula");
            DropTable("dbo.TurmaDisciplinaAulaTarefa");
            DropTable("dbo.TipoTarefa");
            DropTable("dbo.TarefaSub");
            DropTable("dbo.Tarefa");
            DropTable("dbo.aspnetusers");
            DropTable("dbo.Area");
            DropTable("dbo.Disciplina");
            DropTable("dbo.DisciplinaAnoEscolar");
            DropTable("dbo.AnoEscolar");
        }
    }
}
