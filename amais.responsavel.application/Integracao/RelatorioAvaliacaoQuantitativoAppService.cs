﻿// 20:31 15 03 2018  AvaliacaoDiagnostica.Application  AvaliacaoDiagnostica.Solution

using System.Collections.Generic;
using System.Linq;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.DTO.Integracao;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Infra.Data.Interfaces;
using Linte.Core.Infra.Data.ValueObject;

namespace AMais.Responsavel.Application.Integracao
{
    public class RelatorioAvaliacaoQuantitativoAppService : RelatorioAvaliacaoAppService, IRelatorioAvaliacaoQuantitativoAppService
    {
        private readonly IDirectDataAccess _directDataAccess;

        public RelatorioAvaliacaoQuantitativoAppService(IUserLogged userLogged, IDirectDataAccess directDataAccess, ITokenService tokenService) : base(userLogged, tokenService)
        {
            _directDataAccess = directDataAccess;
        }

        public IEnumerable<AvaliacaoQuantitativaDTO> ObterPorAvaliacao(AvaliacaoRelatorioPesquisaDTO pesquisaDTO)
        {

            const string sqlText =
                " select Escola.EscolaId, Turma.TurmaId, Avaliacao.AvaliacaoId, AvaliacaoItem.AvaliacaoItemId, concat(Avaliacao.Descricao, ' - ', Ano.Descricao, ' - ', TipoAvaliacao.Descricao, ' - ', TipoProva.Descricao) as DescricaoAvaliacao, " +
                " count(*) as TotalResposta, " +
                " count(distinct AvaliacaoResposta.TurmaEstudanteId) as TotalEstudante, " +
                " count(distinct Turma.TurmaId) as TotalTurma, " +
                " count(distinct Disciplina.DisciplinaId) as TotalDisciplina, " +
                " count(distinct Disciplina.AreaId) as TotalArea, " +
                " count(distinct AvaliacaoResposta.AvaliacaoRespostaId) as TotalQuestao, " +
                " count(distinct AvaliacaoResposta.AvaliacaoGabaritoId) as TotalProva, " +
                " count(distinct AvaliacaoResposta.AvaliacaoId) as TotalAvaliacao, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 1 Then 1 end) as TotalAcerto, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 0 Then 1 end) as TotalErro " +
                " from AvaliacaoResposta ";

            var groupBy = new List<GroupByToSql>
            {
                new GroupByToSql {GroupBy = "Escola.EscolaId"},
                new GroupByToSql {GroupBy = "Turma.TurmaId"},
                new GroupByToSql {GroupBy = "AvaliacaoItem.AvaliacaoItemId"}
            };

            var orderBy = new List<OrderToSql>
            {
                new OrderToSql {OrderBy = "Escola.Nome"},
                new OrderToSql {OrderBy = "Turma.Descricao"},
                new OrderToSql {OrderBy = "Avaliacao.Descricao"}
            };

            var filter = ApplySqlFilter(pesquisaDTO);
            var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            var registros = _directDataAccess.GetDirectDataAccess<AvaliacaoQuantitativaDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            return registros;
        }

        public IEnumerable<AvaliacaoQuantitativaAreaDTO> ObterPorArea(AvaliacaoRelatorioPesquisaDTO pesquisaDTO)
        {
            const string sqlText =
                " select AvaliacaoItem.AvaliacaoItemId, Area.CodigoInterno, Area.AreaId, Area.Descricao as DescricaoArea, concat(Avaliacao.Descricao, ' - ', Ano.Descricao, ' - ', TipoAvaliacao.Descricao, ' - ', TipoProva.Descricao) as DescricaoAvaliacao, " +
                " count(*) as TotalResposta, " +
                " count(distinct AvaliacaoResposta.TurmaEstudanteId) as TotalEstudante, " +
                " count(distinct AvaliacaoResposta.AvaliacaoId) as TotalAvaliacao, " +
                " count(distinct AvaliacaoResposta.AvaliacaoGabaritoId) as TotalQuestao, " + 
                " count(distinct Disciplina.DisciplinaId) as TotalDisciplina, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 1 Then 1 end) as TotalAcerto, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 0 Then 1 end) as TotalErro " +
                " from AvaliacaoResposta ";

            var groupBy = new List<GroupByToSql>
            {
                new GroupByToSql {GroupBy = "AvaliacaoItem.AvaliacaoItemId"},
                new GroupByToSql {GroupBy = "Area.CodigoInterno"},
                new GroupByToSql {GroupBy = "Area.AreaId"},
            };

            var orderBy = new List<OrderToSql>
            {
                new OrderToSql {OrderBy = "AvaliacaoItem.AvaliacaoItemId"},
                new OrderToSql {OrderBy = "Area.Descricao"},
            };

            var filter = ApplySqlFilter(pesquisaDTO);
            var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            var registros = _directDataAccess.GetDirectDataAccess<AvaliacaoQuantitativaAreaDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            return registros;
        }

        public IEnumerable<AvaliacaoQuantitativaDisciplinaDTO> ObterPorDisciplina(AvaliacaoRelatorioPesquisaDTO pesquisaDTO)
        {
            const string sqlText =
                " select  AvaliacaoItem.AvaliacaoItemId, Disciplina.DisciplinaId, Disciplina.Descricao as DescricaoDisciplina, Area.CodigoInterno, Area.AreaId, Area.Descricao as DescricaoArea, concat(Avaliacao.Descricao, ' - ', Ano.Descricao, ' - ', TipoAvaliacao.Descricao, ' - ', TipoProva.Descricao) as DescricaoAvaliacao, " +
                " count(*) as TotalResposta, " +
                " count(distinct AvaliacaoResposta.TurmaEstudanteId) as TotalEstudante, " +
                " count(distinct AvaliacaoResposta.AvaliacaoId) as TotalAvaliacao, " +
                " count(distinct AvaliacaoResposta.AvaliacaoGabaritoId) as TotalQuestao, " +
                " count(distinct Disciplina.DisciplinaId) as TotalDisciplina, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 1 Then 1 end) as TotalAcerto, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 0 Then 1 end) as TotalErro " +
                " from AvaliacaoResposta ";

            var groupBy = new List<GroupByToSql>
            {
                new GroupByToSql {GroupBy = "AvaliacaoItem.AvaliacaoItemId"},
                new GroupByToSql {GroupBy = "Disciplina.DisciplinaId"},
            };

            var orderBy = new List<OrderToSql>
            {
                new OrderToSql {OrderBy = "AvaliacaoItem.AvaliacaoItemId"},
                new OrderToSql {OrderBy = "Disciplina.CodigoInterno"},
            };

            var filter = ApplySqlFilter(pesquisaDTO);
            var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            var registros = _directDataAccess.GetDirectDataAccess<AvaliacaoQuantitativaDisciplinaDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            return registros;
        }

        public IEnumerable<AvaliacaoQuantitativaCorrecaoAvaliacaoDTO> ObterPorEstudanteComCorrecao(AvaliacaoRelatorioPesquisaDTO pesquisaDTO)
        {
            const string sqlText =
                " select TurmaEstudante.TurmaEstudanteId, TurmaEstudante.Nome, TurmaEstudante.Matricula, Area.CodigoInterno, Area.AreaId, Area.Descricao as DescricaoArea, concat(Avaliacao.Descricao, ' - ', Ano.Descricao, ' - ', TipoAvaliacao.Descricao, ' - ', TipoProva.Descricao) as DescricaoAvaliacao, " +
                " count(*) as TotalResposta, " +
                " count(distinct AvaliacaoResposta.TurmaEstudanteId) as TotalEstudante, " +
                " count(distinct AvaliacaoResposta.AvaliacaoGabaritoId) as TotalQuestao, " +
                " count(distinct Disciplina.DisciplinaId) as TotalDisciplina, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 1 Then 1 end) as TotalAcerto, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 0 Then 1 end) as TotalErro " +
                " from AvaliacaoResposta ";

            var groupBy = new List<GroupByToSql>
            {
                new GroupByToSql {GroupBy = "Area.AreaId"},
            };

            var orderBy = new List<OrderToSql>
            {
                new OrderToSql {OrderBy = "Disciplina.CodigoInterno"},
            };

            var filter = ApplySqlFilter(pesquisaDTO);
            var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            var registros = _directDataAccess.GetDirectDataAccess<AvaliacaoQuantitativaCorrecaoAvaliacaoDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            return registros;
            
        }

        public IEnumerable<AvaliacaoQuantitativaDistribuicaoDTO> ObterPorDistribuicaoResposta(AvaliacaoRelatorioPesquisaDTO pesquisaDTO)
        {
            const string sqlText =
                " select AvaliacaoResposta.AvaliacaoRespostaId, AvaliacaoGabarito.AvaliacaoGabaritoId, AvaliacaoGabarito.DescritorId, AvaliacaoItem.AvaliacaoItemId, AvaliacaoItem.AvaliacaoItemGuid, Ano.CodigoInterno as OrdemAno, Ano.Descricao as DescricaoAno, TipoAvaliacao.Descricao as DescricaoTipoAvaliacao, TipoProva.Descricao as DescricaoTipoProva, AvaliacaoGabarito.Questao, AvaliacaoGabarito.Gabarito, AvaliacaoResposta.Resposta, Area.Descricao as DescricaoArea, concat(Avaliacao.Descricao, ' - ', Ano.Descricao, ' - ', TipoAvaliacao.Descricao, ' - ', TipoProva.Descricao) as DescricaoAvaliacao, Disciplina.Descricao as DescricaoDisciplina " +
                " from AvaliacaoResposta ";

            var groupBy = new List<GroupByToSql>
            {
            };

            var orderBy = new List<OrderToSql>
            {
                new OrderToSql {OrderBy = "Ano.CodigoInterno, AvaliacaoItem.AvaliacaoItemId, AvaliacaoGabarito.Questao"},
            };

            var filter = ApplySqlFilter(pesquisaDTO);
            var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            var registros = _directDataAccess.GetDirectDataAccess<AvaliacaoQuantitativaDistribuicaoDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            return registros;

            
        }
    }
}