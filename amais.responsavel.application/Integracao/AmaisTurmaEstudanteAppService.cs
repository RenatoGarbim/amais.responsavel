﻿using System.Collections.Generic;
using System.Linq;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.DTO.Integracao;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Infra.Data.Interfaces;
using Linte.Core.Infra.Data.ValueObject;

namespace AMais.Responsavel.Application.Integracao
{
    public class AmaisTurmaEstudanteAppService : AmaisIntegracao, IAmaisTurmaEstudanteAppService
    {
        
        public AmaisTurmaEstudanteAppService(IUserLogged userLogged, IDirectDataAccess directDataAccess) : base(userLogged, directDataAccess)
        {
            
        }

        public AMaisTurmaEstudanteDTO ObterTurmaEstudante(string token)
        {
            const string sqlText =
                " select TurmaEstudante.TurmaEstudanteId, TurmaEstudante.TurmaEstudanteGuid, TurmaEstudante.TurmaId, Turma.Descricao as DescricaoTurma, AnoLetivo.Ano as AnoLetivo, Escola.Nome as NomeEscola, Ano.Descricao as DescricaoAno, TurmaEstudante.Matricula, TurmaEstudante.Nome as NomeEstudante, TurmaEstudante.Ativo, TurmaEstudante.TokenAcesso from turmaestudante " +
                " inner join turma on turma.turmaid=turmaestudante.turmaid " +
                " inner join escolaanoletivo on escolaanoletivo.escolaanoletivoid=turma.escolaanoletivoid" +
                " inner join anoletivo on anoletivo.anoletivoid=escolaanoletivo.anoletivoid" +
                " inner join escola on escola.escolaid=turma.escolaid " +
                " inner join ano on ano.anoid=turma.anoid ";

            var filter = new List<ParameterToSql>()
            {
                new ParameterToSql {ColumnName = "TokenAcesso", OperatorQuery = " = ", StringWhere = $"'{token}'"}
            };

            //var filter = ApplySqlFilter(pesquisaDTO);
            //var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            //var registros = _directDataAccess.GetDirectDataAccess<AvaliacaoQuantitativaDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            var registroTeste = DirectDataAccess.GetDirectDataAccess<AMaisTurmaEstudanteDTO>(StrConexao, sqlText, new List<InnerToSql>(), filter, new List<OrderToSql>(), new List<GroupByToSql>());

            return registroTeste.FirstOrDefault();
        }
    }
}
