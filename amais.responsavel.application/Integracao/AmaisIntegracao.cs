﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Infra.Data.Interfaces;
using Linte.Core.Infra.Data.ValueObject;

namespace AMais.Responsavel.Application.Integracao
{
    public abstract class AmaisIntegracao
    {
        internal readonly string StrConexao = ConfigurationManager.ConnectionStrings["AMais"].ConnectionString;

        internal readonly IUserLogged UserLogged;
        internal readonly IDirectDataAccess DirectDataAccess;

        protected AmaisIntegracao(IUserLogged userLogged, IDirectDataAccess directDataAccess)
        {
            UserLogged = userLogged;
            DirectDataAccess = directDataAccess;
        }

        public List<ParameterToSql> ApplySqlFilter()
        {
            var parameters = new List<ParameterToSql>()
            {
                new ParameterToSql()
            };
            
            ApplySqlUserAccessFilter().ForEach(x => parameters.Add(x));

            return parameters;
        }

        public List<ParameterToSql> ApplySqlUserAccessFilter()
        {
            var parameterSql = new List<ParameterToSql>();
            var usuario = UserLogged.GetUsuario();
            if (usuario.LinteAdministrador || usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("02-MAESTRO-ADMINISTRACAO")))
                return parameterSql;

            if (usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("03.1-ESCOLA-DIRETORIA")))
                parameterSql.Add(new ParameterToSql { ColumnName = "Escola.EscolaId", AndOr = " AND ", OperatorQuery = "IN", StringWhere = $"(Select distinct EscolaId from EscolaUsuario where UsuarioId = '{usuario.Id}')" });

            if (usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("03.2-ESCOLA-SECRETARIA")))
                parameterSql.Add(new ParameterToSql { ColumnName = "Escola.EscolaId", AndOr = " AND ", OperatorQuery = "IN", StringWhere = $"(Select distinct EscolaId from EscolaUsuario where UsuarioId = '{usuario.Id}')" });

            if (usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("03.3-ESCOLA.COORDENACAO")))
                parameterSql.Add(new ParameterToSql { ColumnName = "Escola.EscolaId", AndOr = " AND ", OperatorQuery = "IN", StringWhere = $"(Select distinct EscolaId from EscolaUsuario where UsuarioId = '{usuario.Id}')" });

            if (usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("03.4-ESCOLA-PROFESSOR")))
                parameterSql.Add(new ParameterToSql { ColumnName = "Escola.EscolaId", AndOr = " AND ", OperatorQuery = "IN", StringWhere = $"(Select distinct EscolaId from EscolaUsuario where UsuarioId = '{usuario.Id}')" });

            return parameterSql;

        }

        public List<InnerToSql> ApplyInner(string sql)
        {
            var inners = new List<InnerToSql>();

            if (sql.ToUpper().Contains("TURMAAVALIACAO."))
                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

            if (sql.ToUpper().Contains("TURMAESTUDANTE."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAAVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaEstudante", On = "TurmaEstudante.TurmaEstudanteId = AvaliacaoResposta.TurmaEstudanteId" });
            }

            if (sql.ToUpper().Contains("TURMA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAAVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAESTUDANTE")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaEstudante", On = "TurmaEstudante.TurmaEstudanteId = AvaliacaoResposta.TurmaEstudanteId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Turma", On = "Turma.TurmaId = TurmaEstudante.TurmaId" });
            }

            if (sql.ToUpper().Contains("ESCOLA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAAVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAESTUDANTE")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaEstudante", On = "TurmaEstudante.TurmaEstudanteId = AvaliacaoResposta.TurmaEstudanteId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMA")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Turma", On = "Turma.TurmaId = TurmaEstudante.TurmaId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Escola", On = "Escola.EscolaId = Turma.EscolaId" });
            }

            if (sql.ToUpper().Contains("AVALIACAOITEM."))
            {
                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });
            }

            if (sql.ToUpper().Contains("AVALIACAO."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });
                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Avaliacao", On = "Avaliacao.AvaliacaoId = AvaliacaoItem.AvaliacaoId" });
            }

            if (sql.ToUpper().Contains("TIPOAVALIACAO."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Avaliacao", On = "Avaliacao.AvaliacaoId = AvaliacaoItem.AvaliacaoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TipoAvaliacao", On = "TipoAvaliacao.TipoAvaliacaoId = AvaliacaoItem.TipoAvaliacaoId" });
            }

            if (sql.ToUpper().Contains("TIPOPROVA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Avaliacao", On = "Avaliacao.AvaliacaoId = AvaliacaoItem.AvaliacaoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TipoProva", On = "TipoProva.TipoProvaId = AvaliacaoItem.TipoProvaId" });
            }

            if (sql.ToUpper().Contains("AVALIACAOGABARITO."))
            {
                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoGabarito", On = "AvaliacaoGabarito.AvaliacaoGabaritoId = AvaliacaoResposta.AvaliacaoGabaritoId" });
            }

            if (sql.ToUpper().Contains("DISCIPLINA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOGABARITO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoGabarito", On = "AvaliacaoGabarito.AvaliacaoGabaritoId = AvaliacaoResposta.AvaliacaoGabaritoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Disciplina", On = "Disciplina.DisciplinaId = AvaliacaoGabarito.DisciplinaId" });
            }

            if (sql.ToUpper().Contains("AREA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOGABARITO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoGabarito", On = "AvaliacaoGabarito.AvaliacaoGabaritoId = AvaliacaoResposta.AvaliacaoGabaritoId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("DISCIPLINA")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Disciplina", On = "Disciplina.DisciplinaId = AvaliacaoGabarito.DisciplinaId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Area", On = "Area.AreaId = Disciplina.AreaId" });
            }

            if (sql.ToUpper().Contains("ANO."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Ano", On = "Ano.AnoId = AvaliacaoItem.AnoId" });
            }

            if (sql.ToUpper().Contains("SEGMENTO."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("ANO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Ano", On = "Ano.AnoId = AvaliacaoItem.AnoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Segmento", On = "Segmento.SegmentoId = Ano.SegmentoId" });
            }

            return inners;
        }


    }
}