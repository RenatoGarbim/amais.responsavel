﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AMais.Responsavel.Domain.Interfaces.Services;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Infra.Data.Interfaces;
using Linte.Core.Infra.Data.ValueObject;

namespace AMais.Responsavel.Application.Integracao.AgendaDoEstudante
{
    public abstract class AgendaDoEstudante
    {
        internal readonly string StrConexao = ConfigurationManager.ConnectionStrings["AgendaDoEstudante"].ConnectionString;

        internal readonly IUserLogged UserLogged;
        internal readonly IDirectDataAccess DirectDataAccess;
        internal readonly ITokenService _tokenService;
        
        protected AgendaDoEstudante(IUserLogged userLogged, IDirectDataAccess directDataAccess, ITokenService tokenService)
        {
            UserLogged = userLogged;
            DirectDataAccess = directDataAccess;
            _tokenService = tokenService;
        }

        public List<ParameterToSql> ApplySqlFilter()
        {
            var parameterSql = new List<ParameterToSql>();
            var usuario = UserLogged.GetUsuario();
            if (usuario.LinteAdministrador || usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("02-MAESTRO-ADMINISTRACAO")))
                return parameterSql;

            var listaTurma = _tokenService.GetAllRecords().Select(x => x.TurmaEstudanteId).Distinct().ToList();

            if (listaTurma.Count == 1)
                parameterSql.Add(new ParameterToSql()
                {
                    AndOr = " AND ",
                    ColumnName = "Token.TurmaEstudanteId",
                    OperatorQuery = "=",
                    StringWhere = listaTurma.SingleOrDefault().ToString()
                });
            else
                parameterSql.Add(new ParameterToSql()
                {
                    AndOr = " AND ",
                    ColumnName = "Token.TurmaEstudanteId",
                    OperatorQuery = "IN",
                    StringWhere = listaTurma.Aggregate(string.Empty, (p, q) => (p != string.Empty ? p + "," + q : q.ToString()))
                });

            return parameterSql;
        }

        public List<ParameterToSql> ApplySqlUserAccessFilter()
        {
            var parameterSql = new List<ParameterToSql>();
            var usuario = UserLogged.GetUsuario();
            if (usuario.LinteAdministrador || usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("02-MAESTRO-ADMINISTRACAO")))
                return parameterSql;

            if (usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("03.1-ESCOLA-DIRETORIA")))
                parameterSql.Add(new ParameterToSql { ColumnName = "Escola.EscolaId", AndOr = " AND ", OperatorQuery = "IN", StringWhere = $"(Select distinct EscolaId from EscolaUsuario where UsuarioId = '{usuario.Id}')" });

            if (usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("03.2-ESCOLA-SECRETARIA")))
                parameterSql.Add(new ParameterToSql { ColumnName = "Escola.EscolaId", AndOr = " AND ", OperatorQuery = "IN", StringWhere = $"(Select distinct EscolaId from EscolaUsuario where UsuarioId = '{usuario.Id}')" });

            if (usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("03.3-ESCOLA.COORDENACAO")))
                parameterSql.Add(new ParameterToSql { ColumnName = "Escola.EscolaId", AndOr = " AND ", OperatorQuery = "IN", StringWhere = $"(Select distinct EscolaId from EscolaUsuario where UsuarioId = '{usuario.Id}')" });

            if (usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("03.4-ESCOLA-PROFESSOR")))
                parameterSql.Add(new ParameterToSql { ColumnName = "Escola.EscolaId", AndOr = " AND ", OperatorQuery = "IN", StringWhere = $"(Select distinct EscolaId from EscolaUsuario where UsuarioId = '{usuario.Id}')" });

            return parameterSql;

        }

        public List<InnerToSql> ApplyInner(string sql)
        {
            var inners = new List<InnerToSql>();

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "aspnetusers", On = "aspnetusers.Id = token.UsuarioId" });

            return inners;
        }


    }
}