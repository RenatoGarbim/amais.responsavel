﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMais.Responsavel.Application.Interface.Integracao.AgendaDoEstudante;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Integracao.AgendaDoEstudante;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Infra.Data.Interfaces;
using Linte.Core.Infra.Data.ValueObject;

namespace AMais.Responsavel.Application.Integracao.AgendaDoEstudante
{
    public class ObterDadosUsuario : AgendaDoEstudante, IObterDadosUsuario
    {
        private readonly IDirectDataAccess _directDataAccess;

        public ObterDadosUsuario(IUserLogged userLogged, IDirectDataAccess directDataAccess, ITokenService tokenService) : base(userLogged, directDataAccess, tokenService)
        {
            _directDataAccess = directDataAccess;
        }

        public IEnumerable<EstudanteDadosDTO> ObterDados()
        {
            const string sqlText =
                " select token.TurmaEstudanteId, token.Descricao as Token, aspnetusers.CaminhoImagem" +
                " from token ";

            var groupBy = new List<GroupByToSql>
            {
            };

            var orderBy = new List<OrderToSql>
            {

            };

            var filter = ApplySqlFilter();
            var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            var registros = _directDataAccess.GetDirectDataAccess<EstudanteDadosDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            return registros;
        }
    }
}
