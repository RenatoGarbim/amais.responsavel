﻿// 20:31 15 03 2018  AvaliacaoDiagnostica.Application  AvaliacaoDiagnostica.Solution

using System.Collections.Generic;
using System.Linq;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.DTO.Integracao;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Infra.Data.Interfaces;
using Linte.Core.Infra.Data.ValueObject;

namespace AMais.Responsavel.Application.Integracao
{
    public class RelatorioAvaliacaoQualitativoAppService : RelatorioAvaliacaoAppService, IRelatorioAvaliacaoQualitativoAppService
    {
        private readonly IDirectDataAccess _directDataAccess;

        public RelatorioAvaliacaoQualitativoAppService(IUserLogged userLogged, IDirectDataAccess directDataAccess, ITokenService tokenService) : base(userLogged, tokenService)
        {
            _directDataAccess = directDataAccess;
        }

        public IEnumerable<AvaliacaoQualitativaDescritorDTO> ObterPorDescritor2(AvaliacaoRelatorioPesquisaDTO pesquisaDTO)
        {
            const string sqlText =
                " select AvaliacaoItem.AvaliacaoItemId, AvaliacaoGabarito.DescritorId, Area.AreaId, Area.CodigoInterno, Area.Descricao as DescricaoArea, Descritor.DescricaoDescritor, concat(Avaliacao.Descricao, ' - ', Ano.Descricao, ' - ', TipoAvaliacao.Descricao, ' - ', TipoProva.Descricao) as DescricaoAvaliacao, " +
                " count(*) as TotalResposta, " +
                " count(distinct AvaliacaoResposta.TurmaEstudanteId) as TotalEstudante, " +
                " count(distinct AvaliacaoResposta.AvaliacaoGabaritoId) as TotalQuestao, " +
                " count(distinct Disciplina.DisciplinaId) as TotalDisciplina, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 1 Then 1 end) as TotalAcerto, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 0 Then 1 end) as TotalErro " +
                " from AvaliacaoResposta ";

            var groupBy = new List<GroupByToSql>
            {
                new GroupByToSql {GroupBy = "AvaliacaoItem.AvaliacaoItemId"},
                new GroupByToSql {GroupBy = "AvaliacaoGabarito.DescritorId"},
                new GroupByToSql {GroupBy = "Area.AreaId"},
            };

            var orderBy = new List<OrderToSql>
            {
                new OrderToSql {OrderBy = "AvaliacaoItem.AvaliacaoItemId"},
                new OrderToSql {OrderBy = "Area.Descricao"},
            };

            var filter = ApplySqlFilter(pesquisaDTO);
            var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            var registros = _directDataAccess.GetDirectDataAccess<AvaliacaoQualitativaDescritorDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            return registros;
        }

        public IEnumerable<AvaliacaoQualitativaDescritorDTO> ObterPorDescritor(AvaliacaoRelatorioPesquisaDTO pesquisaDTO)
        {
            const string sqlText =
                " select AvaliacaoGabarito.DescritorId, Area.AreaId, Area.CodigoInterno, Area.Descricao as DescricaoArea, Descritor.DescricaoDescritor, " +
                " count(*) as TotalResposta, " +
                " count(distinct AvaliacaoResposta.TurmaEstudanteId) as TotalEstudante, " +
                " count(distinct AvaliacaoResposta.AvaliacaoGabaritoId) as TotalQuestao, " +
                " count(distinct Disciplina.DisciplinaId) as TotalDisciplina, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 1 Then 1 end) as TotalAcerto, " +
                " sum(case when AvaliacaoResposta.RespostaCorreta = 0 Then 1 end) as TotalErro " +
                " from AvaliacaoResposta ";

            var groupBy = new List<GroupByToSql>
            {
                new GroupByToSql {GroupBy = "AvaliacaoGabarito.DescritorId"},
                new GroupByToSql {GroupBy = "Area.AreaId"},
            };

            var orderBy = new List<OrderToSql>
            {
                new OrderToSql {OrderBy = "Area.Descricao"},
            };

            var filter = ApplySqlFilter(pesquisaDTO);
            var inner = ApplyInner($"{sqlText}{filter.Aggregate("", (current, item) => current + $"{item.ColumnName},")}");
            var registros = _directDataAccess.GetDirectDataAccess<AvaliacaoQualitativaDescritorDTO>(StrConexao, sqlText, inner, filter, orderBy, groupBy);

            return registros;
        }
    }
}