﻿// 16:57 17 03 2018  AvaliacaoDiagnostica.Application  AvaliacaoDiagnostica.Solution

using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.DTO.Integracao;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Infra.Data.ValueObject;

namespace AMais.Responsavel.Application.Integracao
{
    public abstract class RelatorioAvaliacaoAppService
    {
        protected readonly string StrConexao = ConfigurationManager.ConnectionStrings["Amais"].ConnectionString;
        private readonly IUserLogged _userLogged;
        private readonly ITokenService _tokenService;

        protected RelatorioAvaliacaoAppService(IUserLogged userLogged, ITokenService tokenService)
        {
            _userLogged = userLogged;
            _tokenService = tokenService;
        }

        public List<ParameterToSql> ApplySqlFilter(AvaliacaoRelatorioPesquisaDTO modelToFilter)
        {
            var parameters = new List<ParameterToSql>()
            {
                new ParameterToSql() {ColumnName = "AvaliacaoGabarito.Anulada", OperatorQuery = "=", StringWhere = "0"}
            };

            //ApplySqlUserAccessFilter().ForEach(x => parameters.Add(x));

            if (modelToFilter.TurmaEstudanteId != null)
            {
                modelToFilter.TurmaEstudanteId =
                    modelToFilter.TurmaEstudanteId.Where(x => !string.IsNullOrEmpty(x.ToString())).ToArray();

                if (modelToFilter.TurmaEstudanteId.Length == 1)
                    parameters.Add(new ParameterToSql()
                    {
                        AndOr = " AND ",
                        ColumnName = "TurmaEstudante.TurmaEstudanteId",
                        OperatorQuery = "=",
                        StringWhere = modelToFilter.TurmaEstudanteId[0].ToString()

                    });
                else if (modelToFilter.TurmaEstudanteId.Any())
                    parameters.Add(new ParameterToSql()
                    {
                        AndOr = " AND ",
                        ColumnName = "TurmaEstudante.TurmaEstudanteId",
                        OperatorQuery = "IN",
                        StringWhere = modelToFilter.TurmaEstudanteId.Aggregate(string.Empty,
                            (p, q) => (p != string.Empty ? p + "," + q : q.ToString()))
                    });
            }

            return parameters;
        }

        public List<ParameterToSql> ApplySqlUserAccessFilter()
        {
            var parameterSql = new List<ParameterToSql>();
            var usuario = _userLogged.GetUsuario();
            if (usuario.LinteAdministrador || usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("02-MAESTRO-ADMINISTRACAO")))
                return parameterSql;

            var listaTurma = _tokenService.GetAllRecords().Select(x => x.TurmaEstudanteId).Distinct().ToList();

            if (listaTurma.Count == 1)
                parameterSql.Add(new ParameterToSql()
                {
                    AndOr = " AND ",
                    ColumnName = "TurmaEstudante.TurmaEstudanteId",
                    OperatorQuery = "=",
                    StringWhere = listaTurma.SingleOrDefault().ToString()
                });
            else
                parameterSql.Add(new ParameterToSql()
                {
                    AndOr = " AND ",
                    ColumnName = "TurmaEstudante.TurmaEstudanteId",
                    OperatorQuery = "IN",
                    StringWhere = listaTurma.Aggregate(string.Empty, (p, q) => ( p != string.Empty ? p + "," + q : q.ToString() ))
                });

            return parameterSql;

        }

        public List<InnerToSql> ApplyInner(string sql)
        {
            var inners = new List<InnerToSql>();

            if (sql.ToUpper().Contains("TURMAAVALIACAO."))
                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

            if (sql.ToUpper().Contains("TURMAESTUDANTE."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAAVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaEstudante", On = "TurmaEstudante.TurmaEstudanteId = AvaliacaoResposta.TurmaEstudanteId" });
            }

            if (sql.ToUpper().Contains("TURMA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAAVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAESTUDANTE")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaEstudante", On = "TurmaEstudante.TurmaEstudanteId = AvaliacaoResposta.TurmaEstudanteId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Turma", On = "Turma.TurmaId = TurmaEstudante.TurmaId" });
            }

            if (sql.ToUpper().Contains("ESCOLA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAAVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAESTUDANTE")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaEstudante", On = "TurmaEstudante.TurmaEstudanteId = AvaliacaoResposta.TurmaEstudanteId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMA")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Turma", On = "Turma.TurmaId = TurmaEstudante.TurmaId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Escola", On = "Escola.EscolaId = Turma.EscolaId" });
            }

            if (sql.ToUpper().Contains("AVALIACAOITEM."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("TURMAAVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TurmaAvaliacao", On = "TurmaAvaliacao.TurmaAvaliacaoId = AvaliacaoResposta.TurmaAvaliacaoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });
            }

            if (sql.ToUpper().Contains("AVALIACAO."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });
                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Avaliacao", On = "Avaliacao.AvaliacaoId = AvaliacaoItem.AvaliacaoId" });
            }

            if (sql.ToUpper().Contains("TIPOAVALIACAO."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Avaliacao", On = "Avaliacao.AvaliacaoId = AvaliacaoItem.AvaliacaoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TipoAvaliacao", On = "TipoAvaliacao.TipoAvaliacaoId = AvaliacaoItem.TipoAvaliacaoId" });
            }

            if (sql.ToUpper().Contains("TIPOPROVA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Avaliacao", On = "Avaliacao.AvaliacaoId = AvaliacaoItem.AvaliacaoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "TipoProva", On = "TipoProva.TipoProvaId = AvaliacaoItem.TipoProvaId" });
            }

            if (sql.ToUpper().Contains("AVALIACAOGABARITO."))
            {
                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoGabarito", On = "AvaliacaoGabarito.AvaliacaoGabaritoId = AvaliacaoResposta.AvaliacaoGabaritoId" });
            }

            if (sql.ToUpper().Contains("DISCIPLINA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOGABARITO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoGabarito", On = "AvaliacaoGabarito.AvaliacaoGabaritoId = AvaliacaoResposta.AvaliacaoGabaritoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Disciplina", On = "Disciplina.DisciplinaId = AvaliacaoGabarito.DisciplinaId" });
            }

            if (sql.ToUpper().Contains("AREA."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOGABARITO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoGabarito", On = "AvaliacaoGabarito.AvaliacaoGabaritoId = AvaliacaoResposta.AvaliacaoGabaritoId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("DISCIPLINA")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Disciplina", On = "Disciplina.DisciplinaId = AvaliacaoGabarito.DisciplinaId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Area", On = "Area.AreaId = Disciplina.AreaId" });
            }

            if (sql.ToUpper().Contains("ANO."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Ano", On = "Ano.AnoId = AvaliacaoItem.AnoId" });
            }

            if (sql.ToUpper().Contains("SEGMENTO."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOITEM")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoItem", On = "AvaliacaoItem.AvaliacaoItemId = TurmaAvaliacao.AvaliacaoItemId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("ANO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Ano", On = "Ano.AnoId = AvaliacaoItem.AnoId" });

                inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Segmento", On = "Segmento.SegmentoId = Ano.SegmentoId" });
            }

            if (sql.ToUpper().Contains("DESCRITOR."))
            {
                if (!inners.Any(x => x.TableName.ToUpper().Equals("AVALIACAOGABARITO")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "AvaliacaoGabarito", On = "AvaliacaoGabarito.AvaliacaoGabaritoId = AvaliacaoResposta.AvaliacaoGabaritoId" });

                if (!inners.Any(x => x.TableName.ToUpper().Equals("DESCRITOR")))
                    inners.Add(new InnerToSql { InnerType = "INNER JOIN", TableName = "Descritor", On = "Descritor.DescritorId = AvaliacaoGabarito.DescritorId" });
            }


            return inners;
        }
    }
}