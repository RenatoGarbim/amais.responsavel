﻿using System;
using System.Collections.Generic;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.DTO;
using Linte.Core.Domain.Returns;

namespace AMais.AgendaDoEstudante.Application.Interfaces
{
    public interface ITurmaDisciplinaAppService : IAppServiceBase<TurmaDisciplina, TurmaDisciplinaViewModel>
    {
        IEnumerable<TurmaDisciplinaViewModel> ObterDisciplinas(Guid turmaId);
        IEnumerable<TurmaDisciplinaViewModel> ObterDisciplinasPorUsuario(Guid usuarioId);

        DataResult AdicionarAula(Guid turmaDisciplinaId, string sala, string professor, DateTime? data = null, string horaInicio = null, string horaTermino = null);
        DataResult AdicionarAnotacaoDeAula(Guid turmaDisciplinaAulaId, string anotacao);
        DataResult AdicionarTarefaDeAula(Guid turmaDisciplinaAulaId, Guid tipoTarefaId, string descricao, DateTime? dataInicio = null, DateTime? dataTermino = null, int percentualCompleto = 0, bool ativo = true);
        IEnumerable<TurmaDisciplinaAulaViewModel> ObterRegistrosDeAulas(Guid turmaDisciplinaId);
        IEnumerable<TurmaDisciplinaAulaAnotacaoViewModel> ObterRegistrosDeAnotacoesDeAulas(Guid turmaDisciplinaAulaId);
        IEnumerable<TurmaDisciplinaAulaTarefaViewModel> ObterRegistrosDeTarefasDeAulas(Guid turmaDisciplinaAulaId);

        DataResult AdicionarExame(Guid turmaDisciplinaId, DateTime data, string horaInicio, string horaTermino, string sala = null, string nota = null, string anotacao = null);
        DataResult AdicionarConteudoDoExame(Guid turmaDisciplinaExameId, string descricao);
        DataResult AdicionarTarefaDeConteudoDoExame(Guid turmaDisciplinaExameConteudoId, Guid tipoTarefaId, string descricao, DateTime? dataInicio = null, DateTime? dataTermino = null, int percentualCompleto = 0, bool ativo = true);
        TurmaDisciplinaExameViewModel ObterExame(Guid TurmaDisciplinaExameId);
        IEnumerable<TurmaDisciplinaExameViewModel> ObterRegistrosDeExames(Guid turmaDisciplinaId);
        IEnumerable<TurmaDisciplinaExameViewModel> ObterTodosExames(Guid usuarioId);
        IEnumerable<TurmaDisciplinaExameConteudoViewModel> ObterRegistrosDeConteudosDeExames(Guid turmaDisciplinaExameId);
        IEnumerable<TurmaDisciplinaExameConteudoTarefaViewModel> ObterRegistrosDeTarefasDeConteudosDeExames(Guid turmaDisciplinaExameConteudoId);

        DataResult AdicionarTrabalho(Guid turmaDisciplinaId, string descricao, DateTime dataEntrega, string nota = null);
        DataResult AdicionarTarefaDeTrabalho(Guid turmaDisciplinaTrabalhoId, Guid tipoTarefaId, string descricao, DateTime? dataInicio = null, DateTime? dataTermino = null, int percentualCompleto = 0, bool ativo = true);
        TurmaDisciplinaTrabalhoViewModel ObterRegistroTrabalho(Guid trabalhoId);
        IEnumerable<TurmaDisciplinaTrabalhoViewModel> ObterRegistrosDeTrabalhos(Guid turmaDisciplinaId);
        IEnumerable<TurmaDisciplinaTrabalhoTarefaViewModel> ObterRegistrosDeTarefasDeTrabalhos(Guid turmaDisciplinaTarefaId);
        IEnumerable<TurmaDisciplinaTrabalhoViewModel> ObterTrabalhosPorUsuario(Guid usuarioId);

    }
}