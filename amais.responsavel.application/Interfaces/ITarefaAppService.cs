﻿using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.DTO;
using Linte.Core.Domain.Returns;
using System;
using System.Collections.Generic;

namespace AMais.AgendaDoEstudante.Application.Interfaces
{
    public interface ITarefaAppService : IAppServiceBase<Tarefa, TarefaViewModel>
    {
        IEnumerable<TarefaSubViewModel> ObterSubTarefas(Guid tarefaId);
        DataResult AdicionarSubTarefa(Guid tarefaId, string descricao, Guid tipoTarefaId);
    }
}