﻿using System;
using System.Collections.Generic;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.DTO;
using Linte.Core.Domain.Returns;

namespace AMais.AgendaDoEstudante.Application.Interfaces
{
    public interface IUsuarioAppService : IAppServiceBase<Usuario, UsuarioViewModel>
    {
        DataResult Adicionar(Guid userId, string email);
        DataResult AdicionarEscola(Guid usuarioId, Guid escolaId, bool ativo, DateTime? dataMatricula = null, DateTime? dataEncerramento = null);
        DataResult AdicionarTurma(Guid usuarioId, string nomeTurma, Guid? escolaId = null, Guid? anoEscolarId = null);
        DataResult AdicionarAtualizarEndereco(Guid usuarioId, Guid enderecoId, string codigoCidade, string cidade, string estado, string logradouro, string complemento, string numero, string bairro, string cep);

        TurmaViewModel ObterRegistroTurma(Guid turmaId);
        EscolaViewModel ObterRegistroEscola(Guid escolaId);
        IEnumerable<EscolaUsuarioViewModel> ObterRegistrosEscolas(Guid usuarioId);
        IEnumerable<TurmaViewModel> ObterRegistrosTurmas(Guid turmaId);
        

    }
}