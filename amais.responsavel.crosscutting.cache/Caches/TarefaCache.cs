﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AMais.Responsavel.CrossCutting.Cache.Entities;
using AMais.Responsavel.CrossCutting.Cache.Events;
using AMais.Responsavel.CrossCutting.Cache.Interfaces;
using Dapper;
using Linte.Core.Infra.Cache;
using MySql.Data.MySqlClient;

namespace AMais.Responsavel.CrossCutting.Cache.Caches
{
    public class TarefaCache : CacheProvider<TarefaClearCacheEvent>, ITarefaCache
    {
        private readonly string _strConexao = ConfigurationManager.ConnectionStrings["Responsavel"].ConnectionString;

        public TarefaCache()
        {
            CacheName = "TarefaInCache";
        }

        public IEnumerable<GraficoTarefaMensal> GraficoTarefaMensal(string usuarioId, int ano, bool somentePendentes)
        {
            var cacheName = $"{CacheName}-{usuarioId}-Mensal-{ano}-{somentePendentes}";
            var dadosGrafico = Get(cacheName) as List<GraficoTarefaMensal>;

            if (dadosGrafico != null) return dadosGrafico;

            var registros = ObterDadosGraficoMensal(usuarioId, ano, somentePendentes);
            dadosGrafico = new List<GraficoTarefaMensal>();

            var graficoTarefaMensals = registros as GraficoTarefaMensal[] ?? registros.ToArray();
            if (!graficoTarefaMensals.Any())
            {
                dadosGrafico.Add(new GraficoTarefaMensal());
                return dadosGrafico;
            }

            dadosGrafico.AddRange(graficoTarefaMensals);
            Set(cacheName, dadosGrafico, 30000);

            return dadosGrafico;
        }
       

        public IEnumerable<GraficoTarefaAtividade> GraficoTarefaAtividade(string usuarioId, int ano)
        {
            var cacheName = $"{CacheName}-{usuarioId}-Atividade-{ano}";
            var dadosGrafico = Get(cacheName) as List<GraficoTarefaAtividade>;

            if (dadosGrafico != null) return dadosGrafico;

            var registros = ObterDadosGraficoAtividade(usuarioId, ano);
            dadosGrafico = new List<GraficoTarefaAtividade>();

            var graficoTarefaMensals = registros as GraficoTarefaAtividade[] ?? registros.ToArray();
            if (!graficoTarefaMensals.Any())
            {
                dadosGrafico.Add(new GraficoTarefaAtividade());
                return dadosGrafico;
            }

            dadosGrafico.AddRange(graficoTarefaMensals);
            Set(cacheName, dadosGrafico, 30000);

            return dadosGrafico;
        }

        private IEnumerable<GraficoTarefaAtividade> ObterDadosGraficoAtividade(string usuarioId, int ano)
        {
            try
            {
                var dbConnection = new MySqlConnection(_strConexao);
                var clausulaWhere = $" where usuarioId ='{usuarioId}' and year(datainicio) = {ano} ";
                dbConnection.OpenAsync();
                var resultado = dbConnection.QueryAsync<GraficoTarefaAtividade>(
                    " select tipotarefa.descricao as Atividade, count(*) as TotalRegistro from tarefa inner join tipotarefa on tipotarefa.tipotarefaid=tarefa.tipotarefaid " +
                    clausulaWhere +
                    " group by tipotarefa.descricao");
                return resultado.Result.ToList();
            }
            catch (Exception e)
            {
                return new List<GraficoTarefaAtividade>();
            }
        }


        private IEnumerable<GraficoTarefaMensal> ObterDadosGraficoMensal(string usuarioId, int ano, bool somentePendentes)
        {
            try
            {
                var dbConnection = new MySqlConnection(_strConexao);
                var clausulaWhere = somentePendentes ? 
                    $" where usuarioId ='{usuarioId}' and year(datainicio) = {ano}  and status=0 " : 
                    $" where usuarioId ='{usuarioId}' and year(datainicio) = {ano} ";
                
                dbConnection.OpenAsync();
                var resultado = dbConnection.QueryAsync<GraficoTarefaMensal>(
                    " select case month(datainicio) " +
                    " when 1 then 'Janeiro' " +
                    " when 2 then 'Fevereiro' " +
                    " when 3 then 'Março' " +
                    " when 4 then 'Abril' " +
                    " when 5 then 'Maio' " +
                    " when 6 then 'Junho' " +
                    " when 7 then 'Julho' " +
                    " when 8 then 'Agosto' " +
                    " when 9 then 'Setembro' " +
                    " when 10 then 'Outubro' " +
                    " when 11 then 'Novembro' " +
                    " when 12 then 'Dezembro' " +
                    " end as mes, count(*) as TotalRegistro from tarefa " +
                    clausulaWhere +
                    "group by month(datainicio)");
                return resultado.Result.ToList();
            }
            catch (Exception e)
            {
                return new List<GraficoTarefaMensal>();
            }
        }
    }
}