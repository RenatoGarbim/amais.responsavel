﻿using System.Collections.Generic;
using AMais.Responsavel.CrossCutting.Cache.Entities;

namespace AMais.Responsavel.CrossCutting.Cache.Interfaces
{
    public interface ITarefaCache
    {
        IEnumerable<GraficoTarefaMensal> GraficoTarefaMensal(string usuarioId, int ano, bool somentePendentes);
        IEnumerable<GraficoTarefaAtividade> GraficoTarefaAtividade(string usuarioId, int ano);
    }
}