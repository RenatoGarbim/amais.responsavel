﻿using System;
using Linte.Core.DomainEvent.Interfaces;

namespace AMais.Responsavel.CrossCutting.Cache.Events
{
    public class TarefaClearCacheEvent : IDomainEvent
    {
        public TarefaClearCacheEvent(int versao, DateTime dataOcorrencia)
        {
            Versao = versao;
            DataOcorrencia = dataOcorrencia;
        }

        public TarefaClearCacheEvent() : this(1, DateTime.Now)
        {
        }

        public int Versao { get; }
        public DateTime DataOcorrencia { get; }
    }
}