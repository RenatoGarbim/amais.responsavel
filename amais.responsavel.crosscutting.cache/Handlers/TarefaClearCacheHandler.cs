﻿using AMais.Responsavel.CrossCutting.Cache.Events;
using Linte.Core.Infra.Cache;

namespace AMais.Responsavel.CrossCutting.Cache.Handlers
{
    public class TarefaClearCacheHandler : ClearCacheHandler<TarefaClearCacheEvent>
    {
        public TarefaClearCacheHandler()
        {
            CacheName = "TarefaInCache";
        }

    }
}