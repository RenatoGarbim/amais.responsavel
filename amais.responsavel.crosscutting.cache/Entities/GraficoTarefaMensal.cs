﻿namespace AMais.Responsavel.CrossCutting.Cache.Entities
{
    public class GraficoTarefaMensal
    {
        public string Mes { get; set; }
        public int TotalRegistro { get; set; }
    }
}