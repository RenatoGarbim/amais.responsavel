﻿// 15:2107-02-2017-

using AMais.Responsavel.Application.Integracao;
using AMais.Responsavel.Application.Integracao.AgendaDoEstudante;
using AMais.Responsavel.Application.Interface;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.Application.Interface.Integracao.AgendaDoEstudante;
using AMais.Responsavel.Application.Service;
using SimpleInjector;

namespace AMais.Responsavel.CrossCutting.IoC.Modules
{
    public class ApplicationModule
    {
        public static void SetModules(Container container)
        {
            //container.Register(typeof(IAppServiceBase<,>), typeof(AppServiceBase<,>), Lifestyle.Scoped);

            container.Register<IUsuarioAppService, UsuarioAppService>(Lifestyle.Scoped);

            container.Register<ITokenAppService, TokenAppService>(Lifestyle.Scoped);

            container.Register<IPerfilResponsavelAppService, PerfilResponsavelAppService>(Lifestyle.Scoped);

            container.Register<IDadosCache, DadosCache>(Lifestyle.Scoped);

            container.Register<IAmaisTurmaEstudanteAppService, AmaisTurmaEstudanteAppService>(Lifestyle.Scoped);

            container.Register<IObterDadosUsuario, ObterDadosUsuario>(Lifestyle.Scoped);

            container.Register<IRelatorioAvaliacaoQuantitativoAppService, RelatorioAvaliacaoQuantitativoAppService>(Lifestyle.Scoped);
            container.Register<IRelatorioAvaliacaoQualitativoAppService, RelatorioAvaliacaoQualitativoAppService>(Lifestyle.Scoped);
        }
    }
}