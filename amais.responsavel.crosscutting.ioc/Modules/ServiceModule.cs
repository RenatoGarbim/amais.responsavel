﻿// 15:1907-02-2017-

using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.Domain.Services;
using SimpleInjector;

namespace AMais.Responsavel.CrossCutting.IoC.Modules
{
    public class ServiceModule
    {

        public static void SetModules(Container container)
        {

            container.Register<IUsuarioService, UsuarioService>(Lifestyle.Scoped);
            container.Register<ITokenService, TokenService>(Lifestyle.Scoped);
            container.Register<IPerfilResponsavelService, PerfilResponsavelService>(Lifestyle.Scoped);

        }
    }
}