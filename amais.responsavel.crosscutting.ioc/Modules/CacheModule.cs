﻿using AMais.Responsavel.CrossCutting.Cache.Caches;
using AMais.Responsavel.CrossCutting.Cache.Events;
using AMais.Responsavel.CrossCutting.Cache.Handlers;
using AMais.Responsavel.CrossCutting.Cache.Interfaces;
using Linte.Base.Cache;
using Linte.Base.Cache.Interfaces;
using Linte.Core.DomainEvent.Interfaces;
using SimpleInjector;

namespace AMais.Responsavel.CrossCutting.IoC.Modules
{
    public class CacheModule
    {
        public static void SetModules(Container container)
        {
            container.Register<IEstadoCache, EstadoCache>(Lifestyle.Scoped);
            container.Register<IMunicipioCache, MunicipioCache>(Lifestyle.Scoped);
            container.Register<IEscolaCache, EscolaCache>(Lifestyle.Scoped);
            container.Register<ITarefaCache, TarefaCache>(Lifestyle.Scoped);

            container.Register<IHandler<TarefaClearCacheEvent>, TarefaClearCacheHandler>(Lifestyle.Singleton);

        }
    }
}