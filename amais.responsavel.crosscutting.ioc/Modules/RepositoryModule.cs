﻿// 15:1507-02-2017-

using AMais.Responsavel.Infra.Data;
using AMais.Responsavel.Infra.Data.Repositories;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using Linte.Core.CrossCutting.Interfaces;
using SimpleInjector;

namespace AMais.Responsavel.CrossCutting.IoC.Modules
{
    public class RepositoryModule
    {
        public static void SetModules(Container container)
        {
            container.Register(typeof(IRepositoryBase<>), typeof(RepositoryBase<>), Lifestyle.Scoped);

            container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Scoped);
            container.Register<ITokenRepository, TokenRepository>(Lifestyle.Scoped);
            container.Register<IPerfilResponsavelRepository, ResponsavelRepository>(Lifestyle.Scoped);
        }

    }
}