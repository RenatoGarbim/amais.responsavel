﻿using AMais.Responsavel.CrossCutting.IoC.Modules;
using SimpleInjector;

namespace AMais.Responsavel.CrossCutting.IoC
{
    public static class ResponsavelBootStrapper
    {
        public static void RegisterServices(Container container)
        {
            RepositoryModule.SetModules(container);
            ServiceModule.SetModules(container);
            ApplicationModule.SetModules(container);
            CacheModule.SetModules(container);
        }
        
    }
}