﻿namespace AMais.Responsavel.Domain.Entities.ValueObjects
{
    public class Endereco
    {
        public string Uf { get; private set; }
        public string Cidade { get; private set; }
        public string Rua { get; private set; }
        public string Numero { get; private set; }
        public string Complemento { get; private set; }
        public string Bairro { get; private set; }
        public string Cep { get; private set; }
        public string Telefone { get; private set; }

        public Endereco()
        {
                
        }

        public Endereco(string uf, string cidade, string rua, string numero, string complemento, string bairro, string cep, string telefone)
        {
            Uf = uf;
            Cidade = cidade;
            Rua = rua;
            Numero = numero;
            Complemento = complemento;
            Bairro = bairro;
            Cep = cep;
            Telefone = telefone;
        }
    }
}