﻿using System;
using AMais.Responsavel.Domain.Entities.Scopes;
using Linte.Core.Domain.Entities;

namespace AMais.Responsavel.Domain.Entities
{
    public class Token : EntitiesCamposComuns
    {
        public int TokenId { get; private set; }
        public Guid TokenGuid { get; private set; }
        public string Descricao { get; private set; }
        public Guid UsuarioId { get; private set; }
        public int TurmaEstudanteId { get; set; }

        public Token()
        {

        }

        public Token(int tokenId, Guid tokenGuid, string descricao, Guid usuarioId, int turmaEstudanteId)
        {
            TokenId = tokenId;
            TokenGuid = tokenGuid;
            Descricao = descricao;
            UsuarioId = usuarioId;
            TurmaEstudanteId = turmaEstudanteId;
        }

        public override bool EntidadeEhValida()
        {
            ValidarEntidade(this.CampoTokenGuidEhValido(TokenGuid));
            ValidarEntidade(this.CampoUsuarioIdEhValido(UsuarioId));
            ValidarEntidade(this.CampoDescricaoEhValido(Descricao));
            ValidarEntidade(this.CampoTurmaEstudanteIdEhValido(TurmaEstudanteId));
            return base.EntidadeEhValida();
        }

        public void DefinirCampoUsuario(Guid value)
        {
            UsuarioId = value;
        }
    }
}
