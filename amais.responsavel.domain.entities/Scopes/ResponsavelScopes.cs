﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linte.Core.Domain.ValueObjects;

namespace AMais.Responsavel.Domain.Entities.Scopes
{
    public static class ResponsavelScopes
    {
        public static bool CampoResponsavelGuidEhValido(this PerfilResponsavel entity, Guid value)
        {
            return entity.ValidarEntidade(AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNotNullOrEmpty(value.ToString(), "O campo Guid de identificação do registro não é válido"),
                AssertionConcern.AssertAreNotEquals(value.ToString(), "00000000-0000-0000-0000-000000000000", "O campo Guid de identificação do registro não é válido")));
        }

    }
}
