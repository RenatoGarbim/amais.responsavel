﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linte.Core.Domain.ValueObjects;

namespace AMais.AgendaDoEstudante.Domain.Entities.Scopes
{
    public static class EnderecoScopes
    {
        public static bool CampoEnderecoGuidEhValido(this Endereco entity, Guid value)
        {
            return entity.ValidarEntidade(AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNotNullOrEmpty(value.ToString(), "O campo de identificação do registro não é válido"),
                AssertionConcern.AssertAreNotEquals(value.ToString(), "00000000-0000-0000-0000-000000000000", "O campo de identificação do registro não é válido")));
        }

        public static bool CampoDescricaoEhValido(this Endereco entity, string value)
        {
            return entity.ValidarEntidade(AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNotNullOrEmpty(value, "Por favor, preencha a descrição da área.")));
        }
    }
}
