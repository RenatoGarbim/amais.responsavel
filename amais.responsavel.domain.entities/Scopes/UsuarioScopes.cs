﻿using Linte.Core.Domain.ValueObjects;

namespace AMais.Responsavel.Domain.Entities.Scopes
{
    public static class UsuarioScopes
    {
        public static bool CampoUsuarioIdEhValido(this Usuario entity, string value)
        {
            return entity.ValidarEntidade(AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNotNullOrEmpty(value, "O campo de identificação do registro não é válido"),
                AssertionConcern.AssertAreNotEquals(value, "00000000-0000-0000-0000-000000000000", "O campo de identificação do registro não é válido")));
        }
        public static bool CampoNomeEhValido(this Usuario entity, string descricao)
        {
            return AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNotNullOrEmpty(descricao, "Por favor, preencha o nome do usuário corretamente"));
        }

    }
}