﻿using System;
using Linte.Core.Domain.ValueObjects;

namespace AMais.Responsavel.Domain.Entities.Scopes
{
    public static class TokenScopes
    {
        public static bool CampoTokenGuidEhValido(this Token entity, Guid value)
        {
            return entity.ValidarEntidade(AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNotNullOrEmpty(value.ToString(), "O campo Guid de identificação do registro não é válido"),
                AssertionConcern.AssertAreNotEquals(value.ToString(), "00000000-0000-0000-0000-000000000000", "O campo Guid de identificação do registro não é válido")));
        }

        public static bool CampoDescricaoEhValido(this Token entity, string value)
        {
            return entity.ValidarEntidade(AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNotNullOrEmpty(value, "Por favor, preencha a descrição da área.")));
        }

        public static bool CampoUsuarioIdEhValido(this Token entity, Guid value)
        {
            return entity.ValidarEntidade(AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNotNullOrEmpty(value.ToString(), "O campo Guid de identificação do usuário não é válido"),
                AssertionConcern.AssertAreNotEquals(value.ToString(), "00000000-0000-0000-0000-000000000000", "O campo Guid de identificação do usuário não é válido")));
        }

        public static bool CampoTurmaEstudanteIdEhValido(this Token entity, int value)
        {
            return entity.ValidarEntidade(AssertionConcern.IsSatisfiedBy(AssertionConcern.AssertNumberIsMoreOrEqualsThan(value, 1, "O campo de identificação da turma não é válido")));
        }

    }
}
