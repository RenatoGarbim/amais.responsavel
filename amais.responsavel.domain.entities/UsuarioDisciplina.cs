﻿using Linte.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMais.AgendaDoEstudante.Domain.Entities
{
    public class UsuarioDisciplina : EntitiesCamposComuns
    {
        public Guid UsuarioDisciplinaId { get; private set; }

        public Guid DisciplinaId { get; private set; }
        public Guid UsuarioId { get; private set; }

        public string DisciplinaNome { get; private set; }
        public string DiscplinaArea { get; private set; }


        public UsuarioDisciplina()
        {
            EntidadeValida = false;
        }

        public UsuarioDisciplina(Guid usuarioDisciplinaId, Guid disciplinaId, Guid usuarioId, string disciplinaNome, string disciplinaArea)
        {
            UsuarioDisciplinaId = usuarioDisciplinaId;
            DisciplinaId = disciplinaId;
            UsuarioId = usuarioId;
            DisciplinaNome = disciplinaNome;
            DiscplinaArea = disciplinaArea;
        }

        public override bool EntidadeEhValida()
        {
            return base.EntidadeEhValida();
        }
    }
}
