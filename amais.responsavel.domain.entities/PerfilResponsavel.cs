﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMais.Responsavel.Domain.Entities.Scopes;
using Linte.Core.Domain.Entities;

namespace AMais.Responsavel.Domain.Entities
{
    public class PerfilResponsavel : EntitiesCamposComuns
    {
        public Guid PerfilResponsavelGuid { get; private set; }
        public int PerfilResponsavelId { get; private set; }

        public string Nome { get; private set; }

        public string Sobrenome { get; private set; }

        public string CaminhoImagem { get; private set; }

        public string CPF { get; private set; }
        public string Telefone { get; private set; }
        public string Celular { get; private set; }

        public Guid UsuarioId { get; private set; }

        public PerfilResponsavel()
        {
            
        }

        public PerfilResponsavel(Guid perfilResponsavelGuid, int perfilResponsavelId, string nome, string sobrenome, string caminhoImagem, string cpf, string telefone, string celular, Guid usuarioId)
        {
            PerfilResponsavelGuid = perfilResponsavelGuid;
            PerfilResponsavelId = perfilResponsavelId;
            Nome = nome;
            Sobrenome = sobrenome;
            CaminhoImagem = caminhoImagem;
            CPF = cpf;
            Telefone = telefone;
            Celular = celular;
            UsuarioId = usuarioId;
        }


        public override bool EntidadeEhValida()
        {
            ValidarEntidade(this.CampoResponsavelGuidEhValido(PerfilResponsavelGuid));
         
            return base.EntidadeEhValida();
        }
    }
}
