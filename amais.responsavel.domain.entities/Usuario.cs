﻿using System.Collections.Generic;
using AMais.Responsavel.Domain.Entities.Scopes;
using Linte.Core.Domain.Services;

namespace AMais.Responsavel.Domain.Entities
{
    public class Usuario : ServiceEntity
    {
        public string UsuarioId { get; private set; }

        public string Nome { get; private set; }
        public string Sobrenome { get; private set; }
        public string CaminhoImagem { get; private set; }

        public Usuario()
        {

        }

        public Usuario(string usuarioId, string nome, string sobrenome, string caminhoImagem = "")
        {
            UsuarioId = usuarioId;
            Sobrenome = sobrenome;
            CaminhoImagem = caminhoImagem;
            Nome = nome;
        }

        public override bool EntidadeEhValida()
        {
            ValidarEntidade(this.CampoUsuarioIdEhValido(UsuarioId));
            ValidarEntidade(this.CampoNomeEhValido(Nome));
            return base.EntidadeEhValida();
        }


    }
}