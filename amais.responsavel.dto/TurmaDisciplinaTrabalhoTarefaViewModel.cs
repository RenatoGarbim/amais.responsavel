﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDisciplinaTrabalhoTarefaViewModel
    {
        public Guid TurmaDisciplinaTrabalhoTarefaId { get;  set; }

        public Guid TurmaDisciplinaTrabalhoId { get;  set; }

        public Guid TarefaId { get;  set; }

        [Required(ErrorMessage = "A Tarefa necessita ter uma data de Inicio.")]
        public DateTime? DataInicio { get; set; }

        [Required(ErrorMessage = "A Tarefa necessita ter uma data de Termino.")]
        public DateTime? DataTermino { get; set; }

        [Required(ErrorMessage = "A tarefa necessita ser de algum tipo, por favor, selecione um.")]
        public Guid TipoTarefaId { get; set; }
        [Required(ErrorMessage = "Por favor, informe a hora de inicio da Disciplina.")]
        public string HoraInicio { get; set; }

        [Required(ErrorMessage = "Por favor, informe a hora de termino da Disciplina.")]
        public string HoraTermino { get; set; }
        public int PercentualCompleto { get; set; }
        public bool Status { get; set; }

        public string DetalhamentoTarefa { get; set; }

        public string DescricaoTipoTarefa { get; set; }

        public string DescricaoTarefa { get; set; }
               
        public bool DiaTodo { get; set; }

    }
}