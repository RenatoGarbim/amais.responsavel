﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TarefaDTO
    {
        public Guid TarefaId { get;  set; }

        
        public string UsuarioId { get;  set; }
        public string NomeUsuario { get; set; }

        [Required(ErrorMessage = "A tarefa necessita ser de algum tipo, por favor, selecione um.")]
        public Guid TipoTarefaId { get;  set; }

        public string DescricaoTipoTarefa { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o campo de descrição da Tarefa.")]
        [MaxLength(100, ErrorMessage = "A Descrição da Tarefa deve conter no máximo 100 caracteres.")]
        public string Descricao { get;  set; }

        [MaxLength(300, ErrorMessage = "A Nota da Tarefa deve conter no máximo 300 caracteres.")]
        public string NotaTarefa { get; set; }

        [Required(ErrorMessage = "A Tarefa necessita ter uma data de Inicio.")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? DataInicio { get;  set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? DataTermino { get;  set; }

        [DisplayName("Hora inicio")]
        public string HoraInicio { get; set; }

        [DisplayName("Hora termino")]
        public string HoraTermino { get; set; }

        public int PercentualCompleto { get;  set; }
        public bool Status { get;  set; }

        public int SubTarefa { get; set; }
        public string OrigemTarefa { get; set; }

        public bool DiaInteiro { get; set; }

    }
}