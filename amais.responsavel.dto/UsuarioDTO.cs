﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class UsuarioDTO
    {
        public string UsuarioId { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o campo Nome.")]
        [StringLength(100, ErrorMessage = "O Nome deve ter no máximo 100 caracteres.")]
        public string Nome { get; set; }

        [DisplayName("Sobrenome")]
        [Required(ErrorMessage = "Por favor, preencha o campo Sobrenome.")]
        [StringLength(100, ErrorMessage = "O sobrenome deve ter no máximo 100 caracteres.")]
        public string Sobrenome { get; set; }

        public string CaminhoImagem { get; set; }

        public string Email { get; set; }

    }
}