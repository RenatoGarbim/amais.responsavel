﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDisciplinaExameViewModel
    {
        public Guid TurmaDisciplinaExameId { get; set; }

        [Required(ErrorMessage = "Por favor, selecione uma disciplina.")]
        public Guid TurmaDisciplinaId { get; set; }

        public string TurmaDisciplinaDescricao { get; set; }

        [Required(ErrorMessage = "Por favor, indique a data da Prova.")]
        public DateTime Data { get; set; }

        [Required(ErrorMessage = "Por favor, indique o horário de início da Prova.")]
        public string HoraInicio { get; set; }

        [Required(ErrorMessage = "Por favor, indique o horário de término da Prova.")]
        public string HoraTermino { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o campo titulo do exame.")]
        [MaxLength(100, ErrorMessage = "O máximo de caracteres permitido para o Titulo do Exame é de 100.")]
        public string Descricao { get; set; }

        [MaxLength(50, ErrorMessage = "O máximo de caracteres permitido para sala é de 50.")]
        public string Sala { get; set; }

        [MaxLength(2, ErrorMessage = "O máximo de caracteres permitido para a nota é de 2")]        
        public string Nota { get;  set; }

        [MaxLength(300, ErrorMessage = "O máximo de caracteres permitido para a anotação sobre este exame é de 300")]
        public string Anotacao { get;  set; }

    }
}