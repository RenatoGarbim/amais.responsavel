﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDisciplinaDTO
    {
        [Key]
        [DisplayName("ID")]
        public int TurmaDisciplinaId { get; set; }

        [DisplayName("Guid")]
        public Guid TurmaDisciplinaGuid { get; set; }

        [DisplayName("Turma Id")]
        public int TurmaId { get; set; }

        [DisplayName("Disciplina Id")]
        public int DisciplinaId { get; set; }

        [DisplayName("Nome da turma")]
        public string NomeTurma { get; set; }

        [DisplayName("Descrição da disciplina")]
        public string DescricaoDisciplina { get; set; }

        [DisplayName("Professor")]
        public string Professor { get;  set; }

        [DisplayName("Nome da sala")]
        [MaxLength(50, ErrorMessage = "O máximo de caracteres permitido para a descrição da sala é de 50")]
        public string Sala { get; set; }
       
    }
}