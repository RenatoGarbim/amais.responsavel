﻿namespace AMais.Responsavel.DTO.Graficos
{
    public class GraficoTarefaMensalViewModel
    {
        public string Mes { get; set; }
        public int TotalRegistro { get; set; }
    }
}