﻿namespace AMais.Responsavel.DTO.Graficos
{
    public class GraficoTarefaAtividadeViewModel
    {
        public string Atividade { get; set; }
        public int TotalRegistro { get; set; }

    }
}