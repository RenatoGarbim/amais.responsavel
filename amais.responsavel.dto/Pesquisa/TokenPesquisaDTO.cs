﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class TokenPesquisaDTO
    {
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        [DisplayName("Usuário")]
        public string UsuarioId { get; set; }
    }
}
