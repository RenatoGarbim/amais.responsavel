﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class ProfessorPesquisaDTO
    {
        [DisplayName("Turma")]
        public string TurmaId { get; set; }

        [DisplayName("Disciplina")]
        public string DisciplinaId { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        [DisplayName("e-mail")]
        public string Email { get; set; }
    }
}
