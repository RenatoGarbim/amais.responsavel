﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class TurmaUsuarioPesquisaDTO
    {
        [DisplayName("Turma")]
        public string TurmaId { get; set; }

        [DisplayName("Usuario")]
        public string UsuarioId { get; set; }

    }
}
