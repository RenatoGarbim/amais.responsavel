﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class TrabalhoPesquisaDTO
    {
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
    }
}
