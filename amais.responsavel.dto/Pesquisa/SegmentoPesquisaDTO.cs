﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class SegmentoPesquisaDTO
    {
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
    }
}
