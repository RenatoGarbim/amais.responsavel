﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class TarefaSubPesquisaDTO
    {
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

    }
}
