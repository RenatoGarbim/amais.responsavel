﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class UsuarioPesquisaDTO
    {
        [DisplayName("Nome")]
        public string Nome { get; set; }

    }
}
