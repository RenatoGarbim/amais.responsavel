﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class DisciplinaPesquisaDTO
    {
        [DisplayName("Disciplina")]
        public string Descricao { get; set; }

        [DisplayName("Área")]
        public string AreaId { get; set; }
    }
}
