﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class TurmaDisciplinaPesquisaDTO
    {
        [DisplayName("Turma")]
        public string TurmaId { get; set; }

        [DisplayName("Disciplina")]
        public string DisciplinaId { get; set; }

    }
}
