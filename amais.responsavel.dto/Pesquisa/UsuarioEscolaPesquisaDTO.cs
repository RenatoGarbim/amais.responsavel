﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class UsuarioEscolaPesquisaDTO
    {
        [DisplayName("Escola")]
        public string EscolaId { get; set; }

        [DisplayName("Usuario")]
        public string UsuarioId { get; set; }

    }
}
