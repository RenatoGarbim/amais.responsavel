﻿// 12:50 16 03 2018  AvaliacaoDiagnostica.DTO  AvaliacaoDiagnostica.Solution

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class AvaliacaoRelatorioPesquisaDTO
    {
        [DisplayName("Escola")]
        public int[] EscolaId { get; set; }

        [DisplayName("Turma")]
        public string[] TurmaId { get; set; }

        [DisplayName("Estudante")]
        public string[] Estudante { get; set; }

        [DisplayName("Matrícula A+")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Informe um número de matrícula válido")]
        public string Matricula { get; set; }


        [DisplayName("Avaliação")]
        public string[] AvaliacaoId { get; set; }

        [DisplayName("Avaliação aplicada")]
        public string[] AvaliacaoItemId { get; set; }


        [DisplayName("Tipo avaliação")]
        public string[] TipoAvaliacaoId { get; set; }

        [DisplayName("Tipo Prova")]
        public string[] TipoProvaId { get; set; }


        [DisplayName("Segmento")]
        public string[] SegmentoId { get; set; }

        [DisplayName("Ano")]
        public string[] AnoId { get; set; }


        [DisplayName("Área")]
        public string[] AreaId { get; set; }

        [DisplayName("Disciplina")]
        public string[] DisciplinaId { get; set; }

        public Dictionary<string, string> Avaliacao { get; set; }
        public Dictionary<string, string> AvaliacaoItem { get; set; }
        public Dictionary<string, string> TipoAvaliacao { get; set; }
        public Dictionary<string, string> TipoProva { get; set; }
        public Dictionary<string, string> Segmento { get; set; }
        public Dictionary<string, string> Ano { get; set; }
        public Dictionary<string, string> Area { get; set; }
        public Dictionary<string, string> Disciplina { get; set; }
        public Dictionary<string, string> Escola { get; set; }
        public Dictionary<string, string> Turma { get; set; }

    }
}