﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class EscolaPesquisaDTO
    {
        [DisplayName("Escola")]
        public string Escola { get; set; }

    }
}
