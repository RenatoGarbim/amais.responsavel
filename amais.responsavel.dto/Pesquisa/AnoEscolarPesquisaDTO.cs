﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class AnoEscolarPesquisaDTO
    {
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
    }
}
