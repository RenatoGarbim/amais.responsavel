﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class AulaPesquisaDTO
    {
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
    }
}
