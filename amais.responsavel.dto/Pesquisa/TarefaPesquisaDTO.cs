﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class TarefaPesquisaDTO
    {
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

    }
}
