﻿using System.ComponentModel;

namespace AMais.Responsavel.DTO.Pesquisa
{
    public class TurmaPesquisaDTO
    {
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        [DisplayName("Escola")]
        public string EscolaGuid { get; set; }

        [DisplayName("Ano")]
        public string Ano { get; set; }

        public string EscolaId { get; set; }
    }
}
