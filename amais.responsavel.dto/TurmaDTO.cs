﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDTO
    {
        [Key]
        [DisplayName("Turma Id")]
        public int TurmaId { get; set; }

        [Required]
        [DisplayName("Guid")]
        public Guid TurmaGuid { get; set; }

        [Required]
        [DisplayName("Turma")]
        public string Descricao { get; set; }

        [Required]
        [DisplayName("Escola Id")]
        public int EscolaId { get; set; }

        [Required]
        [DisplayName("Ano Escolar Id")]
        public int AnoEscolarId { get; set; }

        [DisplayName("WhatsUp do Grupo")]
        public string WhatsUpGrupo { get; set; }

        [DisplayName("Email do Grupo")]
        [EmailAddress(ErrorMessage = "Por favor, informe um e-mail valido.")]
        public string EmailGrupo { get; set; }

        [DisplayName("Escola")]
        public string DescricaoEscola { get; set; }

        [DisplayName("Ano")]
        public string DescricaoAno { get; set; }

        [DisplayName("Segmento")]
        public string DescricaoSegmento { get; set; }


    }
}