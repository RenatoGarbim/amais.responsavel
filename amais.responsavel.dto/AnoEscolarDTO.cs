﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class AnoEscolarDTO
    {
        [Key]
        [DisplayName("ID")]
        public int AnoEscolarId { get; set; }

        [Required]
        [DisplayName("Guid")]
        public Guid AnoEscolarGuid { get; set; }

        [Required]
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        [Required]
        [DisplayName("Segmento ID")]
        public int SegmentoId { get; set; }

        [DisplayName("Segmento")]
        public string DescricaoSegmento { get; set; }

        [DisplayName("Registro Interno")]
        public bool RegistroInterno { get;  set; }

    }
}