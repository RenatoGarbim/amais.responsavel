﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TipoTarefaDTO
    {
        public Guid TipoTarefaId { get;  set; }

        [Required(ErrorMessage = "Por favor, preencha a descrição do tipo de tarefa.")]
        public string Descricao { get;  set; }
        

    }
}