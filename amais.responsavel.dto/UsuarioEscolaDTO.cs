﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class UsuarioEscolaDTO
    {
        [Key]
        [DisplayName("Id")]
        public int UsuarioEscolaId { get; set; }

        [DisplayName("Guid")]
        public Guid UsuarioEscolaGuid { get; set; }

        [DisplayName("Escola")]
        public int EscolaId { get; set; }

        [DisplayName("Usuário")]
        public int UsuarioId { get; set; }

        [DisplayName("Telefone")]
        public string Telefone { get; set; }

        [DisplayName("Celular")]
        public string Celular { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Nome da Escola")]
        public string NomeEscola { get; set; }

        [DisplayName("Nome do Usuário")]
        public string NomeUsuario { get; set; }

        public bool Ativo { get; set; }

    }
}