﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaUsuarioDTO
    {

        [Key]
        [DisplayName("Id")]
        public int TurmaUsuarioId { get; set; }

        [DisplayName("Guid")]
        public Guid TurmaUsuarioGuid { get; set; }

        [DisplayName("Turma")]
        public int TurmaId { get; set; }

        [DisplayName("Usuário")]
        public int UsuarioId { get; set; }

        [DisplayName("Turma")]
        public string NomeTurma { get; set; }

        [DisplayName("Usuario")]
        public string NomeUsuario { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

    }
}