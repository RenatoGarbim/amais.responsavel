﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TarefaSubDTO
    {
        public Guid TarefaSubId { get;  set; }

        public Guid TarefaId { get;  set; }

        public string DescricaoTarefa { get; set; }

        [Required(ErrorMessage = "Por favor, preencha a descrição Tarefa.")]
        [MaxLength(100, ErrorMessage = "A Descrição da Tarefa deve conter no máximo 100 caracteres.")]
        public string Descricao { get;  set; }

        [Required(ErrorMessage = "Por favor, selecione um Tipo de Tarefa.")]
        public Guid TipoTarefaId { get;  set; }

        [DisplayName("Tipo da Tarefa")]
        public string DescricaoTipoTarefa { get; set; }

        [Required(ErrorMessage = "A tarefa necessita ter uma data de inicio.")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? DataInicio { get;  set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? DataTermino { get;  set; }

        [DisplayName("Hora inicio")]
        public string HoraInicio { get; set; }

        [DisplayName("Hora termino")]
        public string HoraTermino { get; set; }

        [MaxLength(300, ErrorMessage = "O detalhamento da Sub-Tarefa deve conter no máximo 300 caracteres.")]
        public string NotaTarefa { get; set; }

        public bool Status { get;  set; }
        public int PercentualCompleto { get;  set; }

        public bool DiaInteiro { get; set; }

    }
}