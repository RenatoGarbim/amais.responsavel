﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TokenDTOLista
    {
        [Key]
        [DisplayName("Id")]
        public int TokenId { get; set; }

        [DisplayName("Guid")]
        public Guid TokenGuid { get; set; }

        [DisplayName("Token")]
        public string Descricao { get; set; }

        [DisplayName("Usuário")]
        public Guid UsuarioId { get; set; }

        [DisplayName("Estudante")]
        public string NomeEstudante { get; set; }

        [DisplayName("Turma")]
        public string DescricaoTurma { get; set; }

        [DisplayName("Escola")]
        public string NomeEscola { get; set; }

        [DisplayName("Ano de ensino")]
        public string DescricaoAno { get; set; }

        [DisplayName("Matricula")]
        public int Matricula { get; set; }
    }
}
