﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO.ValueObjects
{
    public class EnderecoDTO
    {
        [DisplayName("UF")]
        public string Uf { get; set; }

        [DisplayName("Cidade")]
        public string Cidade { get; set; }

        [Required]
        [DisplayName("Endereço")]
        public string Rua { get; set; }

        [DisplayName("Número")]
        public string Numero { get; set; }

        [DisplayName("Complemento")]
        public string Complemento { get; set; }

        [DisplayName("Bairro")]
        public string Bairro { get; set; }

        [DisplayName("CEP")]
        public string Cep { get; set; }

        [DisplayName("Telefone")]
        [DataType(DataType.PhoneNumber)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:(##) #####-####}")]
        public string Telefone { get; set; }
    }
}
