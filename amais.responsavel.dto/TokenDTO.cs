﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TokenDTO
    {
        [Key]
        [DisplayName("Id")]
        public int TokenId { get; set; }

        [DisplayName("Guid")]
        public Guid TokenGuid { get; set; }

        [DisplayName("Token")]
        public string Descricao { get; set; }

        public Guid UsuarioId { get; set; }

        public int TurmaEstudanteId { get; set; }
        
    }
}
