﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDisciplinaAulaTarefaViewModel
    {
        public Guid TurmaDisciplinaAulaTarefaId { get;  set; }

        public Guid TurmaDisciplinaAulaId { get;  set; }

        public Guid TarefaId { get;  set; }

        [Required(ErrorMessage = "A tarefa necessita ser de algum tipo, por favor, selecione um.")]
        public Guid TipoTarefaId { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o campo de descrição da Tarefa.")]
        public string DescricaoTarefa { get; set; }

        [Required(ErrorMessage = "A Tarefa necessita ter uma data de Inicio.")]
        public DateTime? DataInicio { get; set; }

        [Required(ErrorMessage = "A Tarefa necessita ter uma data de Termino.")]
        public DateTime? DataTermino { get; set; }

        [Required(ErrorMessage = "Por favor, informe a hora de inicio.")]
        public string HoraInicio { get; set; }

        [Required(ErrorMessage = "Por favor, informe a hora de termino.")]
        public string HoraTermino { get; set; }

        public int PercentualCompleto { get; set; }
        public bool Status { get; set; }

        public string DetalhamentoTarefa { get; set; }

        public string DescricaoTipoTarefa { get; set; }
        
        public bool DiaTodo { get; set; }

    }
}