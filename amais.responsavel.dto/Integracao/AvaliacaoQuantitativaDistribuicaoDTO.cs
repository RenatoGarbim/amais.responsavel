﻿// 23:18 25 03 2018  AvaliacaoDiagnostica.DTO  AvaliacaoDiagnostica.Solution

using System;

namespace AMais.Responsavel.DTO.Integracao
{
    public class AvaliacaoQuantitativaDistribuicaoDTO
    {
        public int AvaliacaoRespostaId { get; set; }
        public int AvaliacaoGabaritoId { get; set; }
        public int DescritorId { get; set; }
        public int AvaliacaoItemId { get; set; }
        public Guid AvaliacaoItemGuid { get; set; }
        public string DescricaoAvaliacao { get; set; }
        public string DescricaoArea { get; set; }
        public string DescricaoDisciplina { get; set; }

        public int OrdemAno { get; set; }
        public string DescricaoAno { get; set; }
        public string DescricaoTipoAvaliacao { get; set; }
        public string DescricaoTipoProva { get; set; }
        public int Questao { get;  set; }
        public string Gabarito { get; set; }
        public string Resposta { get;  set; }
        public string DescricaoItem => $"{DescricaoAvaliacao}-{DescricaoAno}-{DescricaoTipoAvaliacao}-{DescricaoTipoProva}";
    }
}