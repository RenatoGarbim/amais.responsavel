﻿// AgendaDoEstudante - AMais.AgendaDoEstudante.DTO - AMaisTurma.cs
// 2018 / 09 / 24 : 18:18
// 2018 / 09 / 24 : 18:18

using System;
using System.ComponentModel;

namespace AMais.Responsavel.DTO.Integracao
{
    public class AMaisTurmaEstudanteDTO
    {

        public int TurmaEstudanteId { get; set; }
        public Guid TurmaEstudanteGuid { get; set; }
        public int TurmaId { get; set; }

        [DisplayName("Escola")]
        public string NomeEscola { get; set; }

        [DisplayName("Turma")]
        public string DescricaoTurma { get; set; }

        [DisplayName("Ano Letivo")]
        public int AnoLetivo { get; set; }

        [DisplayName("Ano de ensino")]
        public string DescricaoAno { get; set; }

        [DisplayName("Estudante")]
        public string NomeEstudante { get; set; }

        [DisplayName("Matrícula A+")]
        public int Matricula { get; set; }

        [DisplayName("Ativo")]
        public bool Ativo { get; set; }

        public string TokenAcesso { get; set; }

    }
}