﻿// 20:32 25 03 2018  AvaliacaoDiagnostica.DTO  AvaliacaoDiagnostica.Solution

using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO.Integracao
{
    public class AvaliacaoQuantitativaDTO
    {
        public int AvaliacaoId { get; set; }
        public string DescricaoAvaliacao { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public int TotalAvaliacao { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public int TotalProva { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public int TotalQuestao { get; set; }


        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public int TotalArea { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public int TotalDisciplina { get; set; }

        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public int TotalTurma{ get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public int TotalEstudante { get; set; }

        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public decimal TotalAcerto { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public decimal TotalErro { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public decimal PercentualAcerto => (TotalAcerto + TotalErro) > 0 ? Math.Round((TotalAcerto / (TotalAcerto + TotalErro)) * 100, 2) : 0;

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public decimal PercentualErro => (TotalAcerto + TotalErro) > 0 ? Math.Round(TotalErro / (TotalAcerto + TotalErro) * 100, 2) : 0;
    }
}