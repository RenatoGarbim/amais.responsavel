﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMais.Responsavel.DTO.Integracao.AgendaDoEstudante
{
    public class EstudanteDadosDTO
    {
        public int TurmaEstudanteId { get; set; }

        public string Token { get; set; }

        public string Nome { get; set; }

        public string Sobrenome { get; set; }

        public string CaminhoImagem { get; set; }

        public string DescricaoTurma { get; set; }
        public string DescricaoAno { get; set; }

        public int AnoLetivo { get; set; }
    }
}
