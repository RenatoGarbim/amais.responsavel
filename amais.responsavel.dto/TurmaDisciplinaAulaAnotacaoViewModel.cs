﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDisciplinaAulaAnotacaoViewModel
    {

        public Guid TurmaDisciplinaAulaAnotacaoId { get;  set; }

        public Guid TurmaDisciplinaAulaId { get;  set; }

        [MaxLength(300, ErrorMessage = "O máximo de caracteres permitido para uma Anotação na Aula é de 300.")]
        public string Anotacao { get; set; }

    }
}