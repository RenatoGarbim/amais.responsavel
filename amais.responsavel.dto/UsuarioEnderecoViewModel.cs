﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class UsuarioEnderecoViewModel
    {
        public Guid UsuarioEnderecoId { get; set; }
        public string UsuarioId { get; set; }

        [Required(ErrorMessage = "Por favor, selecione o tipo de endereço")]
        public Guid TipoEnderecoId { get; set; }

        public string TipoEnderecoDescricao { get; set; }

        public Guid EnderecoId { get; set; }

        [MaxLength(200, ErrorMessage = "O nome da rua não deve conter mais do que 200 caracteres.")]
        public string Descricao { get; set; }

        public string Numero { get; set; }

        [MaxLength(30, ErrorMessage = "O Máximo de caracteres permitidos para o campo Complemento é de 30.")]
        public string Complemento { get; set; }

        [StringLength(100, ErrorMessage = "O máximo de caracteres aceito em bairro é de 100, por favor, reveja o nome do bairro.")]
        public string Bairro { get; set; }

        [StringLength(9, ErrorMessage = "O campo CEP deve conter apenas números e deve ter no máximo 9 caracteres")]
        public string CEP { get; set; }

        [MaxLength(150, ErrorMessage = "O Máximo de caracteres permitidos para o campo Cidade é de 150")]
        public string Cidade { get; set; }

        [MaxLength(2, ErrorMessage = "O Máximo de caracteres permitidos para o campo Estado é de 2")]
        public string Estado { get; set; }

        public string NomeUsuario { get; set; }
    }
}
