﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMais.Responsavel.DTO
{
    public class PerfilResponsavelDTO
    {
        public Guid PerfilResponsavelGuid { get;  set; }
        public int PerfilResponsavelId { get;  set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        [DisplayName("Sobrenome")]
        public string Sobrenome { get; set; }

        [DisplayName("Imagem do Perfil")]
        public string CaminhoImagem { get; set; }

        public string CPF { get;  set; }
        public string Telefone { get;  set; }
        public string Celular { get;  set; }

        [DisplayName("Usuário")]
        public Guid UsuarioId { get;  set; }
    }
}
