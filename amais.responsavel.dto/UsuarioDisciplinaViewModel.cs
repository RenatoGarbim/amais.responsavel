﻿using System;

namespace AMais.Responsavel.DTO
{
    public class UsuarioDisciplinaViewModel
    {
        public Guid UsuarioDisciplinaId { get;  set; }

        public Guid DisciplinaId { get;  set; }
        public string UsuarioId { get;  set; }

        public string DisciplinaNome { get;  set; }
        public string DiscplinaArea { get;  set; }

    }
}
