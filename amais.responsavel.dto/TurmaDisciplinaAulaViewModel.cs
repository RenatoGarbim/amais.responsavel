﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDisciplinaAulaViewModel
    {
        [Key]
        public Guid TurmaDisciplinaAulaId { get; set; }

        public Guid TurmaDisciplinaId { get;  set; }
         
        public string DescricaoDisciplina { get; set; }

        public string Sala { get;  set; }

        [MaxLength(50, ErrorMessage = "O máximo de caracteres permitido para o nome de professor é 50")]
        public string ProfessorNome { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        [Required(ErrorMessage = "Por favor, indique uma Data para esta aula.")]
        public DateTime? Data { get;  set; }

        [Required(ErrorMessage = "Por favor, informe a hora de inicio da Disciplina.")]
        public string HoraInicio { get;  set; }

        [Required(ErrorMessage = "Por favor, informe a hora de termino da Disciplina.")]
        public string HoraTermino { get;  set; }

        public string DescricaoTurma { get; set; }

        public int TurmaId { get; set; }

    }
}