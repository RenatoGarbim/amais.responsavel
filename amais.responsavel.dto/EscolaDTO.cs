﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using AMais.Responsavel.DTO.ValueObjects;

namespace AMais.Responsavel.DTO
{
    public class EscolaDTO
    {
        [Key]
        [DisplayName("Id")]
        public int EscolaId { get; set; }

        [DisplayName("Guid")]
        public Guid EscolaGuid { get; set; }

        [DisplayName("Código MEC")]
        public string CodigoMec { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        public EnderecoDTO Endereco { get; set; }

        [DisplayName("Integração A+")]
        public bool IntegracaoAmais { get; set; }

    }
}