﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDisciplinaTrabalhoViewModel
    {
        public Guid TurmaDisciplinaTrabalhoId { get;  set; }

        public Guid TurmaDisciplinaId { get;  set; }
        public string NomeDisciplina { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o campo Trabalho.")]
        [MaxLength(100, ErrorMessage = "O máximo de caracteres permitido para o titulo do trabalho é de 100.")]
        public string Descricao { get;  set; }

        [Required(ErrorMessage = "Por favor, indique uma data de entrega.")]
        public DateTime DataEntrega { get;  set; }

        public string BriefTrabalho { get; set; }

        public string Nota { get;  set; }
        

    }
}