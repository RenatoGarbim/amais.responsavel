﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AMais.Responsavel.DTO
{
    public class TurmaDisciplinaExameConteudoViewModel
    {
        public Guid TurmaDisciplinaExameConteudoId { get;  set; }
        public Guid TurmaDisciplinaExameId { get;  set; }

        [Required(ErrorMessage = "Por favor preencha a descrição ")]
        [MaxLength(100, ErrorMessage = "O máximo de caracteres permitido para a descrição do conteúdo do exame é de 100")]
        public string Descricao { get;  set; }
        

    }
}