﻿using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using AMais.Responsavel.Domain.Specifications;
using DomainValidation.Validation;

namespace AMais.Responsavel.Domain.Validation
{
    public sealed class UsuarioAptoParaCadastro : Validator<Usuario>
    {
        public UsuarioAptoParaCadastro(IUsuarioRepository repository)
        {
            Add("RegistroDuplicado", new Rule<Usuario>(new UsuarioNaoPodeTerDuplicado(repository), "Já existe um registro para esse ano cadastrado!"));
        }
    }
}