﻿using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using AMais.Responsavel.Domain.Specifications;
using DomainValidation.Validation;

namespace AMais.Responsavel.Domain.Validation
{
    public sealed class TokenAptoParaCadastro : Validator<Token>
    {
        public TokenAptoParaCadastro(ITokenRepository repository)
        {
            Add("RegistroDuplicado", new Rule<Token>(new TokenNaoPodeTerDuplicado(repository), "O código do token informado já foi validado e utilizado, operação cancelada!"));
            Add("RegistroAlterado", new Rule<Token>(new TokenNaoPodeSerAlterado(), "Não é permitido alterar tokens validados!"));
        }
    }
}