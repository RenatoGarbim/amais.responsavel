﻿using AMais.Responsavel.Domain.Entities;
using Linte.Core.CrossCutting.Interfaces;

namespace AMais.Responsavel.Domain.Interfaces.Repositories
{
    public interface ITokenRepository : IRepositoryBase<Token>
    {
    }
}
