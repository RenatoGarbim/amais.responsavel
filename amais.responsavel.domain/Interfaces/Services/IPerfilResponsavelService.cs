﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMais.Responsavel.Domain.Entities;
using Linte.Core.CrossCutting.Interfaces;

namespace AMais.Responsavel.Domain.Interfaces.Services
{
    public interface IPerfilResponsavelService : IServiceBase<PerfilResponsavel>
    {
        Entities.PerfilResponsavel ObterPor(string usuarioId);
    }
}
