﻿using AMais.Responsavel.Domain.Entities;
using Linte.Core.CrossCutting.Interfaces;

namespace AMais.Responsavel.Domain.Interfaces.Services
{
    public interface ITokenService: IServiceBase<Token>
    {
    }
}
