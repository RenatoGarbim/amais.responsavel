﻿using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.Domain.Validation;
using DomainValidation.Validation;
using Linte.Core.Domain.Services;

namespace AMais.Responsavel.Domain.Services
{
    public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
    {
        private readonly IUsuarioRepository _repository;

        public UsuarioService(IUsuarioRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public override ValidationResult GetObjectToValidationSaveResult(Usuario obj)
        {
            return new UsuarioAptoParaCadastro(_repository).Validate(obj);
        }

    }
}