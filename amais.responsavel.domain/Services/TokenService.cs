﻿using System;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using AMais.Responsavel.Domain.Interfaces.Services;
using AMais.Responsavel.Domain.Validation;
using DomainValidation.Validation;
using Linte.Core.CrossCutting.Class;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.Domain.Services;
using Linte.Core.DomainEvent.Events;

namespace AMais.Responsavel.Domain.Services
{
    public class TokenService : ServiceBase<Token>, ITokenService
    {
        private readonly ITokenRepository _repository;
        private readonly IUserLogged _userLogged;

        public TokenService(ITokenRepository repository, IUserLogged userLogged) : base(repository)
        {
            _repository = repository;
            _userLogged = userLogged;
        }

        public override ValidationResult GetObjectToValidationSaveResult(Token obj)
        {
            return new TokenAptoParaCadastro(_repository).Validate(obj);
        }

        public override DataResult InsertRecord(Token obj)
        {
            if (!Guid.TryParse(_userLogged.GetUserId(), out var userId))
            {
                Notifications.Handle(new DomainNotification("Erro", "Erro ao definir o código do usuário, operação cancelada!"));
                return new DataResult() { Result = false, MessageException = "Erro ao definir código do usuário." };
            }

            obj.DefinirCampoUsuario(userId);
            return base.InsertRecord(obj);
        }
    }
}