﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using AMais.Responsavel.Domain.Interfaces.Services;
using Linte.Core.Domain.Services;

namespace AMais.Responsavel.Domain.Services
{
    public class PerfilResponsavelService : ServiceBase<Entities.PerfilResponsavel>, IPerfilResponsavelService
    {
        private readonly IPerfilResponsavelRepository _repository;

        public PerfilResponsavelService(IPerfilResponsavelRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public Entities.PerfilResponsavel ObterPor(string usuarioId)
        {
            return _repository.ObterPor(usuarioId);
        }

    }
}
