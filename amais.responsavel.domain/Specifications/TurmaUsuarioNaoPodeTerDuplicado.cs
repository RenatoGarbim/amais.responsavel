﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaUsuarioNaoPodeTerDuplicado : ISpecification<TurmaUsuario>
    {
        private readonly ITurmaUsuarioRepository _repository;

        public TurmaUsuarioNaoPodeTerDuplicado(ITurmaUsuarioRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaUsuario entity)
        {
            return !_repository.GetAllRecords().Any(x => x.TurmaId.Equals(entity.TurmaId) && x.UsuarioId.Equals(entity.UsuarioId));
        }
    }
}