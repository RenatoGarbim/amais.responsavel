﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisciplinaExameConteudoTarefaNaoPodeTerTarefaDuplicadaPorConteudo : ISpecification<TurmaDisciplinaExameConteudoTarefa>
    {
        private readonly ITurmaDisciplinaExameConteudoTarefaRepository _repository;

        public TurmaDisciplinaExameConteudoTarefaNaoPodeTerTarefaDuplicadaPorConteudo(ITurmaDisciplinaExameConteudoTarefaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplinaExameConteudoTarefa entity)
        {
            return !_repository.ObterPorTurmaDisciplinaExameConteudo(entity.TurmaDisciplinaExameConteudoId).Any(x => x.TarefaId.Equals(entity.TarefaId));
        }
    }
}