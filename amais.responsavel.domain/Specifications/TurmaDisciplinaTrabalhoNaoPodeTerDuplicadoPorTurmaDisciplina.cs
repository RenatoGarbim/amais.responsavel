﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisciplinaTrabalhoNaoPodeTerDuplicadoPorTurmaDisciplina : ISpecification<TurmaDisciplinaTrabalho>
    {
        private readonly ITurmaDisciplinaTrabalhoRepository _repository;

        public TurmaDisciplinaTrabalhoNaoPodeTerDuplicadoPorTurmaDisciplina(ITurmaDisciplinaTrabalhoRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplinaTrabalho entity)
        {
            return !_repository.ObterPorTurmaDisciplina(entity.TurmaDisciplinaId).Any(x => x.Descricao.Equals(entity.Descricao) && x.TurmaDisciplinaTrabalhoId != entity.TurmaDisciplinaTrabalhoId);
        }
    }
}