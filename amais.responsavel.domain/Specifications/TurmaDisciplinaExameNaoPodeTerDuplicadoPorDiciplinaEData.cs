﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisciplinaExameNaoPodeTerDuplicadoPorDiciplinaEData : ISpecification<TurmaDisciplinaExame>
    {
        private readonly ITurmaDisciplinaExameRepository _repository;

        public TurmaDisciplinaExameNaoPodeTerDuplicadoPorDiciplinaEData(ITurmaDisciplinaExameRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplinaExame entity)
        {
            return !_repository.ObterPorTurmaDisciplina(entity.TurmaDisciplinaId).Any(x => x.Data == entity.Data && x.TurmaDisciplinaExameId != entity.TurmaDisciplinaExameId);
        }
    }
}