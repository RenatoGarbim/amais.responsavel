﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisiciplinaAulaAnotacaoNaoPodeTerDuplicadoPorAula : ISpecification<TurmaDisciplinaAulaAnotacao>
    {

        private readonly ITurmaDisciplinaAulaAnotacaoRepository _repository;

        public TurmaDisiciplinaAulaAnotacaoNaoPodeTerDuplicadoPorAula(ITurmaDisciplinaAulaAnotacaoRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplinaAulaAnotacao entity)
        {
            return !_repository.ObterPorTurmaDisciplinaAula(entity.TurmaDisciplinaAulaId).Any(x => x.Anotacao.Equals(entity.Anotacao));
        }
    }
}