﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisciplinaAulaNaoPodeTerDuplicadoPorDataEHora : ISpecification<TurmaDisciplinaAula>
    {

        private readonly ITurmaDisciplinaAulaRepository _repository;

        public TurmaDisciplinaAulaNaoPodeTerDuplicadoPorDataEHora(ITurmaDisciplinaAulaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplinaAula entity)
        {
            return !_repository.ObterPorTurmaDisciplina(entity.TurmaDisciplinaId).Any(x => x.Data == entity.Data && x.HoraInicio.Equals(entity.HoraInicio) && x.TurmaDisciplinaAulaId != entity.TurmaDisciplinaAulaId);
        }
    }
}