﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisciplinaNaoPodeTerDuplicadoPorTurma : ISpecification<TurmaDisciplina>
    {

        private readonly ITurmaDisciplinaRepository _repository;

        public TurmaDisciplinaNaoPodeTerDuplicadoPorTurma(ITurmaDisciplinaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplina entity)
        {
            return !_repository.ObterPorTurma(entity.TurmaId).Any(x => x.DisciplinaId == entity.DisciplinaId);
        }
    }
}