﻿using AMais.Responsavel.Domain.Entities;
using DomainValidation.Interfaces.Specification;

namespace AMais.Responsavel.Domain.Specifications
{
    public class TokenNaoPodeSerAlterado : ISpecification<Token>
    {

        public bool IsSatisfiedBy(Token entity)
        {
            return entity.TokenId == 0;
        }
    }
}