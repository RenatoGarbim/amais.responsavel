﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisciplinaTrabalhoTarefaNaoPodeTerDuplicadoPorTrabalho : ISpecification<TurmaDisciplinaTrabalhoTarefa>
    {
        private readonly ITurmaDisciplinaTrabalhoTarefaRepository _repository;

        public TurmaDisciplinaTrabalhoTarefaNaoPodeTerDuplicadoPorTrabalho(ITurmaDisciplinaTrabalhoTarefaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplinaTrabalhoTarefa entity)
        {
            return !_repository.ObterPorTurmaDisciplinaTrabalho(entity.TurmaDisciplinaTrabalhoId).Any(x => x.TarefaId.Equals(entity.TarefaId));
        }
    }
}