﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisciplinaAulaTarefaNaoPodeTerDuplicadoPorAula : ISpecification<TurmaDisciplinaAulaTarefa>
    {

        private readonly ITurmaDisciplinaAulaTarefaRepository _repository;

        public TurmaDisciplinaAulaTarefaNaoPodeTerDuplicadoPorAula(ITurmaDisciplinaAulaTarefaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplinaAulaTarefa entity)
        {
            return !_repository.ObterPorTurmaDisciplinaAula(entity.TurmaDisciplinaAulaId).Any(x => x.TarefaId.Equals(entity.TarefaId));
        }
    }
}