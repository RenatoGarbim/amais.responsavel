﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaDisciplinaExameConteudoNaoPodeTerDuplicadoPorExame : ISpecification<TurmaDisciplinaExameConteudo>
    {
        private readonly ITurmaDisciplinaExameConteudoRepository _repository;

        public TurmaDisciplinaExameConteudoNaoPodeTerDuplicadoPorExame(ITurmaDisciplinaExameConteudoRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(TurmaDisciplinaExameConteudo entity)
        {
            return !_repository.ObterPorTurmaDisciplinaExame(entity.TurmaDisciplinaExameId).Any(x => x.Descricao.Equals(entity.Descricao) && x.TurmaDisciplinaExameConteudoId != entity.TurmaDisciplinaExameConteudoId);
        }
    }
}