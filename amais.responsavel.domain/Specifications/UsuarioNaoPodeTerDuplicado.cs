﻿using System.Linq;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.Responsavel.Domain.Specifications
{
    public class UsuarioNaoPodeTerDuplicado : ISpecification<Usuario>
    {
        private readonly IUsuarioRepository _repository;

        public UsuarioNaoPodeTerDuplicado(IUsuarioRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(Usuario entity)
        {
            return !_repository.GetAllRecords().Any(x => x.UsuarioId != entity.UsuarioId && x.Nome.Equals(entity.Nome));
        }
    }
}