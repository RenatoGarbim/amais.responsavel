﻿using System.Linq;
using AMais.AgendaDoEstudante.Domain.Entities;
using AMais.AgendaDoEstudante.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.AgendaDoEstudante.Domain.Specifications
{
    public class TurmaNaoPodeTerDuplicadoPorUsuario : ISpecification<Turma>
    {

        private readonly ITurmaRepository _repository;

        public TurmaNaoPodeTerDuplicadoPorUsuario(ITurmaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(Turma entity)
        {
            return !_repository.ObterPorUsuario(entity.UsuarioId).Any(x => x.NomeTurma.Equals(entity.NomeTurma) && x.TurmaId != entity.TurmaId);
        }
    }
}