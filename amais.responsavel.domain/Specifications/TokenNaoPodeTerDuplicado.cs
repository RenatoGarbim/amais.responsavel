﻿using System.Linq;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using DomainValidation.Interfaces.Specification;

namespace AMais.Responsavel.Domain.Specifications
{
    public class TokenNaoPodeTerDuplicado : ISpecification<Token>
    {
        private readonly ITokenRepository _repository;

        public TokenNaoPodeTerDuplicado(ITokenRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(Token entity)
        {
            return !_repository.GetAllRecords().Any(x => x.Descricao.Equals(entity.Descricao));
        }
    }
}