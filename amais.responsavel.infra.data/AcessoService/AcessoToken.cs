﻿// 16:24 23 03 2018  AvaliacaoDiagnostica.Domain  AvaliacaoDiagnostica.Solution

using System;
using System.Collections.Generic;
using System.Linq;
using AMais.Responsavel.Domain.Entities;
using Linte.Core.CrossCutting.Interfaces;

namespace AMais.Responsavel.Infra.Data.AcessoService
{
    internal static class AcessoToken
    {
        internal static IQueryable<Token> ObterLista(IUserLogged userLogged, IQueryable<Token> registros)
        {
            var usuario = userLogged.GetUsuario();
            if (usuario.LinteAdministrador || usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("02-MAESTRO-ADMINISTRACAO")))
                return registros;

            return Guid.TryParse(userLogged.GetUserId(), out var usuarioId) ? registros.Where(x => x.UsuarioId == usuarioId) : new List<Token>().AsQueryable();
        }

        internal static Token ObterRegistro(IUserLogged userLogged, Token registro)
        {
            var usuario = userLogged.GetUsuario();
            if (usuario.LinteAdministrador || usuario.UsuarioGrupo.Any(x => x.RoleName.Equals("02-MAESTRO-ADMINISTRACAO")))
                return registro;

            if (!Guid.TryParse(userLogged.GetUserId(), out var usuarioId))
                return new Token();

            return registro.UsuarioId == usuarioId ? registro : new Token();
        }
    }
}