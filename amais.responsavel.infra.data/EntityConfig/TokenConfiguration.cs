﻿using System.Data.Entity.ModelConfiguration;
using AMais.Responsavel.Domain.Entities;

namespace AMais.Responsavel.Infra.Data.EntityConfig
{
    public class TokenConfiguration : EntityTypeConfiguration<Token>
    {
        public TokenConfiguration()
        {
            Property(x => x.Descricao)
                .HasMaxLength(50);

        }
    }
}