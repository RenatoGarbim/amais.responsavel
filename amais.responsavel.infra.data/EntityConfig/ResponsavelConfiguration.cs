﻿using System.Data.Entity.ModelConfiguration;
using AMais.Responsavel.Domain.Entities;

namespace AMais.Responsavel.Infra.Data.EntityConfig
{
    public class PerfilResponsavelConfiguration : EntityTypeConfiguration<PerfilResponsavel>
    {
        public PerfilResponsavelConfiguration()
        {
            Property(x => x.Nome)
                .HasMaxLength(200);

            Property(x => x.Sobrenome)
                .HasMaxLength(200);

            Property(x => x.CPF)
                .HasMaxLength(20);
        }
    }
}