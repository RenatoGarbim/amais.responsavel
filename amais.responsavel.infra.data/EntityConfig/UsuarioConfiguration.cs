﻿using System.Data.Entity.ModelConfiguration;
using AMais.Responsavel.Domain.Entities;

namespace AMais.Responsavel.Infra.Data.EntityConfig
{
    public class UsuarioConfiguration : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfiguration()
        {
            Property(x => x.UsuarioId)
                .HasColumnName("Id");

            Property(x => x.Nome)
                .HasMaxLength(200);

            Property(x => x.Sobrenome)
                .HasMaxLength(200);

            ToTable("aspnetusers");
        }
    }
}