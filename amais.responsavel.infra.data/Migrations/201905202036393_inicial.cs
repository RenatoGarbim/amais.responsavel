using System.Data.Entity.Migrations;

namespace AMais.Responsavel.Infra.Data.Migrations
{
    public partial class inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Token",
                c => new
                    {
                        TokenId = c.Int(nullable: false, identity: true),
                        TokenGuid = c.Guid(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50, unicode: false),
                        UsuarioId = c.Guid(nullable: false),
                        TurmaEstudanteId = c.Int(nullable: false),
                        RegistroDataCadastro = c.DateTime(precision: 0),
                        RegistroDataAlteracao = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.TokenId);
            
            CreateTable(
                "dbo.aspnetusers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, unicode: false),
                        Nome = c.String(maxLength: 200, unicode: false),
                        Sobrenome = c.String(maxLength: 200, unicode: false),
                        CaminhoImagem = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.aspnetusers");
            DropTable("dbo.Token");
        }
    }
}
