using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Sql;
using System.IO;
using System.Linq;
using System.Text;
using AMais.Responsavel.Infra.Data.Contexto;

namespace AMais.Responsavel.Infra.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ResponsavelContext>
    {
        public Configuration()
        {

#if DEBUG
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;

#else
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;

#endif
            SetSqlGenerator("MySql.Data.MySqlClient", new SqlGenerator());
        }

        protected override void Seed(ResponsavelContext context)
        {
            //context.TipoEndereco.AddOrUpdate(new TipoEndereco(Guid.NewGuid(), "Residencial"));
            //context.TipoEndereco.AddOrUpdate(new TipoEndereco(Guid.NewGuid(), "Comercial"));
            context.SaveChanges();
        }

    }

    class SqlGenerator : MySql.Data.Entity.MySqlMigrationSqlGenerator
    {
        public override IEnumerable<MigrationStatement> Generate(IEnumerable<MigrationOperation> migrationOperations, string providerManifestToken)
        {
            var stringBuilder = new StringBuilder();
            IEnumerable<MigrationStatement> res = base.Generate(migrationOperations, providerManifestToken).ToList();
            foreach (var ms in res)
            {
                ms.Sql = ms.Sql.Replace("dbo.", "");
                ms.Sql = ms.Sql.Replace("create table", "create table if not exists");
                ms.Sql = ms.Sql.Replace("nvarchar(", "varchar(");
                ms.Sql += ";";
                stringBuilder.Append(ms.Sql);
            }
            var fileName = $"c:\\temp\\SaveDB.Responsavel.{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Hour}-{DateTime.Now.Minute}-{DateTime.Now.Second}.sql";
            File.WriteAllText(fileName, stringBuilder.ToString());
            return res;
        }
    }
}