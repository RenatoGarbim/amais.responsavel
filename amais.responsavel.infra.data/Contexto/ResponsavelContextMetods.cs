﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using AMais.Responsavel.Infra.Data.EntityConfig;

namespace AMais.Responsavel.Infra.Data.Contexto
{
    public partial class ResponsavelContext
    {
        public ResponsavelContext() : base("Responsavel")
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
        }

        private static void ConfigurationCommonTables(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add(new UsuarioConfiguration());
            modelBuilder.Configurations.Add(new TokenConfiguration());
            modelBuilder.Configurations.Add(new PerfilResponsavelConfiguration());
        }

        private static DbModelBuilder IgnoreEntitys(DbModelBuilder modelBuilder)
        {
            var modelToIgnore = modelBuilder;
            //modelToIgnore.Ignore<Area>();
            return modelToIgnore;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove();
            ConfigurationCommonTables(modelBuilder);
            modelBuilder = IgnoreEntitys(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties().Where(p => p.Name == p.ReflectedType?.Name + "Id").Configure(p => p.IsKey());
            modelBuilder.Properties().Where(p => p.Name == "Descricao").Configure(p => p.IsRequired());

            modelBuilder.Properties().Where(p => p.PropertyType == typeof(string)).Configure(p => p.IsUnicode(false));

            modelBuilder.Properties().Where(p => p.Name.Contains("Telefone")).Configure(p => p.HasColumnType("varchar").HasMaxLength(20));
            modelBuilder.Properties().Where(p => p.Name.Contains("Celular")).Configure(p => p.HasColumnType("varchar").HasMaxLength(20));

            modelBuilder.Properties().Where(p => p.Name.Contains("EMail")).Configure(p => p.HasColumnType("varchar").HasMaxLength(50));

            modelBuilder.Properties().Where(p => p.Name.Contains("NomeUsuario")).Configure(p => p.HasColumnType("varchar").HasMaxLength(30));
            modelBuilder.Properties().Where(p => p.Name.Contains("Senha")).Configure(p => p.HasColumnType("varchar").HasMaxLength(10));

            base.OnModelCreating(modelBuilder);
        }
        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("RegistroDataCadastro") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("RegistroDataCadastro").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("RegistroDataCadastro").IsModified = false;
                }
            }

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("RegistroDataAlteracao") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("RegistroDataAlteracao").IsModified = false;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("RegistroDataAlteracao").CurrentValue = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }
    }
}