﻿using System.Data.Entity;
using AMais.Responsavel.Domain.Entities;
using MySql.Data.Entity;

namespace AMais.Responsavel.Infra.Data.Contexto
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public partial class ResponsavelContext : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }

        public DbSet<Token> Token { get; set; }
        public DbSet<Domain.Entities.PerfilResponsavel>Responsavel { get; set; }

    }
}