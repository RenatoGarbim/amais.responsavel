﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMais.Responsavel.Domain.Interfaces.Repositories;

namespace AMais.Responsavel.Infra.Data.Repositories
{
    public class ResponsavelRepository : RepositoryBase<Domain.Entities.PerfilResponsavel>, IPerfilResponsavelRepository
    {

        public Domain.Entities.PerfilResponsavel ObterPor(string usuarioId)
        {
            var guid = Guid.Parse(usuarioId);

            return GetAllRecords().FirstOrDefault(x => x.UsuarioId == guid);
        }
    }
}
