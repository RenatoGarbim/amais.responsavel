﻿using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using Linte.Core.CrossCutting.Class;
using Linte.Core.DomainEvent.Events;

namespace AMais.Responsavel.Infra.Data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        public override DataResult InsertRecord(Usuario obj)
        {
            Notifications.Handle(new DomainNotification("OperacaoBloqueada", "Somente é permitido a criação de usuários através do formulário de registro!"));
            return new DataResult() { Result = false };
        }
    }
}