﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AMais.Responsavel.Infra.Data.Contexto;
using Linte.Core.CrossCutting.Class;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.DomainEvent.Events;
using Linte.Core.DomainEvent.Interfaces;
using Linte.Core.Infra.Data.Services;

namespace AMais.Responsavel.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly ContextDataAccess<TEntity> ContextDataAccess;

        protected readonly IHandler<DomainNotification> Notifications;
        protected internal readonly ResponsavelContext Db;

        public event FiltrarRegistrosPorNivelAcesso<TEntity> FiltrarRegistrosPorNivelAcesso;
        public event FiltrarRegistroPorNivelAcesso<TEntity> FiltrarRegistroPorNivelAcesso;


        public RepositoryBase()
        {
            if (DomainEvent.Container != null)
                Notifications = DomainEvent.Container.GetInstance<IHandler<DomainNotification>>();

            Db = new ResponsavelContext();
            ContextDataAccess = new ContextDataAccess<TEntity>(Db);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public virtual void ClearDataCache()
        {

        }

        public virtual DataResult InsertRecord(TEntity obj)
        {
            var dataResult = ContextDataAccess.DbAddNewRecord(obj);
            if (dataResult.Result)
                ClearDataCache();
            return dataResult;
        }

        public virtual DataResult RemoveRecord(TEntity obj)
        {
            var dataResult = ContextDataAccess.DbRemoveRecord(obj);
            if (dataResult.Result)
                ClearDataCache();
            return dataResult;
        }

        public virtual DataResult UpdateRecord(TEntity obj)
        {
            var dataResult = ContextDataAccess.DbUpdateRecord(obj);
            if (dataResult.Result)
                ClearDataCache();
            return dataResult;
        }

        public DataResult UpdateRecord(TEntity obj, params Expression<Func<TEntity, object>>[] updatedProperties)
        {
            var dataResult = ContextDataAccess.DbUpdateRecord(obj, updatedProperties);
            if (dataResult.Result)
                ClearDataCache();
            return dataResult;
        }

        public virtual IQueryable<TEntity> GetAllRecords()
        {
            var registros = Db.Set<TEntity>().AsQueryable();
            return FiltrarRegistrosPorNivelAcesso == null ? registros : FiltrarRegistrosPorNivelAcesso(registros);
        }

        public virtual TEntity GetRecordById(int id)
        {
            var registro = Db.Set<TEntity>().Find(id);
            return FiltrarRegistroPorNivelAcesso == null ? registro : FiltrarRegistroPorNivelAcesso(registro);
        }

        public virtual TEntity GetRecordById(Guid id)
        {
            var registro = Db.Set<TEntity>().Find(id);
            return FiltrarRegistroPorNivelAcesso == null ? registro : FiltrarRegistroPorNivelAcesso(registro);
        }

        public virtual TEntity GetRecordById(string id)
        {
            var registro = Db.Set<TEntity>().Find(id);
            return FiltrarRegistroPorNivelAcesso == null ? registro : FiltrarRegistroPorNivelAcesso(registro);
        }


    }
}
