﻿using System;
using System.Linq;
using AMais.Responsavel.Domain.Entities;
using AMais.Responsavel.Domain.Interfaces.Repositories;
using Linte.Core.CrossCutting.Interfaces;

namespace AMais.Responsavel.Infra.Data.Repositories
{
    public class TokenRepository : RepositoryBase<Token>, ITokenRepository
    {
        private readonly IUserLogged _userLogged;

        public TokenRepository(IUserLogged userLogged)
        {
            _userLogged = userLogged;
        }

        public override Token GetRecordById(Guid id)
        {
            return AcessoService.AcessoToken.ObterRegistro(_userLogged, GetAllRecords().FirstOrDefault(x => x.TokenGuid.Equals(id)));
        }

        public override IQueryable<Token> GetAllRecords()
        {
            return AcessoService.AcessoToken.ObterLista(_userLogged, base.GetAllRecords());
        }

        public override Token GetRecordById(int id)
        {
            return AcessoService.AcessoToken.ObterRegistro(_userLogged, base.GetRecordById(id));
        }

        public override Token GetRecordById(string id)
        {
            return AcessoService.AcessoToken.ObterRegistro(_userLogged, base.GetRecordById(id));
        }

    }
}