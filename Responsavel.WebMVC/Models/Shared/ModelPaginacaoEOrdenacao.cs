﻿namespace Responsavel.WebMVC.Models.Shared
{
    public class ModelPaginacaoEOrdenacao
    {
        public int? Page { get; set; }
        public string SortOrder { get; set; }
        public string SortOld { get; set; }
        public string SortDirection { get; set; }
        public int TotalRegistros { get; set; }

        public string UrlRetorno { get; set; }
        public string PesquisaUrlRetorno { get; set; }
        public string PaginacaoEOrdenacaoUrlRetorno { get; set; }

    }
}
