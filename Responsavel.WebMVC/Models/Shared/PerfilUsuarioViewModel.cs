﻿namespace Responsavel.WebMVC.Models.Shared
{
    public class PerfilUsuarioViewModel
    {
        public string Nome { get; set; }
        public string ImagemPerfil { get; set; }
    }
}