﻿namespace Responsavel.WebMVC.Models.Shared
{
    public class CertificadoModel
    {
        public string NomeCertificado { get; set; }
        public string ImagemParte1 { get; set; }
        public string ImagemParte2 { get; set; }
    }
}