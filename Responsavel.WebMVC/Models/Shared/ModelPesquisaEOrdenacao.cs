﻿namespace Responsavel.WebMVC.Models.Shared
{
    public class ModelPesquisaEOrdenacao
    {
        public int? Page { get; set; }
        public string SortOrder { get; set; }
        public string SortOld { get; set; }
        public string SortDirection { get; set; }
        public int TotalRegistros { get; set; }

        public bool NaoExibirTotalRegistros { get; set; }

        // resultados de Insert/Update
        public bool ActionResult { get; set; }
        public string ActionMessage { get; set; }
        public int ActionRegistroId { get; set; }
    }
}
