﻿// 21:28 03 04 2018  AvaliacaoDiagnostica.WebMVC  AvaliacaoDiagnostica.Solution

using System.Collections.Generic;

namespace Responsavel.WebMVC.Models.Shared
{
    public class BarraDashboardViewModel
    {
        public List<ItemBarraDashboardViewModel> Item { get; set; }
    }

    public class ItemBarraDashboardViewModel
    {
        public string Icone { get; set; }
        public decimal Valor { get; set; }
        public string Descricao { get; set; }
        public string CssCorBarra { get; set; }
        public string Mensagem { get; set; }
    }
}