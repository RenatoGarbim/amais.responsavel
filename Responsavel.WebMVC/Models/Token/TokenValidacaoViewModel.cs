﻿using AMais.Responsavel.DTO.Integracao;

namespace Responsavel.WebMVC.Models.Token
{
    public class TokenValidacaoViewModel
    {
        public AMaisTurmaEstudanteDTO Registro { get; set; }

        public string Token { get; set; }
    }
}