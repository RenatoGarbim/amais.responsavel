﻿using AMais.Responsavel.DTO.Integracao;
using AMais.Responsavel.DTO.Pesquisa;
using Responsavel.WebMVC.Models.Shared;

namespace Responsavel.WebMVC.Models.Token
{
    public class TokenModelEdit
    {

        public ModelPaginacaoEOrdenacao PaginacaoEOrdenacao { get; set; }

        public TokenPesquisaDTO Pesquisa { get; set; }

        public AMaisTurmaEstudanteDTO Registro { get; set; }
    }
}