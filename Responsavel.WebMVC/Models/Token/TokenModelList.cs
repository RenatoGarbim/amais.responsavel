﻿using System.Collections.Generic;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Pesquisa;
using Responsavel.WebMVC.Models.Shared;

namespace Responsavel.WebMVC.Models.Token
{
    public class TokenModelList
    {
        public ModelPaginacaoEOrdenacao PaginacaoEOrdenacao { get; set; }

        public TokenPesquisaDTO Pesquisa { get; set; }

        public IList<TokenDTOLista> Lista { get; set; }

    }
}