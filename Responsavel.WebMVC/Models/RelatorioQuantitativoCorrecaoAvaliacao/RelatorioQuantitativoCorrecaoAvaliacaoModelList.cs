﻿// 18:28 04 10 2017  Atena.WebMVC  Atena.Web.Solution

using System;
using System.Collections.Generic;
using AMais.Responsavel.DTO.Integracao;
using AMais.Responsavel.DTO.Integracao.AgendaDoEstudante;
using Responsavel.WebMVC.Models.Shared;

namespace Responsavel.WebMVC.Models.RelatorioQuantitativoCorrecaoAvaliacao
{
    [Serializable]
    public class RelatorioQuantitativoCorrecaoAvaliacaoModelList
    {
        public ModelPaginacaoEOrdenacao PaginacaoEOrdenacao { get; set; }

        public AvaliacaoRelatorioPesquisaDTO Pesquisa { get; set; }

        public IEnumerable<AvaliacaoQuantitativaCorrecaoAvaliacaoDTO> Registros { get; set; }
        public IEnumerable<AvaliacaoQuantitativaDTO> RegistrosAvaliacao { get; set; }

        public BarraDashboardViewModel BarraDashboard { get; set; }

        public IEnumerable<EstudanteDadosDTO> Estudantes { get; set; }

    }
}