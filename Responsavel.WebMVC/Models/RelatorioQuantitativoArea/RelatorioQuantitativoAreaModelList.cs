﻿// 18:28 04 10 2017  Atena.WebMVC  Atena.Web.Solution

using System;
using System.Collections.Generic;
using AMais.Responsavel.DTO.Integracao;
using AMais.Responsavel.DTO.Integracao.AgendaDoEstudante;
using Responsavel.WebMVC.Models.Shared;

namespace Responsavel.WebMVC.Models.RelatorioQuantitativoArea
{
    [Serializable]
    public class RelatorioQuantitativoAreaModelList
    {
        public ModelPaginacaoEOrdenacao PaginacaoEOrdenacao { get; set; }

        public AvaliacaoRelatorioPesquisaDTO Pesquisa { get; set; }

        public IEnumerable<EstudanteDadosDTO> Estudantes { get; set; }

        public IEnumerable<AvaliacaoQuantitativaAreaDTO> Registros { get; set; }

        public BarraDashboardViewModel BarraDashboard { get; set; }
    }
}