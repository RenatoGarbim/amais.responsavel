﻿using AMais.Responsavel.DTO;

namespace Responsavel.WebMVC.Models.MeuPerfil
{

    public class ViewModelParaMeuPerfil
    {
        public PerfilResponsavelDTO RegistroUsuario { get; set; }

    }
}