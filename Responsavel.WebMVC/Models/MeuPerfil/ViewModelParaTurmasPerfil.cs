﻿using System.Collections.Generic;
using AMais.AgendaDoEstudante.DTO;

namespace AgendaDoEstudanteWeb.Models.Perfil
{
    public class ViewModelParaTurmasPerfil
    {
        public TurmaViewModel RegistroTurma { get; set; }
        public IEnumerable<UsuarioEscolaViewModel> ListaEscolas { get; set; }
    }
}