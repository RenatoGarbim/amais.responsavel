﻿using System.Collections.Generic;
using AMais.Responsavel.DTO;
using Responsavel.WebMVC.Models.Shared;

namespace Responsavel.WebMVC.Models
{
    public class ViewModelParaHome
    {
        public IEnumerable<TarefaDTO> RegistrosTarefas { get; set; }
        public IEnumerable<TurmaDisciplinaTrabalhoViewModel> RegistrosTrabalhos { get; set; }
        public IEnumerable<TurmaDisciplinaAulaViewModel> RegistrosAulas { get; set; }
        public IEnumerable<TurmaDisciplinaExameViewModel> RegistrosExames { get; set; }

        public int Tarefas { get; set; }
        public int TarefasPendentes { get; set; }
        public int AulasFuturas { get; set; }
        public int TrabalhosFuturos { get; set; }
        public int ExamesFuturos { get; set; }

        public PerfilUsuarioViewModel Perfil { get; set; }

    }
}