﻿using System;
using System.Web;
using System.Web.Mvc;
using AMais.Responsavel.Application.Interface;

namespace Responsavel.WebMVC.App_Helpers
{
    public static class ApplicationCookies
    {
        public static void LimparCookieUsuario(ControllerContext context)
        {
            var userName = context.HttpContext.Request.Cookies["userName"];
            var imageName = context.HttpContext.Request.Cookies["imageName"];

            if (userName != null)
            {
                userName.Expires = DateTime.Now.AddDays(-1);
                context.HttpContext.Response.Cookies.Add(userName);
            }

            if (imageName == null) return;

            imageName.Expires = DateTime.Now.AddDays(-1);
            context.HttpContext.Response.Cookies.Add(imageName);
        }

        public static void DefinirCookieUsuario(ControllerContext context, IUsuarioAppService usuarioAppService, string userId)
        {

            if (string.IsNullOrEmpty(userId)) return;

            var userName = context.HttpContext.Request.Cookies["userName"];
            var imageName = context.HttpContext.Request.Cookies["imageName"];

            if  (userName != null && imageName != null) return;

            WriteDataCookie(context, usuarioAppService, userId);
        }

        public static void DefinirCookieUsuarioWithForceReload(ControllerContext context, IUsuarioAppService usuarioAppService, string userId)
        {
            WriteDataCookie(context, usuarioAppService, userId);
        }

        private static void WriteDataCookie(ControllerContext context, IUsuarioAppService usuarioAppService, string userId)
        {
            var usuario = usuarioAppService.ObterDadosDoUsuario(userId);
            var cookie = new HttpCookie("userName") { Value = usuario.Nome };
            var cookieImage = new HttpCookie("imageName") { Value = usuario.CaminhoImagem };

            context.HttpContext.Response.Cookies.Add(cookie);
            context.HttpContext.Response.Cookies.Add(cookieImage);
            context.Controller.TempData["userName"] = usuario.Nome;
            context.Controller.TempData["imageName"] = usuario.CaminhoImagem;
        }
    }
}