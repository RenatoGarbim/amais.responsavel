﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Microsoft.Ajax.Utilities;
using Responsavel.WebMVC.Models.Shared;

namespace Responsavel.WebMVC.App_Helpers
{
    public static class HtmlHelperExtensions
    {
        private static string _displayVersion;

        /// <summary>
        ///     Retrieves a non-HTML encoded string containing the assembly version as a formatted string.
        ///     <para>If a project name is specified in the application configuration settings it will be prefixed to this value.</para>
        ///     <para>
        ///         e.g.
        ///         <code>1.0 (build 100)</code>
        ///     </para>
        ///     <para>
        ///         e.g.
        ///         <code>ProjectName 1.0 (build 100)</code>
        ///     </para>
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static IHtmlString AssemblyVersion(this HtmlHelper helper)
        {
            if (_displayVersion.IsNullOrWhiteSpace())
                SetDisplayVersion();

            return helper.Raw(_displayVersion);
        }

        /// <summary>
        ///     Compares the requested route with the given <paramref name="value" /> value, if a match is found the
        ///     <paramref name="attribute" /> value is returned.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="value">The action value to compare to the requested route action.</param>
        /// <param name="attribute">The attribute value to return in the current action matches the given action value.</param>
        /// <returns>A HtmlString containing the given attribute value; otherwise an empty string.</returns>
        public static IHtmlString RouteIf(this HtmlHelper helper, string value, string attribute)
        {
            var currentController =
                (helper.ViewContext.RequestContext.RouteData.Values["controller"] ?? string.Empty).ToString().UnDash();
            var currentAction =
                (helper.ViewContext.RequestContext.RouteData.Values["action"] ?? string.Empty).ToString().UnDash();

            var hasController = value.Equals(currentController, StringComparison.InvariantCultureIgnoreCase);
            var hasAction = value.Equals(currentAction, StringComparison.InvariantCultureIgnoreCase);

            return hasAction || hasController ? new HtmlString(attribute) : new HtmlString(string.Empty);
        }

        /// <summary>
        ///     Renders the specified partial view with the parent's view data and model if the given setting entry is found and
        ///     represents the equivalent of true.
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="appSetting">The key value of the entry point to look for.</param>
        public static void RenderPartialIf(this HtmlHelper htmlHelper, string partialViewName, string appSetting)
        {
            var setting = Settings.GetValue<bool>(appSetting);

            htmlHelper.RenderPartialIf(partialViewName, setting);
        }

        /// <summary>
        ///     Renders the specified partial view with the parent's view data and model if the given setting entry is found and
        ///     represents the equivalent of true.
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="condition">The boolean value that determines if the partial view should be rendered.</param>
        public static void RenderPartialIf(this HtmlHelper htmlHelper, string partialViewName, bool condition)
        {
            if (!condition)
                return;

            htmlHelper.RenderPartial(partialViewName);
        }

        /// <summary>
        ///     Retrieves a non-HTML encoded string containing the assembly version and the application copyright as a formatted
        ///     string.
        ///     <para>If a company name is specified in the application configuration settings it will be suffixed to this value.</para>
        ///     <para>
        ///         e.g.
        ///         <code>1.0 (build 100) © 2015</code>
        ///     </para>
        ///     <para>
        ///         e.g.
        ///         <code>1.0 (build 100) © 2015 CompanyName</code>
        ///     </para>
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static IHtmlString Copyright(this HtmlHelper helper)
        {
            var copyright = $"{helper.AssemblyVersion()} &copy; {DateTime.Now.Year} {Settings.Company}".Trim();

            return helper.Raw(copyright);
        }

        private static void SetDisplayVersion()
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version;

            _displayVersion =
                string.Format("{4} {0}.{1}.{2} (build {3})", version.Major, version.Minor, version.Build,
                    version.Revision, Settings.Project).Trim();
        }

        /// <summary>
        ///     Returns an unordered list (ul element) of validation messages that utilizes bootstrap markup and styling.
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="alertType">The alert type styling rule to apply to the summary element.</param>
        /// <param name="heading">The optional value for the heading of the summary element.</param>
        /// <returns></returns>
        public static HtmlString ValidationBootstrap(this HtmlHelper htmlHelper, string alertType = "danger",
            string heading = "")
        {
            if (htmlHelper.ViewData.ModelState.IsValid)
                return new HtmlString(string.Empty);

            var sb = new StringBuilder();

            sb.AppendFormat("<div class=\"alert alert-{0} alert-block\">", alertType);
            sb.Append("<button class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>");

            if (!heading.IsNullOrWhiteSpace())
            {
                sb.AppendFormat("<h4 class=\"alert-heading\">{0}</h4>", heading);
            }

            sb.Append(htmlHelper.ValidationSummary());
            sb.Append("</div>");

            return new HtmlString(sb.ToString());
        }


        public static HtmlString ExibirBotaoMensagemErro(this HtmlHelper htmlHelper, string botao)
        {

            if (string.IsNullOrEmpty(botao))
                return new HtmlString("");

            var sb = new StringBuilder();
            sb.AppendFormat(botao);

            return new HtmlString(sb.ToString());
        }

        #region Razor Absolute Path Controller
        private static string AbsolutePath(this UrlHelper urlHelper, string relativePath)
        {
            return new Uri(urlHelper.RequestContext.HttpContext.Request.Url, relativePath).ToString();
        }

        public static string AbsoluteAction(this UrlHelper urlHelper, string actionName, string controllerName)
        {
            return AbsolutePath(urlHelper,
                                urlHelper.Action(actionName, controllerName));
        }

        public static string AbsoluteAction(this UrlHelper urlHelper, string actionName, string controllerName, object routeValues)
        {
            return AbsolutePath(urlHelper, urlHelper.Action(actionName, controllerName, routeValues));
        }

        #endregion

        #region --- Display

        public static HtmlString DisplayForPhone(this HtmlHelper helper, string phone)
        {
            try
            {
                if (phone == null)
                {
                    return new HtmlString(string.Empty);
                }
                string formated = phone;
                if (phone.Length == 10)
                {
                    formated = $"({phone.Substring(0, 3)}) {phone.Substring(3, 3)}-{phone.Substring(6, 4)}";
                }
                else if (phone.Length == 7)
                {
                    formated = $"{phone.Substring(0, 3)}-{phone.Substring(3, 4)}";
                }
                string s = $"<a href='tel:{phone}'>{formated}</a>";
                return new HtmlString(s);
            }
            catch
            {
                return new HtmlString(phone);
            }
        }

        public static HtmlString DisplayForCellPhone(this HtmlHelper helper, string phone)
        {
            try
            {
                if (phone == null)
                {
                    return new HtmlString(string.Empty);
                }
                string formated = phone;
                if (phone.Length == 11)
                {
                    formated = $"({phone.Substring(0, 2)}) {phone.Substring(2, 1)} {phone.Substring(3, 5)}-{phone.Substring(7, 4)}";
                }
                else if (phone.Length == 10)
                {
                    formated = $"({phone.Substring(0, 2)}) {phone.Substring(2, 1)} {phone.Substring(3, 5)}-{phone.Substring(7, 4)}";
                }
                else if (phone.Length == 7)
                {
                    formated = $"{phone.Substring(0, 3)}-{phone.Substring(3, 4)}";
                }
                string s = $"<a href='tel:{phone}'>{formated}</a>";
                return new HtmlString(s);
            }
            catch
            {
                return new HtmlString(phone);
            }

        }

        public static HtmlString DisplayUserRoles(this HtmlHelper helper)
        {
            var isCoordenador = HttpContext.Current.User.IsInRole("Coordenador");
            var isProfessor = HttpContext.Current.User.IsInRole("Professor");
            var userRoles = isCoordenador ? "Coordenador" : "";
            userRoles += (isCoordenador && isProfessor ? " / " : "");
            userRoles += isProfessor ? "Professor" : "";

            return new HtmlString(userRoles);
        }

        #endregion

        public static string ContentArea(this UrlHelper url, string path)
        {
            var area = url.RequestContext.RouteData.DataTokens["area"];

            if (area == null) return string.Empty;

            if (!string.IsNullOrEmpty(area.ToString()))
                area = "Areas/" + area;

            // Simple checks for '~/' and '/' at the
            // beginning of the path.
            if (path.StartsWith("~/"))
                path = path.Remove(0, 2);

            if (path.StartsWith("/"))
                path = path.Remove(0, 1);

            path = path.Replace("../", string.Empty);

            return VirtualPathUtility.ToAbsolute("~/" + area + "/" + path);
        }

        public static string RootContentArea(this UrlHelper url, string path)
        {
            var pathFinal = VirtualPathUtility.ToAbsolute("~/Content/" + path);
            return pathFinal;
        }

        public static string QuestaoImageContentArea(this UrlHelper url, string path)
        {
            var pathFinal = VirtualPathUtility.ToAbsolute("~/QuestaoImagens/" + path);
            return pathFinal;
        }



        public static HtmlString CreatePageList(this HtmlHelper htmlHelper, int totalRegistros, int paginaAtual, Func<int, string> genereateUrlPage)
        {
            var sb = new StringBuilder();

            sb.Append("<div class=\"pagination-container\">");

            sb.Append("<ul class=\"pagination\">");

            sb.Append($"<li><span>Registros : {totalRegistros:N0} </span></li>");

            var paginas = Convert.ToInt32(Math.Ceiling((double)totalRegistros / 15));
            var totalPaginasParaExibicao = paginas <= 10 ? paginas : paginaAtual < 10 ? 10 : paginaAtual + 4;
            if (totalPaginasParaExibicao > paginas)
                totalPaginasParaExibicao = paginas;

            var paginaInicial = paginaAtual < 10 ? 1 : paginaAtual - 5;

            if (totalPaginasParaExibicao > 10)
                sb.AppendFormat("<li class=\"PagedList-skipToFirst\"><a href={0}>««</a></li>", genereateUrlPage.Invoke(1));

            if (paginaAtual > 1 && totalPaginasParaExibicao > 1)
            {
                sb.AppendFormat("<li class=\"PagedList-skipToPrevious\"><a href={0}>«</a></li>", genereateUrlPage.Invoke(paginaAtual - 1));
            }

            for (var i = paginaInicial; i <= totalPaginasParaExibicao; i++)
            {
                if (i == paginaAtual)
                    sb.AppendFormat("<li class=\"active\"><span>{0}</span></li>", i);
                else
                    sb.AppendFormat("<li><a href={1}>{0}</a></li>", i, genereateUrlPage.Invoke(i));
            }

            if (paginaAtual + 1 <= paginas)
                sb.AppendFormat("<li class=\"PagedList-skipToNext\"><a href={0}>»</a></li>", genereateUrlPage.Invoke(paginaAtual + 1));

            if (paginas > 10 && paginas != paginaAtual)
                sb.AppendFormat("<li class=\"PagedList-skipToLast\"><a href={0}>»»</a></li>", genereateUrlPage.Invoke(paginaAtual + paginaAtual + 1 <= paginas ? paginaAtual + 10 : paginas));

            sb.Append("</ul>");
            sb.Append("</div>");

            return new HtmlString(sb.ToString());
        }

        public static MvcHtmlString CheckedRadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object value)
        {
            return CheckedRadioButtonFor(htmlHelper, expression, value, null);
        }

        public static MvcHtmlString CheckedRadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object value, object htmlAttributes)
        {
            var func = expression.Compile();
            var attributes = new RouteValueDictionary(htmlAttributes);
            if ((object)func(htmlHelper.ViewData.Model) == value)
            {
                attributes["checked"] = "checked";
            }
            return htmlHelper.RadioButtonFor(expression, value, attributes);
        }

        public static bool FiltrandoPor(this HtmlHelper helper, ModelPaginacaoEOrdenacao carregarDe, string origem)
        {
            return !string.IsNullOrEmpty(carregarDe?.UrlRetorno) && carregarDe.UrlRetorno.ToUpper().Equals(origem.ToUpper());
        }

    }
}