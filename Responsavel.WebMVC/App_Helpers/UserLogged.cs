﻿using System.Web;
using Linte.Core.CrossCutting.Class;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Identity.Infra.CrossCutting.Identity.Interface;
using Microsoft.AspNet.Identity;

namespace Responsavel.WebMVC.App_Helpers
{
    public class UserLogged : IUserLogged
    {
        private string UserId { get; set; }
        private string UserName { get; set; }
        private string UserEmail { get; set; }
        private UsuarioLogado Usuario { get; set; }

        public UserLogged(IIdentityService identityService)
        {
            if (HttpContext.Current?.User == null || !HttpContext.Current.User.Identity.IsAuthenticated) return;

            try
            {
                UserId = HttpContext.Current.User.Identity.GetUserId();
                UserName = HttpContext.Current.User.Identity.GetUserName();
                UserEmail = HttpContext.Current.User.Identity.GetUserName();
                Usuario = identityService.UsuarioObterPorId(UserId);
            }
            catch
            {
                HttpContext.Current.Response.Redirect("~/Account/LogOff");
            }

        }

        public string GetUserId()
        {
            return UserId;
        }

        public string GetUserName()
        {
            return UserName;
        }

        public string GetUserEmail()
        {
            return UserEmail;
        }

        public UsuarioLogado GetUsuario()
        {
            return Usuario;
        }
    }
}