﻿using System.Runtime.Serialization.Json;
using Linte.Core.Apoio.Utilities;
using Responsavel.WebMVC.Models.Shared;

namespace Responsavel.WebMVC.App_Helpers
{
    internal static class FunctionToView
    {
        public static object JSonDecode<T>(string jSonModel)
        {
            var ser = new DataContractJsonSerializer(typeof(T));
            return (T) ser.ReadObject(LINTJSonController.GenerateStreamFromString(jSonModel));
        }

        public static object SortDataGrid<T>(string jSonModel, string defaultColumnSort, string sortOrder) where T : ModelPaginacaoEOrdenacao
        {
            var formOperation = jSonModel.Length > 0 ? (ModelPaginacaoEOrdenacao) JSonDecode<T>(jSonModel) : new ModelPaginacaoEOrdenacao();

            sortOrder = (string.IsNullOrEmpty(sortOrder) ? defaultColumnSort : sortOrder);
            var sortOld = (!string.IsNullOrEmpty(formOperation.SortOld) ? formOperation.SortOld : "");
            var sortDirection = (!sortOrder.Equals(sortOld) ? "ASC" : ("ASC".Equals(formOperation.SortDirection) ? "DESC" : "ASC"));

            formOperation.SortOrder = sortOrder;
            formOperation.SortOld = sortOrder;
            formOperation.SortDirection = sortDirection;
            formOperation.Page = 1;

            return formOperation;
        }


        public static object SortDataGrid<T>(T formOperation, string defaultColumnSort, string sortOrder) where T : ModelPaginacaoEOrdenacao
        {
            sortOrder = (string.IsNullOrEmpty(sortOrder) ? defaultColumnSort : sortOrder);
            var sortOld = (!string.IsNullOrEmpty(formOperation.SortOld) ? formOperation.SortOld : "");
            var sortDirection = (!sortOrder.Equals(sortOld) ? "ASC" : ("ASC".Equals(formOperation.SortDirection) ? "DESC" : "ASC"));

            formOperation.SortOrder = sortOrder;
            formOperation.SortOld = sortOrder;
            formOperation.SortDirection = sortDirection;
            formOperation.Page = 1;

            return formOperation;
        }
        
        public static int DefinePageNumber(int pageNumber, int totalRecno)
        {
            return totalRecno < ((pageNumber - 1) * 15) ? 1 : pageNumber;
        }
    }
}
