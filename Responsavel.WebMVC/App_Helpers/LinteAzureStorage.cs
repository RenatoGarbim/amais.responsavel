﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Linte.Core.DomainEvent.Events;
using Linte.Core.DomainEvent.Interfaces;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;

namespace Responsavel.WebMVC.App_Helpers
{
    public static class LinteAzureStorage
    {
        public static CloudStorageAccount CreateStorageAccountFromConnectionString()
        {
            CloudStorageAccount storageAccount;

            try
            {
                storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            }
            catch (FormatException)
            {
                throw;
            }
            catch (ArgumentException)
            {
                throw;
            }

            return storageAccount;
        }

        public static Dictionary<string, string> CarregarArquivo(string userId)
        {
            var storageAccount = CreateStorageAccountFromConnectionString();
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("responsavel-imagem-perfil");
            var blobs = container.ListBlobs(useFlatBlobListing: true);

            var retorno = new Dictionary<string, string>();
            foreach (var arquivo in blobs.Select(blob => blob.StorageUri.PrimaryUri.ToString()).Where(x => x.Contains(userId)).ToList())
            {
                retorno.Add(arquivo, arquivo.Replace($"https://lintelligencehd.blob.core.windows.net/responsavel-imagem-perfil/", ""));
            }

            return retorno;
        }

        public static bool SalvarArquivo(HttpPostedFileBase files, string fileName, IHandler<DomainNotification> notifications)
        {
            if (files.ContentLength > 11536000)
            {
                notifications.Handle(new DomainNotification("ImgUpLoadError", "A imagem/arquivo selecionado não é válida para upload, selecione um arquivo menor!"));
                return false;
            }
            else
            {
                var storageAccount = CreateStorageAccountFromConnectionString();
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("responsavel-imagem-perfil");
                try
                {
                    var requestOptions = new BlobRequestOptions() { RetryPolicy = new NoRetry() };
                    container.CreateIfNotExists(requestOptions, null);
                }
                catch (StorageException e)
                {
                    notifications.Handle(new DomainNotification("ErroStorage", e.Message));
                    throw;
                }
                var blockBlob = container.GetBlockBlobReference(fileName);
                blockBlob.Properties.ContentType = files.ContentType;
                using (var fileStream = files.InputStream)
                {
                    blockBlob.UploadFromStream(fileStream);
                }
                return true;
            }
        }

        public static bool ApagarArquivo(string arquivo, IHandler<DomainNotification> notifications)
        {
            var storageAccount = CreateStorageAccountFromConnectionString();
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("responsavel-imagem-perfil");
            try
            {
                Parallel.ForEach(container.ListBlobs().Where(y => y.Uri.ToString().Contains(arquivo)), y => { ((CloudBlockBlob)y).DeleteIfExistsAsync(); });
                return true;

            }
            catch (Exception e)
            {
                notifications.Handle(new DomainNotification("ErroLinteAzure", e.Message));
                return false;
            }
        }

    }
}