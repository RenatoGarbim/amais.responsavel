﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace Responsavel.WebMVC.App_Helpers
{
    public class AsIsBundleOrderer: IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}