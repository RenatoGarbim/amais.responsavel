﻿using Microsoft.Owin;
using Owin;
using Responsavel.WebMVC;

[assembly: OwinStartup(typeof(Startup))]
namespace Responsavel.WebMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
