﻿using System;
using System.Web.Mvc;
using Linte.Core.DomainEvent.Events;
using Linte.Core.Log.Domain.Class;
using Linte.Core.Log.Domain.Entities;
using Linte.Core.Log.Domain.Events;

namespace Responsavel.WebMVC.ErrorHandler
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class LogHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
           
            if (filterContext?.HttpContext == null || filterContext.Exception == null) return;

            try
            {
                var ticketNumber = Guid.NewGuid();
                filterContext.Controller.TempData["TicketNumber"] = ticketNumber;
                filterContext.Controller.TempData["ErrorMessage"] = filterContext.Exception.Message;
                var registroLog = new LogDataTable()
                {
                    Ticked = ticketNumber,
                    TipoOperacao = TipoOperacao.OcorrenciaExcecao,
                    Ocorrencia = filterContext.Exception.Message + Environment.NewLine + filterContext.Exception.StackTrace,
                    Controller = $"{filterContext.Controller}"
                };
                DomainEvent.Raise(new WriteLogEvent(registroLog));
            }
            catch
            {
                // ignored
            }
            base.OnException(filterContext);
        }
    }
}