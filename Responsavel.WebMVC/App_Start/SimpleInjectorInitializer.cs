using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AMais.Responsavel.CrossCutting.IoC;
using CommonServiceLocator.SimpleInjectorAdapter;
using Linte.Core.CrossCutting.Interfaces;
using Linte.Core.DomainEvent.CrossCutting.IoC;
using Linte.Core.DomainEvent.Events;
using Linte.Core.Infra.Data.CrossCutting.IoC;
using Linte.Core.Log.CrossCutting.IoC;
using Linte.Identity.Infra.CrossCutting.IoC;
using Microsoft.Owin;
using Microsoft.Practices.ServiceLocation;
using Responsavel.WebMVC;
using Responsavel.WebMVC.App_Helpers;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using Container = SimpleInjector.Container;

[assembly: WebActivator.PostApplicationStartMethod(typeof(SimpleInjectorInitializer), "Initialize")]

namespace Responsavel.WebMVC
{
    public static class SimpleInjectorInitializer
    {
        private static void InitializeContainer(Container container)
        {
            container.Register<IUserLogged, UserLogged>(Lifestyle.Scoped);

            ResponsavelBootStrapper.RegisterServices(container);
            BootStrapperDomainEvent.RegisterServices(container);
            BootStrapperLog.RegisterServices(container);
            BootStrapperIdentity.RegisterServices(container);
            BootStrapperCore.RegisterServices(container);
        }

        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            InitializeContainer(container);

            container.Register(() =>
            {
                if (HttpContext.Current != null && HttpContext.Current.Items["owin.Enviroment"] == null && container.IsVerifying)
                {
                    return new OwinContext().Authentication;
                }
                return HttpContext.Current.GetOwinContext().Authentication;
            }, Lifestyle.Scoped);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            var adapter = new SimpleInjectorServiceLocatorAdapter(container);
            ServiceLocator.SetLocatorProvider(() => adapter);
            DomainEvent.Container = new DomainEventsContainer(DependencyResolver.Current);

             container.Verify();  
        }
    }
}