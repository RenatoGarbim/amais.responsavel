﻿using System.Web.Mvc;
using Linte.Core.MvcFilters;

namespace Responsavel.WebMVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new AuthorizeAttribute());
            filters.Add(new ErrorHandler.AiHandleErrorAttribute());
            filters.Add(new ErrorHandler.LogHandleErrorAttribute());
            filters.Add(new GlobalLogHandler());
            filters.Add(new ActionExecutingUserCookie());
        }
    }
}
