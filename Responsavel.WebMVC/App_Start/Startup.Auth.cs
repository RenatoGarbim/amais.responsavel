﻿using System;
using System.Web.Configuration;
using System.Web.Mvc;
using Linte.Identity.Infra.CrossCutting.Identity.Configuration;
using Linte.Identity.Infra.CrossCutting.Identity.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;

namespace Responsavel.WebMVC
{
    public partial class Startup
    {
        public static IDataProtectionProvider DataProtectionProvider { get; set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {

            DataProtectionProvider = app.GetDataProtectionProvider();

            app.CreatePerOwinContext(() => DependencyResolver.Current.GetService<ApplicationUserManager>());

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            var chaveFacebook = WebConfigurationManager.AppSettings["KeyLoginFacebook"];
            if (string.IsNullOrEmpty(chaveFacebook))
                chaveFacebook = "0000;0000";
            
            var facebookAuthenticationOptions = new FacebookAuthenticationOptions()
            {
                AppId = chaveFacebook.Split(';')[0],
                AppSecret = chaveFacebook.Split(';')[1]
            };
            facebookAuthenticationOptions.Scope.Add("email");

            facebookAuthenticationOptions.UserInformationEndpoint = "https://graph.facebook.com/v2.7/me?fields=id,name,email";
            app.UseFacebookAuthentication(facebookAuthenticationOptions);

            var chaveGoogle = Environment.GetEnvironmentVariable("KeyLoginGoogle");
            if (string.IsNullOrEmpty(chaveGoogle))
                chaveGoogle = "0000;0000";

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = chaveGoogle.Split(';')[0],
                ClientSecret = chaveGoogle.Split(';')[1]
            });

        }
    }
}