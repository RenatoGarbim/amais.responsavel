﻿using System.Web.Mvc;

namespace Responsavel.WebMVC
{
    public class ActionExecutingUserCookie : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userName = filterContext.HttpContext.Request.Cookies["userName"];
            var imageName = filterContext.HttpContext.Request.Cookies["imageName"];

            if (userName != null) filterContext.Controller.TempData["userName"] = userName.Value;
            if (imageName != null) filterContext.Controller.TempData["imageName"] = imageName.Value;
        }
    }
}