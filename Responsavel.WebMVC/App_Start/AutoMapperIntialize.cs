﻿using AMais.Responsavel.CrossCutting.AutoMapper;
using AutoMapper;
using Responsavel.WebMVC;

[assembly: WebActivator.PostApplicationStartMethod(typeof(AutoMapperIntialize), "Initialize")]
namespace Responsavel.WebMVC
{
    public static class AutoMapperIntialize
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.AllowNullDestinationValues = true;

                BootStrapperAutoMapper.ConfigAction.Invoke(cfg);
            });

#if DEBUG
            Mapper.AssertConfigurationIsValid();
#endif

        }
 
    }
}