﻿using System.Web.Optimization;
using Responsavel.WebMVC.App_Helpers;

namespace Responsavel.WebMVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.mask.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            // material
            var materialOrder = new ScriptBundle("~/bundles/material-design").Include(
                "~/Scripts/core/popper.min.js",
                "~/Scripts/core/bootstrap-material-design.min.js",
                "~/Scripts/plugins/perfect-scrollbar.jquery.min.js",
                "~/Scripts/plugins/moment.min.js",
                "~/Scripts/plugins/jquery.bootstrap-wizard.js", /* JQ*/
                "~/Scripts/plugins/jquery.dataTables.min.js", /* JQ*/
                "~/Scripts/plugins/jquery-jvectormap.js", /* JQ*/
                "~/Scripts/plugins/bootstrap-selectpicker.js", /* JQ*/
                "~/Scripts/plugins/bootstrap-datetimepicker.js", /* JQ*/
                "~/Scripts/plugins/bootstrap-tagsinput.js", /* JQ*/
                "~/Scripts/plugins/bootstrap-notify.js", /* JQ*/
                "~/Scripts/plugins/fullcalendar.min.js", /* JQ*/
                "~/Scripts/plugins/nouislider.min.js", /* JQ*/
                "~/Scripts/plugins/chartist.min.js", /* JQ*/
                "~/Scripts/plugins/sweetalert2.js", /* JQ*/
                "~/Scripts/material-dashboard.js");
            materialOrder.Orderer = new AsIsBundleOrderer();
            bundles.Add(materialOrder);

            // linte scripts
            bundles.Add(new ScriptBundle("~/bundles/linte-scripts").Include(
                "~/Scripts/linte/ControleMenu.js",
                "~/Scripts/linte/MenuNavegacaoPrincipal.js",
                "~/Scripts/linte/ManutencaoRegistro.js",
                "~/Scripts/linte/MascarasFormulario.js",
                "~/Scripts/linte/ControleNotificacao.js",
                "~/Scripts/WaitModal.js",
                "~/Scripts/linte/Zendesk.js"));

            bundles.Add(new ScriptBundle("~/bundles/Chart").Include(
                "~/Scripts/Chart.bundle.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(                    
                    "~/Content/material-dashboard.css",
                    "~/Content/OverLoadTheme.css",
                    "~/Content/EstilosGerais.css",
                    "~/Content/EstilosGeraisFormularios.css",
                    "~/Content/EstilosPaginaLogin.css",
                    "~/Content/EstilosRelatorio.css",
                    "~/Content/Botoes.css",
                    "~/Content/vertical-nav.css",
                    "~/Content/LayoutEscola.css",
                    "~/Content/Cards.css",
                    "~/Content/site.css"));
        }
    }
}
