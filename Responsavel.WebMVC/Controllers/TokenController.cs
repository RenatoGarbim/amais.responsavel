﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AMais.Responsavel.Application.Interface;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.DTO;
using AMais.Responsavel.DTO.Pesquisa;
using Linte.Core.DomainEvent.Events;
using Responsavel.WebMVC.App_Helpers;
using Responsavel.WebMVC.Models.Shared;
using Responsavel.WebMVC.Models.Token;

namespace Responsavel.WebMVC.Controllers
{
    [Authorize]
    public class TokenController : BaseController
    {
        private readonly ITokenAppService _tokenAppService;
        private readonly IAmaisTurmaEstudanteAppService _amaisTurmaEstudanteAppService;

        public TokenController(ITokenAppService tokenAppService, IAmaisTurmaEstudanteAppService amaisTurmaEstudanteAppService)
        {
             _tokenAppService = tokenAppService;
            _amaisTurmaEstudanteAppService = amaisTurmaEstudanteAppService;
        }

        private async Task<TokenModelList> CarregarViewDataAsync(TokenModelList model = null)
        {
            if (model == null)
                model = new TokenModelList();

            if (model.PaginacaoEOrdenacao == null)
                model.PaginacaoEOrdenacao = new ModelPaginacaoEOrdenacao() { SortOrder = "Descricao", SortDirection = "ASC", SortOld = "Descricao" };

            if (model.Pesquisa == null)
                model.Pesquisa = new TokenPesquisaDTO();

            var task = Task.Run(() => _tokenAppService.ObterTodos(model.PaginacaoEOrdenacao.Page ?? 1, model.PaginacaoEOrdenacao.SortOrder, model.PaginacaoEOrdenacao.SortDirection, model.Pesquisa));
            var registros = await task;

            model.Lista = new List<TokenDTOLista>();
            foreach (var registro in registros)
            {
                var registroToken = _amaisTurmaEstudanteAppService.ObterTurmaEstudante(registro.Descricao);
                model.Lista.Add(new TokenDTOLista {UsuarioId = registro.UsuarioId, DescricaoTurma = registroToken.DescricaoTurma, Descricao = registro.Descricao, NomeEscola = registroToken.NomeEscola, NomeEstudante = registroToken.NomeEstudante, DescricaoAno = registroToken.DescricaoAno, Matricula = registroToken.Matricula, TokenGuid = registro.TokenGuid, TokenId = registro.TokenId});
            }
            
            model.PaginacaoEOrdenacao.TotalRegistros = _tokenAppService.TotalDeRegistros();
            return model;
        }

        private TokenModelEdit CarregarViewDataEdicao(TokenModelEdit model)
        {
            return model;
        }

        public async Task<ActionResult> Index()
        {
            GetModelStateData();

            var model = new TokenModelList
            {
                PaginacaoEOrdenacao = (ModelPaginacaoEOrdenacao)TempData["PaginacaoEOrdenacao"],
                Pesquisa = (TokenPesquisaDTO)TempData["Pesquisa"]
            };


            TempData["PaginacaoEOrdenacao"] = null;
            TempData["Pesquisa"] = null;
            model = await CarregarViewDataAsync(model);

            return View(model);
        }


        public async Task<ActionResult> Pesquisar(TokenModelList model)
        {
            TempData["Pesquisa"] = model.Pesquisa;

            return await Task.Run<ActionResult>(() => RedirectToAction("Index"));
        }

        public async Task<ActionResult> LimparPesquisa(TokenModelList model)
        {
            TempData["PaginacaoEOrdenacao"] = model.PaginacaoEOrdenacao;
            return await Task.Run<ActionResult>(() => RedirectToAction("Index"));

        }

        public async Task<ActionResult> OrdenarPor(string iD, string jSonModel, string jSonModelPesquisa)
        {
            var modelPaginacaoEOrdenacao = (ModelPaginacaoEOrdenacao)FunctionToView.SortDataGrid<ModelPaginacaoEOrdenacao>(jSonModel, "Descricao", iD);
            var modelPesquisa = (TokenPesquisaDTO)FunctionToView.JSonDecode<TokenPesquisaDTO>(jSonModelPesquisa);

            TempData["PaginacaoEOrdenacao"] = modelPaginacaoEOrdenacao;
            TempData["Pesquisa"] = modelPesquisa;

            return await Task.Run<ActionResult>(() => RedirectToAction("Index"));
        }

        public async Task<ActionResult> IrParaPagina(int iD, string jSonModel, string jSonModelPesquisa)
        {
            var modelPaginacaoEOrdenacao = (ModelPaginacaoEOrdenacao)FunctionToView.JSonDecode<ModelPaginacaoEOrdenacao>(jSonModel);
            var modelPesquisa = (TokenPesquisaDTO)FunctionToView.JSonDecode<TokenPesquisaDTO>(jSonModelPesquisa);
            modelPaginacaoEOrdenacao.Page = iD;

            TempData["PaginacaoEOrdenacao"] = modelPaginacaoEOrdenacao;
            TempData["Pesquisa"] = modelPesquisa;

            return await Task.Run<ActionResult>(() => RedirectToAction("Index"));
        }

        public async Task<ActionResult> Editar(TokenModelList model, Guid id)
        {
            var registro = _tokenAppService.ObterPorId(id);
            var modelEdit = new TokenModelEdit
            {
                Pesquisa = model.Pesquisa,
                PaginacaoEOrdenacao = model.PaginacaoEOrdenacao,
                Registro = _amaisTurmaEstudanteAppService.ObterTurmaEstudante(registro.Descricao)
            };

            return await Task.Run<ActionResult>(() => View(CarregarViewDataEdicao(modelEdit)));
        }


        public async Task<ActionResult> Recarregar(string messageAction)
        {
            var modelPaginacaoEOrdenacao = (ModelPaginacaoEOrdenacao)TempData["PaginacaoEOrdenacaoTemp"];
            var modelPesquisa = (TokenPesquisaDTO)TempData["PesquisaTemp"];

            TempData["PaginacaoEOrdenacao"] = modelPaginacaoEOrdenacao;
            TempData["Pesquisa"] = modelPesquisa;
            TempData["ActionMessage"] = messageAction;

            return await Task.Run<ActionResult>(() => RedirectToAction("Index"));
        }

        public async Task<ActionResult> Cancelar(TokenModelEdit model)
        {
            TempData["ModelStateError"] = null;
            TempData["PaginacaoEOrdenacao"] = model.PaginacaoEOrdenacao;
            TempData["Pesquisa"] = model.Pesquisa;

            return await Task.Run<ActionResult>(() => RedirectToAction("Index"));
        }

        public async Task<ActionResult> InformarToken()
        {
            return await Task.Run<ActionResult>(() => View("InformarToken", new TokenValidacaoViewModel()));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ValidarUsuario(TokenValidacaoViewModel model)
        {

            model.Registro = _amaisTurmaEstudanteAppService.ObterTurmaEstudante(model.Token);

            if (model.Registro != null) return await Task.Run<ActionResult>(() => View("Validar", model));

            Notifications.Handle(new DomainNotification("Erro", "O token informado não foi localizado na base de dados!"));
            ValidarErrosDominio();
            return await Task.Run<ActionResult>(() => View("InformarToken", model));
        }

        public async Task<ActionResult> Validar(string id)
        {
            var model = new TokenValidacaoViewModel()
            {
                Registro = _amaisTurmaEstudanteAppService.ObterTurmaEstudante(id)
            };

            if (model.Registro != null) return await Task.Run<ActionResult>(() => View("Validar", model));

            Notifications.Handle(new DomainNotification("Erro", "O token informado não foi localizado na base de dados!"));
            ValidarErrosDominio();
            return await Task.Run<ActionResult>(() => View("InformarToken", model));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmarValidacao(TokenValidacaoViewModel model)
        {
            if (!ModelState.IsValid)
                return View("Validar", model);

            var dataResult = _tokenAppService.AdicionarOuAtualizar(new TokenDTO {TokenGuid = Guid.NewGuid(), Descricao = model.Registro.TokenAcesso, TurmaEstudanteId = model.Registro.TurmaEstudanteId});
            ValidarErrosDominio(dataResult.Message);

            if (!dataResult.Result)
                return View("Validar", model);

            return await Task.Run<ActionResult>(() => RedirectToAction("Index", "Home"));
        }
    }
}