﻿using System.Diagnostics;
using System.Reflection;
using System.Web.Mvc;
using AMais.Responsavel.Application.Interface;
using Responsavel.WebMVC.App_Helpers;

namespace Responsavel.WebMVC.Controllers
{
    [Authorize]
    public class AmbienteRedacaoController : BaseController
    {
        // GET: AmbienteRedacao
        private readonly IUsuarioAppService _usuario;

        public AmbienteRedacaoController(IUsuarioAppService usuario)
        {
            _usuario = usuario;
        }

        public ActionResult Index()
        {
            ApplicationCookies.DefinirCookieUsuario(ControllerContext, _usuario, UserId);

            var assembly = Assembly.GetExecutingAssembly();
            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            var version = fvi.FileVersion;
            ViewBag.AtenaVersion = $"{version}";

            return View();
        }

    }
}