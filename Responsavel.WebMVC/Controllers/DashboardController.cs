﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgendaDoEstudante.WebMVC.App_Helpers;
using AgendaDoEstudante.WebMVC.Models;
using AMais.AgendaDoEstudante.Application.Interface;

namespace AgendaDoEstudante.WebMVC.Controllers
{
    public class DashboardController : BaseController
    {
        private readonly ITarefaAppService _tarefaAppService;
        private readonly ITurmaDisciplinaAppService _turmaDisciplinaAppService;
        private readonly IUsuarioAppService _usuario;

        private ViewModelParaHome CarregarViewData(ViewModelParaHome model = null)
        {
            if (model == null)
                model = new ViewModelParaHome();

            model.RegistrosTarefas = _tarefaAppService.ObterListaTarefa(UserId).ToList();
            model.Tarefas = model.RegistrosTarefas.Count();
            model.TarefasPendentes = model.RegistrosTarefas.Count(x => !x.Status);

            var dadosAula = _turmaDisciplinaAppService.ObterRegistrosDeAulasDoUsuario(UserId).Where(x => x.Data >= DateTime.Now.Date).ToList();
            model.AulasFuturas = dadosAula.Count;

            var dadosTrabalho = _turmaDisciplinaAppService.ObterRegistrosDeTrabalhosDoUsario(UserId).Where(x => x.DataEntrega >= DateTime.Now.Date).ToList();
            model.TrabalhosFuturos = dadosTrabalho.Count;

            var dadosExame = _turmaDisciplinaAppService.ObterRegistrosDeExamesDoUsuario(UserId).Where(x => x.Data >= DateTime.Now.Date).ToList();
            model.ExamesFuturos = dadosExame.Count;
            return model;
        }

        public DashboardController(ITarefaAppService tarefaAppService, ITurmaDisciplinaAppService turmaDisciplinaAppService, IUsuarioAppService usuario)
        {
            _tarefaAppService = tarefaAppService;
            _turmaDisciplinaAppService = turmaDisciplinaAppService;
            _usuario = usuario;
        }

        public ActionResult Index()
        {
            ApplicationCookies.DefinirCookieUsuario(ControllerContext, _usuario, UserId);
            return View(CarregarViewData());
        }

        #region Actions

        public ActionResult FinalizarTarefa(Guid tarefaId)
        {
            var registro = _tarefaAppService.ObterPorId(tarefaId);

            registro.Status = !registro.Status;

            var dataResult = _tarefaAppService.AdicionarOuAtualizar(registro);
            ValidarErrosDominio(dataResult.Message);

            return RedirectToAction("Index");
        }

        public ActionResult CarregarResumoTarefas(ViewModelParaHome model = null)
        {
            model.RegistrosTarefas = _tarefaAppService.ObterListaTarefa(UserId).ToList();
            model.TarefasPendentes = model.RegistrosTarefas.Count(x => !x.Status);

            return PartialView("_resumoTarefas", model);
        }


        public ActionResult CarregarResumoTrabalhos(ViewModelParaHome model = null)
        {
            model.RegistrosTrabalhos = _turmaDisciplinaAppService.ObterRegistrosDeTrabalhosDoUsario(UserId);

            return PartialView("_resumoTrabalhos", model);
        }

        public ActionResult CarregarResumoAulas(ViewModelParaHome model = null)
        {
            model.RegistrosAulas = _turmaDisciplinaAppService.ObterRegistrosDeAulasDoUsuario(UserId);

            return PartialView("_resumoAulas", model);
        }

        public ActionResult CarregarResumoExames(ViewModelParaHome model = null)
        {
            model.RegistrosExames = _turmaDisciplinaAppService.ObterRegistrosDeExamesDoUsuario(UserId);

            return PartialView("_resumoExames", model);
        }

        #endregion

    }
}