﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Linte.Core.DomainEvent.Events;
using Linte.Core.DomainEvent.Interfaces;
using Microsoft.AspNet.Identity;

namespace Responsavel.WebMVC.Controllers
{
    public class BaseController : Controller
    {

        protected readonly IHandler<DomainNotification> Notifications;

        protected string UserId => ControllerContext.HttpContext?.User.Identity.GetUserId();

        public BaseController()
        {
            Notifications = DomainEvent.Container.GetInstance<IHandler<DomainNotification>>();
        
        }

        
        public bool ValidarErrosDominio(string messageOk = null)
        {
            ModelState.Clear();

            if (!string.IsNullOrEmpty(messageOk))
            {
                TempData["ActionMessage"] = messageOk;
                ViewBag.StatusMessage = TempData["ActionMessage"];
            }

            TempData["ModelStateError"] = null;
            if (!Notifications.HasNotifications())
                return ModelState.IsValid;

            var mensagemErro = "";
            foreach (var error in Notifications.GetValues())
            {
                ModelState.AddModelError(string.Empty, error.Value);
                mensagemErro = $"{mensagemErro}{Environment.NewLine}{error.Value}";
            }

            TempData["ModelStateError"] = ModelState.Values;
            ViewBag.ErrosDominio = mensagemErro;
            return false;
        }

        public void GetModelStateData()
        {
            ViewBag.StatusMessage = TempData["ActionMessage"];
            TempData["ActionMessage"] = null;

            if (TempData["ModelStateError"] == null)
            {
                ModelState.Clear();
                return;
            }

            var mensagemErro = "";
            foreach (var error in (ICollection<ModelState>)TempData["ModelStateError"])
            {
                foreach (var linhaErro in error.Errors)
                {
                    ModelState.AddModelError("", linhaErro.ErrorMessage);
                    mensagemErro = $"{mensagemErro}{Environment.NewLine}{linhaErro.ErrorMessage}";
                }

            }
            TempData["ModelStateError"] = null;
            ViewBag.ErrosDominio = mensagemErro;
        }

        public void AddNotification(DomainNotification notification)
        {
            Notifications.Handle(notification);
        }
    }


}