﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AMais.Responsavel.Application.Interface;
using Linte.Core.DomainEvent.Events;
using Linte.Core.Log.Domain.Class;
using Linte.Core.Log.Domain.Entities;
using Linte.Core.Log.Domain.Events;
using Linte.Identity.Infra.CrossCutting.Identity.Configuration;
using Linte.Identity.Infra.CrossCutting.Identity.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Responsavel.WebMVC.App_Helpers;

namespace Responsavel.WebMVC.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly IUsuarioAppService _usuario;
        public readonly IPerfilResponsavelAppService _responsavelAppService;


        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IUsuarioAppService usuario, IPerfilResponsavelAppService responsavelAppService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _usuario = usuario;
            _responsavelAppService = responsavelAppService;
        }


        //
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var logTable = new LogDataTable();
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: true);
            switch (result)
            {
                case SignInStatus.Success:
                    var usuario = await _userManager.FindAsync(model.Email, model.Password);
                    if (!usuario.EmailConfirmed)
                    {
                        ViewBag.Id = usuario.Id;
                        return View("DisplayEmail");
                    }
                    logTable.TipoOperacao = TipoOperacao.LoginComSucesso;
                    logTable.Email = model.Email;
                    DomainEvent.Raise(new WriteLogEvent(logTable));
                    ApplicationCookies.DefinirCookieUsuario(ControllerContext, _usuario, usuario.Id);
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    logTable.TipoOperacao = TipoOperacao.UsuarioBloqueado;
                    logTable.Email = model.Email;
                    DomainEvent.Raise(new WriteLogEvent(logTable));
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    logTable.TipoOperacao = TipoOperacao.UsuarioSolicitouSenha;
                    logTable.Email = model.Email;
                    DomainEvent.Raise(new WriteLogEvent(logTable));
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    logTable.TipoOperacao = TipoOperacao.LoginOuSenhaInvalido;
                    logTable.Email = model.Email;
                    DomainEvent.Raise(new WriteLogEvent(logTable));
                    ModelState.AddModelError("", @"Login ou Senha incorretos.");
                    return View(model);
            }
        }

        //
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await _signInManager.HasBeenVerifiedAsync())
            {
                ViewBag.StatusMessage = "004 - Erro na verificação do código de validação.";
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(await _signInManager.GetVerifiedUserIdAsync());
            if (user != null)
            {
                ViewBag.Status = "DEMO: Caso o código não chegue via " + provider + " o código é: ";
                ViewBag.CodigoAcesso = await _userManager.GenerateTwoFactorTokenAsync(user.Id, provider);
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _signInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", @"Código Inválido.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    _responsavelAppService.CriarResponsavel(Guid.Parse(user.Id));

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    await _userManager.SendEmailAsync(user.Id, "Confirme sua Conta", HtmlLinkAtivacao(callbackUrl, user.Email));
                    ViewBag.Link = callbackUrl;
                    ViewBag.Id = user.Id;
                    return View("DisplayEmail");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ResendEmailConfirmationToken(string id)
        {
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(id);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = id, code = code }, protocol: Request.Url.Scheme);
            await _userManager.SendEmailAsync(id, "Confirme sua Conta", HtmlLinkAtivacao(callbackUrl));
            ViewBag.Link = callbackUrl;
            ViewBag.Id = id;
            ViewBag.StatusMessage = "Novo código enviado com sucesso";
            return View("DisplayEmail");
        }


        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                ViewBag.StatusMessage = "001 - Erro na confirmação do e-mail.";
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Não revelar se o usuario nao existe ou nao esta confirmado
                    return View("ForgotPasswordConfirmation");
                }

                //Environment.SetEnvironmentVariable("SENDGRID_APIKEY", "");
                var code = await _userManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                await _userManager.SendEmailAsync(user.Id, "Esqueci minha senha", HtmlTesteResetSenha(callbackUrl, user.Email));

                ViewBag.Link = callbackUrl;
                ViewBag.Status = "DEMO: Usar somente em ambiente de testes, caso o link não chegue: ";
                ViewBag.LinkAcesso = callbackUrl;
                return View("ForgotPasswordConfirmation");
            }

            // No caso de falha, reexibir a view. 
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Não revelar se o usuario nao existe ou nao esta confirmado
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await _userManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            ControllerContext.HttpContext.Session.RemoveAll();
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await _signInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                ViewBag.StatusMessage = "002 - Erro no envio do código de validação.";
                return View("Error");
            }
            var userFactors = await _userManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await _signInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                ViewBag.StatusMessage = "003 - Erro no envio do código de validação em duas etapas.";
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();

            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await _signInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // Se ele nao tem uma conta solicite que crie uma
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Pegar a informação do login externo.
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }


        public ActionResult LogOff()
        {
            var logTable = new LogDataTable() { TipoOperacao = TipoOperacao.LogOffComSucesso };
            DomainEvent.Raise(new WriteLogEvent(logTable));
            AuthenticationManager.SignOut();
            ApplicationCookies.LimparCookieUsuario(ControllerContext);
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion


        #region HTML Email

        private static string HtmlLinkAtivacao(string callBack, string email = null)
        {
            var htmlText = new StringBuilder();

            htmlText.AppendLine("    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            htmlText.AppendLine("    <html xmlns='http://www.w3.org/1999/xhtml'>");
            htmlText.AppendLine("    <head>");
            htmlText.AppendLine("      <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />");
            htmlText.AppendLine("      <title>Área do Responsável</title>");

            #region CSS
            htmlText.AppendLine("      <style type='text/css'>");
            htmlText.AppendLine("      body {");
            htmlText.AppendLine("       padding-top: 0 !important;");
            htmlText.AppendLine("       padding-bottom: 0 !important;");
            htmlText.AppendLine("       padding-top: 0 !important;");
            htmlText.AppendLine("       padding-bottom: 0 !important;");
            htmlText.AppendLine("       margin:0 !important;");
            htmlText.AppendLine("       width: 100% !important;");
            htmlText.AppendLine("       -webkit-text-size-adjust: 100% !important;");
            htmlText.AppendLine("       -ms-text-size-adjust: 100% !important;");
            htmlText.AppendLine("       -webkit-font-smoothing: antialiased !important;");
            htmlText.AppendLine("     }");
            htmlText.AppendLine("     .tableContent img {");
            htmlText.AppendLine("       border: 0 !important;");
            htmlText.AppendLine("       display: block !important;");
            htmlText.AppendLine("       outline: none !important;");
            htmlText.AppendLine("     }");
            htmlText.AppendLine("     a{");
            htmlText.AppendLine("      color:#382F2E;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine("");
            htmlText.AppendLine("    p, h1{");
            htmlText.AppendLine("      color:#382F2E;");
            htmlText.AppendLine("      margin:0;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine(" p{");
            htmlText.AppendLine("      text-align:left;");
            htmlText.AppendLine("      color:#999999;");
            htmlText.AppendLine("      font-size:14px;");
            htmlText.AppendLine("      font-weight:normal;");
            htmlText.AppendLine("      line-height:19px;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine("");
            htmlText.AppendLine("    a.link1{");
            htmlText.AppendLine("      color:#382F2E;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine("    a.link2{");
            htmlText.AppendLine("      font-size:16px;");
            htmlText.AppendLine("      text-decoration:none;");
            htmlText.AppendLine("      color:#ffffff;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine("");
            htmlText.AppendLine("    h2{");
            htmlText.AppendLine("      text-align:left;");
            htmlText.AppendLine("       color:#222222; ");
            htmlText.AppendLine("       font-size:19px;");
            htmlText.AppendLine("      font-weight:normal;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine("    div,p,ul,h1{");
            htmlText.AppendLine("      margin:0;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine("");
            htmlText.AppendLine("    .bgBody{");
            htmlText.AppendLine("      background: #ffffff;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine("    .bgItem{");
            htmlText.AppendLine("      background: #ffffff;");
            htmlText.AppendLine("    }");
            htmlText.AppendLine("	");
            htmlText.AppendLine("@media only screen and (max-width:480px)");
            htmlText.AppendLine("		");
            htmlText.AppendLine("{");
            htmlText.AppendLine("		");
            htmlText.AppendLine("table[class='MainContainer'], td[class='cell'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("		width: 100% !important;");
            htmlText.AppendLine("		height:auto !important; ");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("td[class='specbundle'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("		width:100% !important;");
            htmlText.AppendLine("		float:left !important;");
            htmlText.AppendLine("		font-size:13px !important;");
            htmlText.AppendLine("		line-height:17px !important;");
            htmlText.AppendLine("		display:block !important;");
            htmlText.AppendLine("		padding-bottom:15px !important;");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("		");
            htmlText.AppendLine("td[class='spechide'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("		display:none !important;");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("	    img[class='banner'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("	          width: 100% !important;");
            htmlText.AppendLine("	          height: auto !important;");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("		td[class='left_pad'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("			padding-left:15px !important;");
            htmlText.AppendLine("			padding-right:15px !important;");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("		 ");
            htmlText.AppendLine("}");
            htmlText.AppendLine("	");
            htmlText.AppendLine("@media only screen and (max-width:540px) ");
            htmlText.AppendLine("");
            htmlText.AppendLine("{");
            htmlText.AppendLine("		");
            htmlText.AppendLine("table[class='MainContainer'], td[class='cell'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("		width: 100% !important;");
            htmlText.AppendLine("		height:auto !important; ");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("td[class='specbundle'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("		width:100% !important;");
            htmlText.AppendLine("		float:left !important;");
            htmlText.AppendLine("		font-size:13px !important;");
            htmlText.AppendLine("		line-height:17px !important;");
            htmlText.AppendLine("		display:block !important;");
            htmlText.AppendLine("		padding-bottom:15px !important;");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("		");
            htmlText.AppendLine("td[class='spechide'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("		display:none !important;");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("	    img[class='banner'] ");
            htmlText.AppendLine("	{");
            htmlText.AppendLine("	          width: 100% !important;");
            htmlText.AppendLine("	          height: auto !important;");
            htmlText.AppendLine("	}");
            htmlText.AppendLine("	.font {");
            htmlText.AppendLine("		font-size:18px !important;");
            htmlText.AppendLine("		line-height:22px !important;");
            htmlText.AppendLine("		");
            htmlText.AppendLine("		}");
            htmlText.AppendLine("		.font1 {");
            htmlText.AppendLine("		font-size:18px !important;");
            htmlText.AppendLine("		line-height:22px !important;");
            htmlText.AppendLine("		");
            htmlText.AppendLine("		}");
            htmlText.AppendLine("}");
            htmlText.AppendLine("");
            htmlText.AppendLine("    </style>");
            #endregion

            #region Script
            htmlText.AppendLine("<script type='colorScheme' class='swatch active'>");
            htmlText.AppendLine("{");
            htmlText.AppendLine("    'name':'Default',");
            htmlText.AppendLine("    'bgBody':'ffffff',");
            htmlText.AppendLine("    'link':'382F2E',");
            htmlText.AppendLine("    'color':'999999',");
            htmlText.AppendLine("    'bgItem':'ffffff',");
            htmlText.AppendLine("    'title':'222222'");
            htmlText.AppendLine("}");
            htmlText.AppendLine("</script>");
            #endregion

            htmlText.AppendLine("  </head>");

            htmlText.AppendLine("  <body paddingwidth='0' paddingheight='0'   style='padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;' offset='0' toppadding='0' leftpadding='0'>");
            htmlText.AppendLine("    <table bgcolor='#ffffff' width='100%' border='0' cellspacing='0' cellpadding='0' class='tableContent' align='center'  style='font-family:Helvetica, Arial,serif;'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td><table width='600' border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff' class='MainContainer'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td valign='top' width='40'>&nbsp;</td>");
            htmlText.AppendLine("      <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("  <!-- =============================== Header ====================================== -->   ");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("    	<td height='75' class='spechide'></td>");
            htmlText.AppendLine("        ");
            htmlText.AppendLine("        <!-- =============================== Body ====================================== -->");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td class='movableContentContainer ' valign='top'>");
            htmlText.AppendLine("      	<div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'>");
            htmlText.AppendLine("        	<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td height='35'></td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td valign='top' align='center' class='specbundle'><div class='contentEditableContainer contentTextEditable'>");
            htmlText.AppendLine("                                <div class='contentEditable'>");
            htmlText.AppendLine("                                  <p style='text-align:center;margin:0;font-family:Georgia,Time,sans-serif;font-size:26px;color:#222222;'><span class='specbundle2'><span class='font1'>Bem vindo à sua&nbsp;</span></span></p>");
            htmlText.AppendLine("                                </div>");
            htmlText.AppendLine("                              </div></td>");
            htmlText.AppendLine("      <td valign='top' class='specbundle'><div class='contentEditableContainer contentTextEditable'>");
            htmlText.AppendLine("                                <div class='contentEditable'>");
            htmlText.AppendLine("                                  <p style='text-align:center;margin:0;font-family:Georgia,Time,sans-serif;font-size:26px;color:#289CDC;'><span class='font'>Área do Responsável</span> </p>");
            htmlText.AppendLine("                                </div>");
            htmlText.AppendLine("                              </div></td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("        </div>");
            htmlText.AppendLine("        <div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'>");
            htmlText.AppendLine("        	<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>");
            htmlText.AppendLine("                          <tr>");
            htmlText.AppendLine("                            <td valign='top' align='center'>");
            htmlText.AppendLine("                              <div class='contentEditableContainer contentImageEditable'>");
            htmlText.AppendLine("                                <div class='contentEditable'>");
            htmlText.AppendLine("                                  <img src='http://lintelligence.azurewebsites.net/geral/imagens/line.png' width='251' height='43' alt='' data-default='placeholder' data-max-width='560'>");
            htmlText.AppendLine("                                </div>");
            htmlText.AppendLine("                              </div>");
            htmlText.AppendLine("                            </td>");
            htmlText.AppendLine("                          </tr>");
            htmlText.AppendLine("                        </table>");
            htmlText.AppendLine("        </div>");
            htmlText.AppendLine("        <div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'>");
            htmlText.AppendLine("        	<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>");
            htmlText.AppendLine("                          <tr><td height='55'></td></tr>");
            htmlText.AppendLine("                          <tr>");
            htmlText.AppendLine("                            <td align='left'>");
            htmlText.AppendLine("                              <div class='contentEditableContainer contentTextEditable'>");
            htmlText.AppendLine("                                <div class='contentEditable' align='center'>");
            htmlText.AppendLine("                                  <h2 >Este é o seu e-mail de ativação</h2>");
            htmlText.AppendLine("                                </div>");
            htmlText.AppendLine("                              </div>");
            htmlText.AppendLine("                            </td>");
            htmlText.AppendLine("                          </tr>");
            htmlText.AppendLine("");
            htmlText.AppendLine("                          <tr><td height='15'> </td></tr>");
            htmlText.AppendLine("");
            htmlText.AppendLine("                          <tr>");
            htmlText.AppendLine("                            <td align='left'>");
            htmlText.AppendLine("                              <div class='contentEditableContainer contentTextEditable'>");
            htmlText.AppendLine("                                <div class='contentEditable' align='center'>");
            htmlText.AppendLine("                                  <p >");
            htmlText.AppendLine("                                    Obrigado por se cadastrar na Área do Reponsável, esperamos que nosso produto e ideias possam ajuda-lo a melhorar sua produtividade diária, organizar seus planos de estudos e ter todas as informações de aula nas suas mãos.");  //. Você pode <a target='_blank' href='#' class='link1' >clicar neste link </a> para ter acesso aos nossos materiais de como usar melhor nossa ferramenta.
            htmlText.AppendLine("                                    <br>");
            htmlText.AppendLine("                                    <br>");
            htmlText.AppendLine("                                    Se você tiver dúvidas, questões ou dicas de melhorias, gostaríamos muito de escuta-las, entre em contato com nosso time por e-mail, nas nossas redes sociais, Twitter e Facebook.");
            htmlText.AppendLine("                                    <br>");
            htmlText.AppendLine("                                    <br>");
            htmlText.AppendLine("                                    L'Intelligence,");
            htmlText.AppendLine("                                    <br>");
            htmlText.AppendLine("                                    <!-- <span style='color:#222222;'>LINTE - L'Intelligence IT Solutions</span> -->");
            htmlText.AppendLine("                                  </p>");
            htmlText.AppendLine("                                </div>");
            htmlText.AppendLine("                              </div>");
            htmlText.AppendLine("                            </td>");
            htmlText.AppendLine("                          </tr>");
            htmlText.AppendLine("");
            htmlText.AppendLine("                          <tr><td height='55'></td></tr>");
            htmlText.AppendLine("");
            htmlText.AppendLine("                          <tr>");
            htmlText.AppendLine("                            <td align='center'>");
            htmlText.AppendLine("                              <table>");
            htmlText.AppendLine("                                <tr>");
            htmlText.AppendLine("                                  <td align='center' bgcolor='#289CDC' style='background:#1A54BA; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;'>");
            htmlText.AppendLine("                                    <div class='contentEditableContainer contentTextEditable'>");
            htmlText.AppendLine("                                      <div class='contentEditable' align='center'>");
            htmlText.AppendLine($"                                        <a target='_blank' href='{callBack}' class='link2' style='color:#ffffff;'>Ative sua conta</a>");
            htmlText.AppendLine("                                      </div>");
            htmlText.AppendLine("                                    </div>");
            htmlText.AppendLine("                                  </td>");
            htmlText.AppendLine("                                </tr>");
            htmlText.AppendLine("                              </table>");
            htmlText.AppendLine("                            </td>");
            htmlText.AppendLine("                          </tr>");
            htmlText.AppendLine("                          <tr><td height='20'></td></tr>");
            htmlText.AppendLine("                        </table>");
            htmlText.AppendLine("        </div>");
            htmlText.AppendLine("        <div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'>");
            htmlText.AppendLine("        	<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td height='65'>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td  style='border-bottom:1px solid #DDDDDD;'></td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("    <tr><td height='25'></td></tr>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td valign='top' class='specbundle'><div class='contentEditableContainer contentTextEditable'>");
            htmlText.AppendLine("                                      <div class='contentEditable' align='center'>");
            htmlText.AppendLine("                                        <p  style='text-align:left;color:#CCCCCC;font-size:12px;font-weight:normal;line-height:20px;'>");
            htmlText.AppendLine("                                          <span style='font-weight:bold;'>L'Intelligence IT Solutions</span>");
            htmlText.AppendLine("                                          <br>");
            htmlText.AppendLine("                                          Av. Angélica, 2223 Higienópolis, São Paulo/SP - Brasil");
            htmlText.AppendLine("                                          <br>");
            htmlText.AppendLine("                                        </p>");
            htmlText.AppendLine("                                      </div>");
            htmlText.AppendLine("                                    </div></td>");
            htmlText.AppendLine("      <td valign='top' width='30' class='specbundle'>&nbsp;</td>");
            htmlText.AppendLine("      <td valign='top' class='specbundle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.AppendLine("  <tbody>");
            htmlText.AppendLine("    <tr>");
            htmlText.AppendLine("      <td valign='top' width='52'>");
            htmlText.AppendLine("                                    <div class='contentEditableContainer contentFacebookEditable'>");
            htmlText.AppendLine("                                      <div class='contentEditable'>");
            htmlText.AppendLine("                                        <a target='_blank' href='https://www.facebook.com/amaiseducacional/'><img src='http://lintelligence.azurewebsites.net/geral/imagens/facebook.png' width='52' height='53' alt='facebook icon' data-default='placeholder' data-max-width='52' data-customIcon='true'></a>");
            htmlText.AppendLine("                                      </div>");
            htmlText.AppendLine("                                    </div>");
            htmlText.AppendLine("                                  </td>");
            htmlText.AppendLine("      <td valign='top' width='16'>&nbsp;</td>");
            htmlText.AppendLine("      <td valign='top' width='52'>");
            htmlText.AppendLine("                                    <div class='contentEditableContainer contentTwitterEditable'>");
            htmlText.AppendLine("                                      <div class='contentEditable'>");
            htmlText.AppendLine("                                        <a target='_blank' href='https://twitter.com/lintelligenceit/'><img src='http://lintelligence.azurewebsites.net/geral/imagens/twitter.png' width='52' height='53' alt='twitter icon' data-default='placeholder' data-max-width='52' data-customIcon='true'></a>");
            htmlText.AppendLine("                                      </div>");
            htmlText.AppendLine("                                    </div>");
            htmlText.AppendLine("                                  </td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("    <tr><td height='88'></td></tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("");
            htmlText.AppendLine("        </div>");
            htmlText.AppendLine("        ");
            htmlText.AppendLine("        <!-- =============================== footer ====================================== -->");
            htmlText.AppendLine("      ");
            htmlText.AppendLine("      </td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("      <td valign='top' width='40'>&nbsp;</td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("    </tr>");
            htmlText.AppendLine("  </tbody>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("");
            htmlText.AppendLine("      </body>");
            htmlText.AppendLine("      </html>");
            htmlText.AppendLine("");

            return htmlText.ToString();
        }

        private static string HtmlTesteResetSenha(string callBack, string email)
        {
            var htmlText = new StringBuilder();

            htmlText.AppendLine("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            htmlText.AppendLine("<html xmlns='http://www.w3.org/1999/xhtml'>");
            htmlText.AppendLine("<head>");
            htmlText.AppendLine("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />");
            htmlText.AppendLine("<title> Sistema de Inscrição Online </title>");

            htmlText.AppendLine("<style type='text/css'>");
            htmlText.AppendLine("   table { ");
            htmlText.AppendLine("   font-family: Helvetica;");
            htmlText.AppendLine("       color: #333;");
            htmlText.AppendLine("  }");

            htmlText.AppendLine("     td {");
            htmlText.AppendLine("      padding: 15px 20px;");
            htmlText.AppendLine("      background-color: #fff;");
            htmlText.AppendLine("  }");

            htmlText.AppendLine("     p {");
            htmlText.AppendLine("         color: #adadad;");
            htmlText.AppendLine("         text-align: justify;");
            htmlText.AppendLine("         font-size: 14px;");
            htmlText.AppendLine("     }");

            htmlText.AppendLine("     a {");
            htmlText.AppendLine("        color: #fff;");
            htmlText.AppendLine("     }");

            htmlText.AppendLine("    .social {");
            htmlText.AppendLine("        text-decoration:none; display:block; cursor:pointer; float:right; margin-left: 10px;");
            htmlText.AppendLine("    }");


            htmlText.AppendLine("     .divisoria-tarefa {");
            htmlText.AppendLine("         padding:5px 20px;");
            htmlText.AppendLine("    }");

            htmlText.AppendLine("      .ir-tarefa {");
            htmlText.AppendLine("          color:#fff;");
            htmlText.AppendLine("          font-size:16px;");
            htmlText.AppendLine("          display:block;");
            htmlText.AppendLine("          float:right;");
            htmlText.AppendLine("          width:140px;");
            htmlText.AppendLine("          border-radius:0.5em;");
            htmlText.AppendLine("          text-decoration:none;");
            htmlText.AppendLine("          text-align: center;");
            htmlText.AppendLine("          background-color:#4993cb;");
            htmlText.AppendLine("          padding:15px;");
            htmlText.AppendLine("      }");

            htmlText.AppendLine("      .borda {");

            htmlText.AppendLine("         border-right: 1px solid #b0b0b0;");
            htmlText.AppendLine("         border-left: 1px solid #b0b0b0;");
            htmlText.AppendLine("     }");

            htmlText.AppendLine("    .borda-esquerda {");
            htmlText.AppendLine("        border-left: 1px solid #b0b0b0;");
            htmlText.AppendLine("    }");

            htmlText.AppendLine("    .borda-direita {");
            htmlText.AppendLine("        border-right: 1px solid #b0b0b0;");
            htmlText.AppendLine("    }");

            htmlText.AppendLine("    h2 {");
            htmlText.AppendLine("       color: #222;");
            htmlText.AppendLine("        text-align: justify;");
            htmlText.AppendLine("         font-weight: normal;");
            htmlText.AppendLine("        font-size: 1.3em;");
            htmlText.AppendLine("   }");

            htmlText.AppendLine("     h3 {");
            htmlText.AppendLine("       color: #222;");
            htmlText.AppendLine("       text-align: justify;");
            htmlText.AppendLine("       font-weight: lighter;");
            htmlText.AppendLine("       font-size: 1.2em;");
            htmlText.AppendLine("   }");

            htmlText.AppendLine("   .footer {");

            htmlText.AppendLine("      text-align: center;");
            htmlText.AppendLine("      background: #333;");
            htmlText.AppendLine("      color: #fff;");
            htmlText.AppendLine("      font-size: 12px;   ");
            htmlText.AppendLine("  }");


            htmlText.AppendLine("   .img-superior {");
            htmlText.AppendLine("     margin: 0px 10px;");
            htmlText.AppendLine("   }");
            htmlText.AppendLine("</style>");


            htmlText.AppendLine("</head>");
            htmlText.AppendLine("<body>");
            htmlText.AppendLine("<table bgcolor='#dbdbdb' cellpadding='0' cellspacing='0' border='0' width='100%'>");
            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td style='background:none;'>");
            htmlText.AppendLine("<table cellpadding='0' cellspacing='0' border='0' width='900px' align='center'>");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td style='padding:0px;' colspan='5'>");
            htmlText.AppendLine("<img src='https://lintelligencehd.blob.core.windows.net/siol/header_siol_temporario.png' width='100%'  data-default='placeholder'/>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td class='borda' style='text-align:center;' colspan='5'>");
            htmlText.AppendLine("<h2 style='text-align:center; font-weight: bold;'> SIOL - Sistema de Inscrição Online</h2>");
            htmlText.AppendLine("<h3 style='text-align:center;'> E - mail de redefinição de senha</h3>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td class='borda' colspan='5'>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td class='borda' colspan='5'>");
            htmlText.AppendLine("<h3> Utilize o Botão abaixo para criar sua nova senha.</h3>");
            htmlText.AppendLine("<h4> Enfatizamos que não divulgue ou compartilhe esse e-mail com ninguém.</h4>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td class='borda divisoria-tarefa' colspan='5'>");
            htmlText.AppendLine("<!-- Espaçamento entre as tarefas, a cada 1 tarefa deve conter -->");
            htmlText.AppendLine("<hr style='border:1px dashed #b0b0b0;'>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</ tr > ");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td class='borda' colspan='5'>");
            htmlText.AppendLine($"<a class='ir-tarefa' href='{callBack}'> Redefinir Senha</a>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td class='borda' colspan='5'>");
            htmlText.AppendLine("<!-- Coluna de espaçamento, deve ser mantida vazia! -->");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td class='borda footer' style='padding:0px 20px;' colspan='5'>");
            htmlText.AppendLine("<hr style='border-color:#b0b0b0;'>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");

            htmlText.AppendLine("<tr>");
            htmlText.AppendLine("<td class='borda-esquerda footer' colspan='3'>");
            htmlText.AppendLine($"<p style='text-align:left; font-size:12px;'> email enviado para: {email} </p> ");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("<td class='borda-direita footer' style='text-align:right;' colspan='2'>");
            htmlText.AppendLine("<p>SIOL</p>");
            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");

            htmlText.AppendLine("</table>");

            htmlText.AppendLine("</td>");
            htmlText.AppendLine("</tr>");
            htmlText.AppendLine("</table>");
            htmlText.AppendLine("</body>");

            return htmlText.ToString();

        }

        #endregion

    }
}