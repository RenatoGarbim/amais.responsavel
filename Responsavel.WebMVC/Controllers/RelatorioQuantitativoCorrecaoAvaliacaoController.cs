﻿// 21:55 25 03 2018  AvaliacaoDiagnostica.WebMVC  AvaliacaoDiagnostica.Solution

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AMais.Responsavel.Application.Interface;
using AMais.Responsavel.Application.Interface.Integracao;
using AMais.Responsavel.Application.Interface.Integracao.AgendaDoEstudante;
using AMais.Responsavel.DTO.Integracao;
using AMais.Responsavel.DTO.Integracao.AgendaDoEstudante;
using Responsavel.WebMVC.Models.RelatorioQuantitativoCorrecaoAvaliacao;
using Responsavel.WebMVC.Models.Shared;

namespace Responsavel.WebMVC.Controllers
{
    [Authorize]
    public class RelatorioQuantitativoCorrecaoAvaliacaoController : BaseController
    {
        private readonly IRelatorioAvaliacaoQuantitativoAppService _relatorioQuantitativoAppService;
        private readonly IObterDadosUsuario _obterDadosUsuario;
        private readonly IAmaisTurmaEstudanteAppService _amaisTurmaEstudanteAppService;
        private readonly ITokenAppService _tokenAppService;

        public RelatorioQuantitativoCorrecaoAvaliacaoController(IRelatorioAvaliacaoQuantitativoAppService relatorioQuantitativoAppService, ITokenAppService tokenAppService, IAmaisTurmaEstudanteAppService amaisTurmaEstudanteAppService, IObterDadosUsuario obterDadosUsuario)
        {
            _relatorioQuantitativoAppService = relatorioQuantitativoAppService;
            _tokenAppService = tokenAppService;
            _amaisTurmaEstudanteAppService = amaisTurmaEstudanteAppService;
            _obterDadosUsuario = obterDadosUsuario;
        }

        private IEnumerable<EstudanteDadosDTO> ObterDados()
        {
            var listaDados = new List<EstudanteDadosDTO>();

            var estudantes = _obterDadosUsuario.ObterDados();

            foreach (var estudante in estudantes)
            {
                var dados = _amaisTurmaEstudanteAppService.ObterTurmaEstudante(estudante.Token);

                estudante.Nome = dados.NomeEstudante;
                estudante.DescricaoAno = dados.DescricaoAno;
                estudante.DescricaoTurma = dados.DescricaoTurma;
                estudante.AnoLetivo = dados.AnoLetivo;


                listaDados.Add(estudante);
            }

            return listaDados;
        }

        private RelatorioQuantitativoCorrecaoAvaliacaoModelList CarregarViewDataAsync(RelatorioQuantitativoCorrecaoAvaliacaoModelList model = null)
        {
            if (model == null)
                model = new RelatorioQuantitativoCorrecaoAvaliacaoModelList();

            if (model.PaginacaoEOrdenacao == null)
                model.PaginacaoEOrdenacao = new ModelPaginacaoEOrdenacao() {SortOrder = "Matricula", SortDirection = "ASC", SortOld = "Matricula"};


            model.Estudantes = ObterDados().OrderBy(x => x.DescricaoAno);

            return model;
        }

        private async Task<RelatorioQuantitativoCorrecaoAvaliacaoModelList> CarregarViewDataPesquisaAsync(RelatorioQuantitativoCorrecaoAvaliacaoModelList model)
        {
            var taskRegistroAvaliacao = Task.Run(() => _relatorioQuantitativoAppService.ObterPorAvaliacao(model.Pesquisa));
            model.RegistrosAvaliacao = await taskRegistroAvaliacao;


            var task = Task.Run(() => _relatorioQuantitativoAppService.ObterPorEstudanteComCorrecao(model.Pesquisa));
            model.Registros = await task;

            var relatorioAvaliacaoEfetividadeDtos = model.RegistrosAvaliacao.ToList();
            DefineBarraDashboard(model, relatorioAvaliacaoEfetividadeDtos);

            return model;
        }

        private static void DefineBarraDashboard(RelatorioQuantitativoCorrecaoAvaliacaoModelList model, IReadOnlyCollection<AvaliacaoQuantitativaDTO> relatorioAvaliacaoEfetividadeDtos)
        {
            var totalAcertos = relatorioAvaliacaoEfetividadeDtos.Sum(x => x.TotalAcerto);
            var totalErros = relatorioAvaliacaoEfetividadeDtos.Sum(x => x.TotalErro);
            var efetividade = (totalAcertos + totalErros) > 0 ? Math.Round(totalAcertos / (totalAcertos + totalErros) * 100, 2) : 0;

            var icone = "";
            var cssCorBarra = "";
            if (efetividade >= 66)
            {
                icone = "trending_up";
                cssCorBarra = "efetividade_up";
            }
            else if (efetividade >= 33 && efetividade < 66)
            {
                icone = "trending_flat";
                cssCorBarra = "efetividade_media";
            }
            else if (efetividade < 33)
            {
                icone = "trending_down";
                cssCorBarra = "efetividade_down";
            }

            model.BarraDashboard = new BarraDashboardViewModel {Item = new List<ItemBarraDashboardViewModel>()};
            model.BarraDashboard.Item.Add(new ItemBarraDashboardViewModel
            {
                Icone = "group",
                CssCorBarra = "tarefa",
                Descricao = "Total de Avaliações",
                Mensagem = "Quantidade de avaliações que este relatório contempla",
                Valor = relatorioAvaliacaoEfetividadeDtos.Select(x => x.DescricaoAvaliacao).Distinct().Count()
            });
            model.BarraDashboard.Item.Add(new ItemBarraDashboardViewModel()
            {
                Icone = "perm_identity",
                CssCorBarra = "aula",
                Descricao = "Total de Questões",
                Mensagem = " Quantidade de questões que este relatório contempla",
                Valor = relatorioAvaliacaoEfetividadeDtos.Sum(x => x.TotalQuestao)
            });
            model.BarraDashboard.Item.Add(new ItemBarraDashboardViewModel()
            {
                Icone = "assignment",
                CssCorBarra = "trabalho",
                Descricao = "Total de Disciplinas",
                Mensagem = "Quantidade de disciplinas que este relatório contempla",
                Valor = relatorioAvaliacaoEfetividadeDtos.Sum(x => x.TotalDisciplina)
            });
            model.BarraDashboard.Item.Add(new ItemBarraDashboardViewModel()
            {
                Icone = icone,
                CssCorBarra = cssCorBarra,
                Descricao = "Efetividade",
                Mensagem = $"Porcentual de acertos. Acertos x Erros {totalAcertos:n0} x {totalErros:n0} ",
                Valor = efetividade
            });
        }


        public ActionResult Index()
        {
            GetModelStateData();

            var model = new RelatorioQuantitativoCorrecaoAvaliacaoModelList
            {
                PaginacaoEOrdenacao = (ModelPaginacaoEOrdenacao) TempData["PaginacaoEOrdenacao"],
                Pesquisa = (AvaliacaoRelatorioPesquisaDTO) TempData["Pesquisa"]
            };

            if (!_tokenAppService.ObterTodos().Any())
            {
                ModelState.AddModelError("Erro", "Informe primeiramente seu token de acesso!");
                return RedirectToAction("InformarToken", "Token");
            }

            TempData["PaginacaoEOrdenacao"] = null;
            TempData["Pesquisa"] = null;
            model = CarregarViewDataAsync(model);

            return View(model);
        }


        public async Task<PartialViewResult> ExibirRelatorio(AvaliacaoRelatorioPesquisaDTO pesquisa, int turmaEstudanteId)
        {
            var model = new RelatorioQuantitativoCorrecaoAvaliacaoModelList {Pesquisa = pesquisa};

            var listaTurmaEstudante = new int[] { turmaEstudanteId };

            model.Pesquisa.TurmaEstudanteId = listaTurmaEstudante;

            model = await CarregarViewDataPesquisaAsync(model);

            return await Task.Run(() => PartialView("_exibirRelatorio", model));
        }


        public async Task<ActionResult> LimparPesquisa(RelatorioQuantitativoCorrecaoAvaliacaoModelList model)
        {

            TempData["PaginacaoEOrdenacao"] = model.PaginacaoEOrdenacao;
            return await Task.Run<ActionResult>(() => RedirectToAction("Index"));
        }

    }
}