﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AMais.Responsavel.Application.Interface;
using AMais.Responsavel.DTO;
using Linte.Core.DomainEvent.Events;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using Responsavel.WebMVC.App_Helpers;
using Responsavel.WebMVC.Models.MeuPerfil;

namespace Responsavel.WebMVC.Controllers
{
    [Authorize]
    public class MeuPerfilController : BaseController
    {
        private readonly IUsuarioAppService _usuarioAppService;
        private readonly IPerfilResponsavelAppService _responsavelAppService;

        private async Task<ViewModelParaMeuPerfil> CarregarViewData(ViewModelParaMeuPerfil model = null)
        {
            if (model == null) model = new ViewModelParaMeuPerfil() { RegistroUsuario = new PerfilResponsavelDTO() };

            var task = Task.Run(() => _responsavelAppService.ObterDadosDoUsuario(UserId));
            model.RegistroUsuario = await task;

            return model;
        }

        public MeuPerfilController(IUsuarioAppService usuarioAppService, IPerfilResponsavelAppService responsavelAppService)
        {
            _usuarioAppService = usuarioAppService;
            _responsavelAppService = responsavelAppService;
        }

        public async Task<ActionResult> Index()
        {
            GetModelStateData();

            ViewModelParaMeuPerfil model;

            if (TempData["Model"] == null)
            {
                model = await CarregarViewData();
                return View(model);
            }
            
            model = (ViewModelParaMeuPerfil)TempData["Model"];
            TempData["Model"] = null;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> SalvarDadosAsync(ViewModelParaMeuPerfil model, HttpPostedFileBase files)
        {
            if (files != null && files.ContentLength > 0)
            {
                ApagarImagemPerfil();
                var fileName = $"{UserId}-{new Random().Next()}{Path.GetExtension(files.FileName)}";
                await SalvarImagemPerfil(files, fileName);
                model.RegistroUsuario.CaminhoImagem = $"https://lintelligencehd.blob.core.windows.net/responsavel-imagem-perfil/{fileName}";
            }

            if (!ModelState.IsValid) return View("Index", await CarregarViewData(model));

            var guid = Guid.Parse(UserId);

            model.RegistroUsuario.UsuarioId = guid;

            var dataResult = _responsavelAppService.AdicionarOuAtualizar(model.RegistroUsuario);
            ValidarErrosDominio(dataResult.Message);

            if (!dataResult.Result) return View("Index", await CarregarViewData(model));

            TempData["Model"] = model;
            ApplicationCookies.LimparCookieUsuario(ControllerContext);
            ApplicationCookies.DefinirCookieUsuarioWithForceReload(ControllerContext, _usuarioAppService, UserId);
            return await Task.Run<ActionResult>(() => RedirectToAction("Index"));
        }
        
        private void ApagarImagemPerfil()
        {
            LinteAzureStorage.ApagarArquivo(UserId, Notifications);
        }

        private async Task<bool> SalvarImagemPerfil(HttpPostedFileBase files, string fileName)
        {
            if (files.ContentLength > 1536000)
            {
                Notifications.Handle(new DomainNotification("ImgUpLoadError", "A imagem selecionada não é válida para upload, selecione uma imagem menor!"));
                ModelState.AddModelError("Erro ao salvar imagem do usuário", "Tamanho do arquivo não permitido");
                return false;
            }
            else
            {
                var storageAccount = LinteAzureStorage.CreateStorageAccountFromConnectionString();
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("responsavel-imagem-perfil");
                try
                {
                    var requestOptions = new BlobRequestOptions() { RetryPolicy = new NoRetry() };
                    await container.CreateIfNotExistsAsync(requestOptions, null);
                }
                catch (StorageException e)
                {
                    Notifications.Handle(new DomainNotification("ErroStorage", e.Message));
                    ModelState.AddModelError("Erro ao salvar imagem do usuário", e);
                    throw;
                }
                var blockBlob = container.GetBlockBlobReference(fileName);
                blockBlob.Properties.ContentType = files.ContentType;
                using (var fileStream = files.InputStream)
                {
                    await blockBlob.UploadFromStreamAsync(fileStream);
                }
                return true;
            }
        }
    }
}