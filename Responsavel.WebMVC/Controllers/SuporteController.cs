﻿using System;
using System.Web.Mvc;
using Linte.Core.Zendesk.Integration;
using Microsoft.AspNet.Identity;
using Responsavel.WebMVC.Models.Suporte;

namespace Responsavel.WebMVC.Controllers
{
    public class SuporteController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ErroGeral()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult PageNotFound()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EnviarTicket(string controllerName, string actionName, string message, string ticketNumber)
        {
            TempData["TicketNumber"] = ticketNumber;
            ZendeskIntegration.SendNewTicketToUser(System.Web.HttpContext.Current?.User.Identity.GetUserName(), ticketNumber, controllerName, actionName, message);
            return RedirectToAction("TicketEnviado");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EnviarTicketDoUsuario(SuporteViewModel model)
        {
            var ticketNumber = Guid.NewGuid().ToString();
            TempData["TicketNumber"] = ticketNumber;
            ZendeskIntegration.SendNewTicketToUser(System.Web.HttpContext.Current?.User.Identity.GetUserName(), ticketNumber, "Suporte", "EnviarTicketDoUsuario", model.Menssagem);
            return RedirectToAction("TicketEnviado");
        }

        public ActionResult TicketEnviado()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult BadRequest()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Unauthorized()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Forbidden()
        {
            return View();
        }

        public ActionResult ErroNaRegraDeNegocio(string regra, string mensagemMotivo, string mensagemErro, string botaoDaAcao = null)
        {
            ViewBag.Regra = regra;
            ViewBag.MensagemErro = mensagemErro;
            ViewBag.MensagemMotivo = mensagemMotivo;
            ViewBag.Botao = botaoDaAcao;
            return View();

        }

    }
}