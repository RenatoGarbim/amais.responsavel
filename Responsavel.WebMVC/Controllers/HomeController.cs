﻿using System.Diagnostics;
using System.Reflection;
using System.Web.Mvc;

namespace Responsavel.WebMVC.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            var version = fvi.FileVersion;
            ViewBag.AgendaDoEstudanteVersion = $"{version}";
            return View();
        }

    }
}