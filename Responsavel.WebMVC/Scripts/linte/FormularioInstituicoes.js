﻿
$(document).ready(function () {

    $("#campo-nome-escola").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Instituicao/AutoCompleteEscola",
                type: "POST",
                dataType: "json",
                data: {
                    cidade: $("#campo-cidade-escola").val(),
                    estado: $("#campo-estado").val(),
                    nome: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {

                        return { label: item.Nome, value: item.Nome };
                    }));
                }
            });
        },
        messages: {
            noResults: "", results: ""
        }
    });

    $("#campo-cidade-escola").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Instituicao/AutoCompleteMunicipios",
                type: "POST",
                dataType: "json",
                data: {
                    estado: $("#campo-estado").val(),
                    municipio: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {

                        return { label: item, value: item };

                    }));
                }
            });
        },
        messages: {
            noResults: "", results: ""
        }
    });

    $("#campo-nome-escola").focusout(function () {

        // get value from input-loader textbox
        var UserIDc = $(this).val();

        $(function () {
            // call /home/getuserdata and pass the user id from input-loader textbox
            $.get("/Instituicao/AutoCompleteForm?nomeEscola=" + UserIDc, function (result) {

                // set user name and email textbox value 
                $('#campo-mec-escola').val(result.CodigoMec);
                $('#campo-rua-escola').val(result.Rua);
                $('#campo-bairro-escola').val(result.Bairro);
                $('#campo-complemento-escola').val(result.Complemento);
                $('#campo-estado-escola').val(result.Estado);
                $('#campo-cep-escola').val(result.CEP);
                $('#campo-numero-escola').val(result.Numero);
            });
        });
    });

});