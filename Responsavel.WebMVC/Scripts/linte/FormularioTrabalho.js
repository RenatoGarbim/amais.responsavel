﻿
    AdicionarNovaTarefa = function () {

        $('#formularioTarefa').addClass('on');
        $('#botaoAdicionarNovaTarefa').prop('disabled', true);
        $('#formularioListaTarefa').hide();

    };

    CancelarAdicaoTarefa = function () {

        $('#formularioTarefa').removeClass('on');
        $('#botaoAdicionarNovaTarefa').prop('disabled', false);
        $('#formularioListaTarefa').show();

    };

TarefaFuncao = function (tarefaId) {

    $(function () {

        var token = $('[name=__RequestVerificationToken]').val();

        //var tarefaId = $("#guid-tarefa").val();

        $.get("/Aula/FinalizarTarefa?tarefaId=" + tarefaId, function (dataMessage) {

            if (dataMessage.status) {

                swal({
                    title: 'Sua Tarefa foi Atualizada!',
                    text: dataMessage.mensagem,
                    type: 'success',
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                })

            }
            else {

                swal({
                    title: 'Erro na Requisição.',
                    text: dataMessage.mensagem,
                    type: 'error',
                    confirmButtonClass: "btn btn-info",
                    buttonsStyling: false
                })

            }



        });

    });

};

