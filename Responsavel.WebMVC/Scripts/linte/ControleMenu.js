﻿
function ExibirMenu(navBar, menuItem, menu) {

    $(navBar).append('<ul id="menuSuperior" class="nav nav-pills nav-pills-primary nav-pills-icons justify-content-center" role="tablist"></ul>');

    for (var menuSuperior = 0; menuSuperior < menu.length; menuSuperior++) {

        $("#menuSuperior").append('<li class="nav-item "><a class="nav-link ' + menu[menuSuperior].classContent + ' " href="#superior' + menu[menuSuperior].ref + '" data-toggle="tab"><i class="material-icons">' + menu[menuSuperior].icone + '</i>' + menu[menuSuperior].descricao + '</a></li>');
        //$("#menuSuperior").append('<li class="nav-item "><a class="nav-link" href="#superior' + menu[menuSuperior].ref + '" data-toggle="tab"><i class="material-icons">' + menu[menuSuperior].icone + '</i>' + menu[menuSuperior].descricao + '</a></li>');
    }

    for (var i = 0; i < menu.length; i++) {

        var tabPaneSuperior = $('<div class="tab-pane ' + menu[i].classContent + '" id ="superior' + menu[i].ref + '">');

        if (menu[i].sub !== null) {

            var cardAbas = $('<div class="card">');
            var cardInterno = $('<div class="card-header-noborder" data-background-color="blue">');
            var ulInterno = $('<ul class="nav nav-pills nav-pills-blue tab-content-menubar">');

            for (var menuNivel2 = 0; menuNivel2 < menu[i].sub.length; menuNivel2++) {
                var itemMenuNivel2 = menu[i].sub[menuNivel2];
                var subli = $('<li class="nav-item "><a class="nav-link ' + itemMenuNivel2.classContent + '" href="#superior' + itemMenuNivel2.ref + '" data-toggle="tab">' + itemMenuNivel2.descricao + '</a></li>');
                ulInterno.append(subli);
            }

            cardInterno.append(ulInterno);
            cardAbas.append(cardInterno);

            var cardInterno1 = $('<div class="card-content">');
            var tabContent = $('<div class="tab-content ' + menu[i].classContent + ' tab-space">');
            for (var itemMenu = 0; itemMenu < menu[i].sub.length; itemMenu++) {

                var menuItem1 = menu[i].sub[itemMenu];

                var tabPane = $('<div class="tab-pane menu-card-content ' + menuItem1.classContent + '" id="superior' + menuItem1.ref + '">');

                if (menuItem1.sub !== null) {

                    for (var itemDoMenu = 0; itemDoMenu < menuItem1.sub.length; itemDoMenu++) {
                        var elemento = menuItem1.sub[itemDoMenu];

                        var url = "#";
                        if (elemento.controllerName !== "") {
                            url = '/' + elemento.controllerName + '/' + elemento.actionName;
                        }

                        var ahRefMenu = $(
                            '<a href="' + url + '">' +
                            '<div class="card div-menu-hover">' +
                            '<div class="row">' +
                            '<div class="col-md-12 div-menu-padding">' +
                            '<div class="card-body">' +
                            '<h3 class="card-title">' + elemento.descricao + '</h3>' +
                            '<h4>' + elemento.detalhe + '</h4>' +
                            '<hr/>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</a>');

                        $(tabPane).append(ahRefMenu);
                    }
                    tabContent.append(tabPane);
                    cardInterno1.append(tabContent);
                    $(cardAbas).append(cardInterno1);
                }

                tabPaneSuperior.append(cardAbas);
            }
        }

        $(menuItem).append(tabPaneSuperior);
    }

}

function ExibirMenuLateral(navBar, menuItem, menu, divMenuModal) {

    $(navBar).replaceWith('<ul id="menuLateral" class="nav"></ul>');

    for (var menuSuperior = 0; menuSuperior < menu.length; menuSuperior++) {

        var itemMenuSuperior = menu[menuSuperior];
        var aHref = '#lateral' + itemMenuSuperior.ref;
        if (itemMenuSuperior.controllerName !== undefined)
            aHref = encodeURI(itemMenuSuperior.controllerName + "/" + itemMenuSuperior.actionName);

        var liPrincipal = $('<li class="nav-item">');
        liPrincipal.append('<a class="nav-link" href="' + aHref + '" data-toggle="collapse"><i class="material-icons">' + itemMenuSuperior.icone + '</i><p>' + itemMenuSuperior.descricao + '<b class="caret"></b></p></a>');

        if (itemMenuSuperior.sub !== null) {

            var div = $('<div class="collapse" id="lateral' + itemMenuSuperior.ref + '">');
            var ul = $('<ul class="nav">');

            for (var itemAba = 0; itemAba < itemMenuSuperior.sub.length; itemAba++) {

                var menuItemAba = itemMenuSuperior.sub[itemAba];
                
                if (menuItemAba.sub === null) {
                    var urlLocal = "#";
                    if (menuItemAba.controllerName !== "") {
                        urlLocal = '/' + menuItemAba.controllerName + '/' + menuItemAba.actionName;
                    }

                    ul.append('<li class="nav-item" id="' + menuItemAba.ref + '"><a class="nav-link" href="' + urlLocal +'"><span class="sidebar-mini">' + menuItemAba.descricao.substring(0, 3).toUpperCase() + ' </span><span class="sidebar-normal">' + menuItemAba.descricao + '</span></a></li>');
                }
                else {
                    
                    ul.append('<li class="nav-item" id="' + menuItemAba.ref + '"><a class="nav-link" data-toggle="modal" data-target = "#modal' + menuItemAba.ref + '" href="#"><span class="sidebar-mini">' + menuItemAba.descricao.substring(0, 3).toUpperCase() + ' </span><span class="sidebar-normal">' + menuItemAba.descricao + '</span></a></li>');
                    var divModal = $('<div id="modal' + menuItemAba.ref + '" class="modal big_container fade" role="dialog">');
                    var divDialog = $('<div class="modal-dialog modal-lg">');
                    var divModelContent = $('<div class="modal-content">');
                    var divModalHeader = $('<div class="modal-header">');
                    divModalHeader.append('<h3><b>' + itemMenuSuperior.descricao + '/' + menuItemAba.descricao + '</b><button type="button" class="close" data-dismiss="modal">&times;</button></h3>');
                    divModelContent.append(divModalHeader);

                    var conteudoDivModal = $('<div class="modal-body">');

                    for (var itemMenu = 0; itemMenu < menuItemAba.sub.length; itemMenu++) {

                        var elemento = menuItemAba.sub[itemMenu];

                        var url = "#";
                        if (elemento.controllerName !== "") {
                            url = '/' + elemento.controllerName + '/' + elemento.actionName;
                        }

                        var ahRefMenu = $(
                            '<a href="' + url + '">' +
                            '<div class="card div-menu-hover">' +
                            '<div class="row">' +
                            '<div class="col-md-12 div-menu-padding">' +
                            '<div class="card-content">' +
                            '<h3 class="card-title">' + elemento.descricao + '</h3>' +
                            '<h4>' + elemento.detalhe + '</h4>' +
                            '<hr/>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</a>');

                        $(conteudoDivModal).append(ahRefMenu);

                    }

                    divModelContent.append(conteudoDivModal);
                    var modalFooter = $('<div class="modal-footer">');
                    modalFooter.append('<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>');
                    divModelContent.append(modalFooter);
                    divDialog.append(divModelContent);
                    divModal.append(divDialog);

                    $(divMenuModal).append(divModal);

                }
                
            }
            div.append(ul);
            liPrincipal.append(div);
        }

        $("#menuLateral").append(liPrincipal);

    }

}
