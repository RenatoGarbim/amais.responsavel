﻿$(document).ready(function () {

    function handleFileSelect(evt) {
        var imagens = evt.target.files;
        var f = imagens[0];

        var reader = new FileReader();

        reader.onload = (function(theFile) {
            return function(e) {
                var span = document.createElement("span");
                span.innerHTML = [
                    '<img class="custom-thumb" src="', e.target.result,
                    '" title="', escape(theFile.name), '"/>'
                ].join("");
                document.getElementById("lista").innerHTML = "";
                document.getElementById("lista").insertBefore(span, null);

            };
        })(f);

        reader.readAsDataURL(f);

    }

    document.getElementById("imagens").addEventListener("change", handleFileSelect, false);
    
});