﻿
function mensagemAlerta(type, mensagem) {

    if (type === 'sucesso-na-validacao') {
        swal({
            title: "Operação com sucesso!",
            text: "<b class='text-success'>" + mensagem + "</b>"  ,
            buttonsStyling: false,
            confirmButtonClass: "btn btn-success",
            type: "success"
        });
    }
    if (type === 'erro-na-validacao') {
        swal({
            title: "Falha na Requisição",
            buttonsStyling: false,
            confirmButtonClass: "btn btn-info",
            type: "error",
            html: "<b class='text-danger'>" + mensagem + "</b>"  
                
        });
    }

    if (type === 'sucesso-na-inscricao') {
        swal({
            title: "Sua inscrição foi concluida com sucesso!",
            html: "<div>" +
                "<b class='text-success'>" + "Seu nome já foi inserido na lista do Evento!" +  "</b>" +
            "<p>Usando as informações de acesso cadastradas para logar no sistema.</p>" +
            "<small>na área de acesso você consegue verificar seus dados e saber mais sobre o Evento.</small>" +
                '</div>',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-success",
            confirmButtonText: "Logar agora",
            type: "success"
        }).then(function() {

            var Url = "/Account/Login";
            window.location = Url;

        });
    }
}