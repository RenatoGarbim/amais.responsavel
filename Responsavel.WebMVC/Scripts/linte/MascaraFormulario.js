﻿$(document).ready(function() {

    $(".form-cep").mask("00000-000");
    $(".form-cpf").mask("000.000.000-00");
    $(".form-estado").mask("AA");
    $(".form-telefone-fixo").mask("(00) 0000-0000");
    $(".form-telefone-movel").mask("(00) 00000-0000");
    $(".form-data").mask("00/00/0000");
    $(".datepicker").mask("00/00/0000");
    //$(".datepicker").datepicker({
    //    format: "dd/mm/yyyy",
    //    language: "pt-BR"
    //});
    //$.datepicker.setDefaults($.datepicker.regional["pt-BR"]);

    //$(".datepicker1").regional['pt-BR'] = {
    //    closeText: 'Fechar',
    //    prevText: '&#x3c;Anterior',
    //    nextText: 'Pr&oacute;ximo&#x3e;',
    //    currentText: 'Hoje',
    //monthNames: ["Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio", "Junho",
    //    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
    //    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
    //        'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    //    dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
    //    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    //    dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    //    weekHeader: 'Sm',
    //    dateFormat: 'dd/mm/yyyy',
    //    firstDay: 0,
    //    isRTL: false,
    //    showMonthAfterYear: false,
    //    yearSuffix: ''
    //};
});
