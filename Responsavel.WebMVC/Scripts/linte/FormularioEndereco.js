﻿$(document).ready(function () {

    $("#campo-municipio").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Endereco/AutoCompleteMunicipios",
                type: "POST",
                dataType: "json",
                data: {
                    estado: $("#campo-estado").val(),
                    municipio: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {

                        return { label: item, value: item };

                    }));
                }
            });
        },
        messages: {
            noResults: "", results: ""
        }
    });

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#campo-estado").val("");
        $("#campo-municipio").val("");
        $("#campo-rua").val("");
        $("#campo-bairro").val("");
    }
    
    $("#campo-cep-endereco").blur(function () {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep !== "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#campo-estado").val("...");
                $("#campo-municipio").val("...");
                $("#campo-rua").val("...");
                $("#campo-bairro").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#campo-rua").val(dados.logradouro);
                        $("#campo-bairro").val(dados.bairro);
                        $("#campo-municipio").val(dados.localidade);
                        $("#campo-estado").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    }); 

});