// JavaScript source code

function ExcluirRegistro(registroId, form, actionExcluir, actionRecarregar) {

    swal({
        title: "Tem certeza ?",
        text: "Confirme a exclus&atildeo desse registro ?!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
    }).then(function () {
        var model = $("#" + form).serializeArray();
        $.ajax({
            url: "/" + actionExcluir + "/" + registroId,
            type: "POST",
            data: model,
            dataType: "JSON",
            success: function (response) {

                if (response.success) {

                    var url = "/" + actionRecarregar + "?messageAction=" + response.message;
                    window.location = url;
                }
                else {
                    swal({
                        title: "Falha na Requisi&ccedil&atildeo",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-info",
                        type: "error",
                        html: "<b class='text-danger'>" + response.message + "</b>"

                    });

                }


            },
            error: function () {
                alert("Erro na execu&ccedil&atildeo da rotina de exclus&atildeo");
            }
        });

    },
        function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal({
                    title: "A&ccedil&atildeo Cancelada",
                    text: "Seu registro ser&aacute mantido na base!",
                    type: "error",
                    confirmButtonClass: "btn btn-info",
                    buttonsStyling: false
                });
            }
        });
};
